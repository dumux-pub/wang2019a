# the test to iteraltively slove chemical equilibrium
# suppose the reaction in form a+b<->c

def calK (solution):
    return solution[2]/solution[1]/solution[0]

def calNew(solutionOld,delta):
    solution = solutionOld.copy()
    solution[0] = solution[0] - delta
    solution[1] = solution[1] - delta
    solution[2] = solution[2] + delta
    return solution

# # initial condition
# v = [1,1,2]
# k = calK(v)
# print(k)

# sPre = [15,21,7]

# i = 0
# sOld = sPre.copy()
# sNow = sPre.copy()
# while True:
# #     if (i == 0):
#     delta0 = sNow[0]-sNow[2]/k/sNow[1]
#     delta1 = sNow[1]-sNow[2]/k/sNow[0]
#     delta2 = sNow[0]*sNow[1]*k - sNow[2]
#     delta = max(delta0,delta1,delta2)
#     delta = min(delta,sNow[0],sNow[1])
#     delta = max(-sNow[2],delta)
#     #     delta0 = sNow[0] - sPre[0]
#     #     delta1 = sNow[1] - sPre[1]
#     #     delta2 = sNow[2] - sPre[2]
#     #     delta = max(delta0,delta1,delta2)  
#     sOld = sNow
#     sNow = calNew(sNow,0.9*delta)
#     print("delta:",delta)
#     print("K",calK(sNow))
    
#     i = i + 1
#     if i > 20:
#         break
#     continue

sum = 9.4929e-1
ratio = 4.6791e-7

x1 = 6e-1
x2 = 1
print(x1)
for i in range(20):
    x2old = x2
    x2 = x1*ratio

    x1old = x1
    x1 = sum-x2
    print(x1old/x1,x2old/x2)

        
