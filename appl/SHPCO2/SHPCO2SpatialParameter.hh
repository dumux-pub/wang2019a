// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the SHPCO2 problem, where the trapped CO2 in gas phase 
 *        is removed by the injected water.
 */

#ifndef DUMUX_SHPCO2_SPATIAL_PARAMETERS_HH
#define DUMUX_SHPCO2_SPATIAL_PARAMETERS_HH

#include <cmath>

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>

namespace Dumux {

/*!
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the SHPCO2 problem, where the trapped CO2 in gas phase 
 *        is removed by the injected water.
 */
template<class GridGeometry, class Scalar>
class SHPCO2SpatitalParams
: public FVSpatialParams<GridGeometry, Scalar,
                         SHPCO2SpatitalParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

    using ParentType = FVSpatialParams<GridGeometry, Scalar,
                                       SHPCO2SpatitalParams<GridGeometry, Scalar>>;

    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

    using GlobalPosition = typename SubControlVolume::GlobalPosition;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Scalar;
    //! Export the material law type used
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    SHPCO2SpatitalParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        referencePorosity_     = getParam<Scalar>("SpatialParams.referencePorosity", 0.2);
        irreducibleLiqSat_     = getParam<Scalar>("SpatialParams.IrreducibleLiqSat", 0.2);
        irreducibleGasSat_     = getParam<Scalar>("SpatialParams.IrreducibleGasSat", 0.25);

        // residual saturations
        materialParams_.setSwr(irreducibleLiqSat_);
        materialParams_.setSnr(irreducibleGasSat_);

        // parameters of Brooks & Corey Law
        materialParams_.setVgn(4.37);
        materialParams_.setVgm(0.77);
        materialParams_.setVgAlpha(0.37);
    }

    /*!
     *  \brief Defines the minimum porosity \f$[-]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar minimalPorosity(const Element& element, const SubControlVolume &scv) const
    { return 1e-5; }

    /*!
     *  \brief Defines the volume fraction of the inert component
     *
     *  \param globalPos The global position in the domain
     *  \param compIdx The index of the inert solid component
     */
    template<class SolidSystem>
    Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos, int compIdx) const
    { return 1.0-referencePorosity_; }

    /*!
     *  \brief Defines the reference porosity \f$[-]\f$ distribution.
     *
     *  This is the porosity of the porous medium without any of the
     *  considered solid phases.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    { return referencePorosity_; }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *  \param elemSol The element solution
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        auto ipGlobal = scv.center();
        return ifPointInBarrierZone(ipGlobal)? 1e-15: 100e-15;
    }

    Scalar theta(const SubControlVolume &scv) const
    { return 323.15-273.15;}

    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    { return materialParams_; }

    // define which phase is to be considered as the wetting phase
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }

    template<class GlobalPosition>
    bool ifPointInGasZone(const GlobalPosition& globalPos)
    {
        using std;
        Scalar dist = pow(globPos[0]-1500,2)+pow(globPos[1]-2250,2);
        dist = sqrt(dist);
        Scalar radius = 3/4*sqrt(2)*1000;
        return dist < radius+eps_ ? true : false;
    }

    template<class GlobalPosition>
    bool ifPointInBarrierZone(const GlobalPosition& globalPos)
    {   
        if ((1000-eps_<=globalPos[0] && globalPos[0]<=1250+eps_) && 
            (0-eps_   <=globalPos[1] && globalPos[1]<= 2000+eps_))
           return true;
        
        if ((2250-eps_<=globalPos[0] && globalPos[0]<=2500+eps_) && 
            (1000-eps_   <=globalPos[1] && globalPos[1]<= 3000+eps_))
           return true;

        if ((3500-eps_<=globalPos[0] && globalPos[0]<=3750+eps_) && 
            ( 500-eps_<=globalPos[1] && globalPos[1]<= 3000+eps_))
           return true;
        
        return false;
    }
private:

    MaterialLawParams materialParams_;

    PermeabilityKozenyCarman<PermeabilityType> permLaw_;

    Scalar referencePorosity_;
    Scalar irreducibleLiqSat_;
    Scalar irreducibleGasSat_;
};

} // end namespace Dumux

#endif
