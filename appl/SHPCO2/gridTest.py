import numpy as np
import math


nx,ny = (19,12)
Dx,Dy = (250,250)
Dz = 100
x = np.linspace(0,4750,nx+1)
y = np.linspace(0,3000,ny+1)

xCenter = x[1:] - Dx/2
yCenter = y[1:] - Dy/2
cellCenter = ((px,py) for px in xCenter for py in yCenter)
cellVolume = Dx*Dy*Dz

gasZoneCenter = np.array([1500,2250])
radius = 3/4*1000*math.sqrt(2)

numPointInZone = 0

def inRange(min,max,value):
    if value <= max and value >= min:
        return True
    else:
        return False

def isCellWall(point):
    point = np.array(point)
    px = point[0]
    py = point[1]
    if inRange(1000,1250,px) and inRange(0,2000,py):
        return True
    elif inRange(2250,2500,px) and inRange(1000,3000,py):
        return True
    elif inRange(3500,3750,px) and inRange(500,3000,py):
        return True
    else:
        return False

def isCellInZone(point):
    point = np.array(list(point))
    dis = np.linalg.norm(point-gasZoneCenter)
    return dis <= radius


for point in cellCenter:
    if(isCellInZone(point) and not isCellWall(point)):
        numPointInZone += 1

print(numPointInZone)