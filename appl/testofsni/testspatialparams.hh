// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Spatial parameters for the MECBM problem where CH4 is produced
 * with a biofilm conversion from coal and amendment in a coal-bed.
 */
#ifndef DUMUX_TEST_OF_SNI_SPATIAL_PARAMETERS_HH
#define DUMUX_TEST_OF_SNI_SPATIAL_PARAMETERS_HH

#include <dumux/material/spatialparams/fv1p.hh>

#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>

namespace Dumux {

/*!
 *
 * \brief Spatial parameters for the MECBM problem where CH4 is produced
 * with a biofilm conversion from coal and amendment in a coal-bed.
 */
template<class FVGridGeometry, class Scalar, int numFluidComps, int numActiveSolidComps>
class TestOfSNISpatialParams
: public FVSpatialParamsOneP<FVGridGeometry, Scalar,
                         TestOfSNISpatialParams<FVGridGeometry, Scalar, numFluidComps,
                                                  numActiveSolidComps>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

    using ParentType = FVSpatialParamsOneP<FVGridGeometry, Scalar,
                                       TestOfSNISpatialParams<FVGridGeometry, Scalar, numFluidComps, numActiveSolidComps>>;


    using CoordScalar = typename GridView::ctype;
    enum { dimWorld=GridView::dimensionworld };
    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;

    using PoroLaw = PorosityPrecipitation<Scalar, numFluidComps, numActiveSolidComps>;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Tensor;


    TestOfSNISpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        solubilityLimit_     = getParam<Scalar>("SpatialParams.SolubilityLimit", 0.26);
        initialPorosity_     = getParam<Scalar>("SpatialParams.Porosity", 0.11);
        initialPermeability_ = getParam<Scalar>("SpatialParams.Permeability", 2.23e-14);

    }

    template<class SolidSystem, class ElementSolution>
        Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    { return 1-referencePorosity(element, scv); }

    /*!
     *  \brief Define the initial porosity \f$[-]\f$ distribution
     *  For this special case, the initialPorosity is precalculated value
     *  as a referencePorosity due to an initial volume fraction of biofilm.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return referencePorosity_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
     *  This value calculates the correct porosity that needs to be set
     *  in for the initialPorosity when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePorosity from the porosityprecipitation law.
     *  The reference porosity is calculated as:
     *  \f[referencePorosity = initialPorosity + \sum \phi \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
     *
     * \param fvGridGeometry The fvGridGeometry
     * \param sol The (initial) solution vector
     */
    template<class SolutionVector>
    void computeReferencePorosity(const FVGridGeometry& fvGridGeometry,
                                  const SolutionVector& sol)
    {
        referencePorosity_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePorosity_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& globalPos = scv.dofPosition();
                const bool isGasTrap = isGasTrap_(globalPos);
                auto phi = isGasTrap ? 0.95 : initialPorosity_;
                auto phiEvaluated = poroLaw_.evaluatePorosity(element, scv, elemSol, phi);
                referencePorosity_[eIdx][scv.indexInElement()] = calculatephiRef(phi, phiEvaluated);
            }
        }
    }

    /*!
     *  \brief Define the minimum porosity \f$[-]\f$ after clogging caused by mineralization
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        const auto refPoro = referencePorosity(element, scv);
        return poroLaw_.evaluatePorosity(element, scv, elemSol, refPoro);
    }

    /*!
     *  \brief Define the initial permeability \f$[m^2]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    PermeabilityType referencePermeability(const Element& element,
                                           const SubControlVolume &scv) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isGasTrap_(globalPos))
            return 1e-2;

        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return referencePermeability_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
     *  This value calculates the correct porosity that needs to be set
     *  in for the initialPorosity when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePorosity from the porosityprecipitation law.
     *  The reference porosity is calculated as:
     *  \f[referencePorosity = initialPorosity + \sum \phi \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
     *
     * \param fvGridGeometry The fvGridGeometry
     * \param sol The (initial) solution vector
     */
    template<class SolutionVector>
    void computeReferencePermeability(const FVGridGeometry& fvGridGeometry,
                                      const SolutionVector& sol)
    {
        referencePermeability_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            const auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePermeability_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                auto kInit = initialPermeability_;
                PermeabilityType K (0.0);
                for (int i = 0; i < dimWorld; ++i)
                    K[i][i] = kInit;
                const auto refPoro = referencePorosity(element, scv);
                const auto poro = porosity(element, scv, elemSol);
                auto kEvaluated = permLaw_.evaluatePermeability(K, refPoro, poro);
                K *= K[0][0]/kEvaluated[0][0];
                referencePermeability_[eIdx][scv.indexInElement()] = K;
            }
        }
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto refPerm = referencePermeability(element, scv);
        const auto refPoro = referencePorosity(element, scv);
        const auto poro = porosity(element, scv, elemSol);
        return permLaw_.evaluatePermeability(refPerm, refPoro, poro);
    }

    Scalar solidity(const SubControlVolume &scv) const
    { return 1.0 - porosityAtPos(scv.center()); }

    Scalar solubilityLimit() const
    { return solubilityLimit_; }

    Scalar theta(const SubControlVolume &scv) const
    { return 10.0; }



private:

   Scalar calculatephiRef(Scalar phiInit, Scalar phiEvaluated)
   { return 2*phiInit - phiEvaluated;}

   bool isGasTrap_(const GlobalPosition &globalPos) const
   { return globalPos[dimWorld-1] > 0.13858;}


    PermeabilityKozenyCarman<PermeabilityType> permLaw_;
    PoroLaw poroLaw_;
    Scalar solubilityLimit_;

    Scalar initialPorosity_;
    std::vector< std::vector<Scalar> > referencePorosity_;
    Scalar initialPermeability_;
    std::vector< std::vector<PermeabilityType> > referencePermeability_;

};

} // end namespace Dumux

#endif
