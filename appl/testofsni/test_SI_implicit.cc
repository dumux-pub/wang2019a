// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the two-phase n-component finite volume model used to model e.g. salt dissolution.
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <math.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "testProblemSIImplicitTransport.hh" // insert the needed problem file here
#include "testProblemSIImplicitReaction.hh" // insert the needed problem file here

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TestOfSICCTpfaTypeTag;
    using ReactionTypeTag = Properties::TTag::TestOfSIReactionCCTpfaTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the reaction and transport problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    using ReactionProblem = GetPropType<ReactionTypeTag, Properties::Problem>;
    auto reactionProblem = std::make_shared<ReactionProblem>(fvGridGeometry);

    // the solution vector (use same solution vector for flow/transport and reaction)
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    using ReactionSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    ReactionSolutionVector r(fvGridGeometry->numDofs());
    reactionProblem->applyInitialSolution(r);
    auto rOld = r;

    // initialize the spatialParams separately to compute the referencePorosity and referencePermeability
    problem->spatialParams().computeReferencePorosity(*fvGridGeometry, x);
    problem->spatialParams().computeReferencePermeability(*fvGridGeometry, x);
    reactionProblem->spatialParams().computeReferencePorosity(*fvGridGeometry, r);
    reactionProblem->spatialParams().computeReferencePermeability(*fvGridGeometry, r);

    // the grid variables for both problems
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    using ReactionGridVariables = GetPropType<ReactionTypeTag, Properties::GridVariables>;
    auto reactionGridVariables = std::make_shared<ReactionGridVariables>(reactionProblem, fvGridGeometry);
    reactionGridVariables->init(r);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    auto episode = getParam<Scalar>("TimeLoop.Episode",1);
    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());

    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields


    //add specific output for k, cell-volume, moloutflux
    vtkWriter.addField(problem->getKxx(), "Kxx");
    vtkWriter.addField(problem->getKyy(), "Kyy");
    vtkWriter.addField(problem->getKzz(), "Kzz");
    vtkWriter.addField(problem->getReactionRate(0),"CaSO4Rate");
    vtkWriter.addField(problem->getReactionRate(1),"CaRate");
    vtkWriter.addField(problem->getIterNum(),"IterNum");
    vtkWriter.addField(problem->cellVolume(), "cellVolume");

    // update the output fields before writex
    problem->updateVtkOutput(x);
    vtkWriter.write(0.0);

    // instantiate time loop
    int episodeIdx_ = 0;
    int reactionEpisodeIdx_ = 0;
    Scalar timeLoopEpisodeLength = 0;
    // set the time loop with episodes of various lengths as specified in the injection file
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    std::vector<Scalar> epiEnd_;

    // do it with episodes if an injection parameter file is specified
    // read parameters and set checkpoint
    if (hasParam("Injection.InjectionParamFile"))
    {
        //read injection parameters
        // first read the times of the checkpoints from the injection file
        std::string injectionParameters_ = getParam<std::string>("Injection.InjectionParamFile");
        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        Scalar tempTime = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while (!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while (!injectionData.eof())
        {
            getline(injectionData, row);
            if (row == "EndTimes")
            {
                getline(injectionData, row);
                while (row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempTime;
                        // injectionParameters_ contains the time in days!
                        epiEnd_.push_back(tempTime*3600*24);
                    }
                    getline(injectionData, row);
                }
            }
        }
        injectionData.close();

        // check the injection data against the number of injections specified in the parameter file
        int numInjections_ = getParam<Scalar>("Injection.numInjections");
        if (epiEnd_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection end times specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numEpisodes from injection data file = "<<epiEnd_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        // set the episode ends /check points:
        for (int epiIdx = 0; epiIdx < epiEnd_.size(); ++epiIdx)
        {
            timeLoop->setCheckPoint(epiEnd_[epiIdx]);
        }
                
        if (hasParam("TimeLoop.EpisodeLength"))
            {
                timeLoop->setPeriodicCheckPoint(tEnd/getParam<Scalar>("TimeLoop.EpisodeLength"));
            }
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
        reactionProblem->setEpisodeIdx(reactionEpisodeIdx_);
    }

        // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        timeLoopEpisodeLength = getParam<Scalar>("TimeLoop.EpisodeLength");
        timeLoop->setPeriodicCheckPoint(tEnd/timeLoopEpisodeLength);
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
        reactionProblem->setEpisodeIdx(reactionEpisodeIdx_);
    }    // no episodes
    else
    {
        timeLoop->setCheckPoint(tEnd);
    }

    //set Assembler, LinearSolver, NewtonSolver for transport problem
    // the assembler with time loop for transport problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver for transport problem
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver for transport problem
    using PMNewtonSolver = NewtonSolver<Assembler, LinearSolver>;
    PMNewtonSolver nonLinearSolver(assembler, linearSolver);

    //set Assembler, LinearSolver, NewtonSolver for reaction problem
    using ReactionAssembler = FVAssembler<ReactionTypeTag, DiffMethod::numeric>;
    auto  reactionAssembler = std::make_shared<ReactionAssembler>(reactionProblem, fvGridGeometry, reactionGridVariables, timeLoop);

    using ReactionLinearSolver = AMGBackend<ReactionTypeTag>;
    auto  reactionLinearSolver = std::make_shared<ReactionLinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    using ReactionNewtonSolver = NewtonSolver<ReactionAssembler, ReactionLinearSolver>;
    ReactionNewtonSolver reactionNonLinearSolver(reactionAssembler, reactionLinearSolver);

    int writeTimeStep = getParam<Scalar>("Limit.writeTime"); // write vtk at certain time
    int writeTime = writeTimeStep;
    Scalar iterErrorLimit = getParam<Scalar>("Limit.iterErrorLimit"); // write vtk at certain time
    Scalar iterNumLimit = getParam<Scalar>("Limit.iterNumLimit");
    bool ifAdaptive = getParam<bool>("Limit.Adaptive");
    bool ifWriteIterLog = getParam<bool>("Limit.WriteLog");
    bool ifWriteIterDetail = getParam<bool>("Limit.WriteDetail");
    // time loop
    timeLoop->start();
    do
    {
        timeStepStart:
        // set time for problem for implicit Euler scheme
        problem->setTime(timeLoop->time() + timeLoop->timeStepSize());
        problem->setTimeStepSize(timeLoop->timeStepSize());

        reactionProblem->setTime( timeLoop->time() + timeLoop->timeStepSize() );
        reactionProblem->setTimeStepSize( timeLoop->timeStepSize() );
        
        //problem->resetSourceTerm();

        int iterNum = 0;
        auto xLastIter = xOld;
        Scalar errorLastIter = 0;
        bool notConverged = true;
   
        //start iterative method here
        do
        {
            // set previous solution for storage evaluations
            assembler->setPreviousSolution(xOld);

            // solve the non-linear system with time step control
            nonLinearSolver.solve(x, *timeLoop);
            gridVariables->advanceTimeStep();

            rOld = x;
            reactionAssembler->setPreviousSolution(rOld);
            reactionNonLinearSolver.solve(r,*timeLoop);
            reactionGridVariables->advanceTimeStep();

            // set the source term
            problem->setSourceTerm(reactionProblem->returnSourceTerm());

            // check convergence of error after the first iteration
            if(iterNum >0)
            {
                Scalar error = 0.0;
                int maxI = 0;
                int maxJ = 0;

                //calculate the max iterative error
                for (int i = 0; i < x.size(); ++i){
                    
                    for (int j = 0; j < x[i].size(); ++j){

                        Scalar localError = std::abs(x[i][j] - xLastIter[i][j]) / x[i][j];

                        if (error < localError){
                            error = localError;
                            maxI = i;
                            maxJ = j;
                        }

                    }
                }
                if(ifWriteIterDetail){
                    problem->writeIterDetail(iterNum,maxI,maxJ,error,x);
                }
                

                // if creterian achieved
                if(error < iterErrorLimit){
                    notConverged = false;
                }
                
                // if adaptive timesteping is used
                if (ifAdaptive == true)
                {
                    // if error doesnt converge 
                    if (errorLastIter < error && iterNum > 1)
                    {
                        //set halb timestep
                        std::cout << "\n iterative error did not converge, cut time step size in half." << std::endl;
                        timeLoop->setTimeStepSize(timeLoop->timeStepSize() / 2);
                        Scalar initialDt = getParam<Scalar>("TimeLoop.DtInitial");

                        //quit if time step too small
                        if (timeLoop->timeStepSize()  < 0.001 * initialDt)
                        {
                            std::cout << "invalid time step size" << std::endl;
                            exit(-22);
                        }

                        //restart time step
                        goto timeStepStart;
                    }
                }

                errorLastIter = error;
            } // end of iternum > 0

            xLastIter = x;
            ++iterNum;

            //if exceeds iteration limit
            if (iterNum > iterNumLimit)
            {

             if(ifAdaptive){
                    //set halb timestep
                    std::cout << "maximal iteration limit is exceeded, cut time step size in half." << std::endl;
                    timeLoop->setTimeStepSize(timeLoop->timeStepSize() / 2);
                    Scalar initialDt = getParam<Scalar>("TimeLoop.DtInitial");

                    //quit if time step too small
                    if (timeLoop->timeStepSize() / 2 < 0.001 * initialDt)
                    {
                        std::cout << "invalid time step size" << std::endl;
                        exit(-22);
                    }

                    //restart time step
                    goto timeStepStart;
                }
                // no adaptive
                else{
                    std::cout<<"exceeds the iteration limit"<<std::endl;
                    exit(-22);
                }
                //restart time step
                goto timeStepStart;
            } // end of itertation limit detection
        }
        while(notConverged);

        problem->setIterNum(iterNum);
        reactionGridVariables->advanceTimeStep();

        // make the new solution the old solution
        xOld = x;

        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // update the output fields before write
        problem->updateVtkOutput(x);

        if(timeLoop->time() >= writeTime || timeLoop->isCheckPoint()){
            // write vtk output
            vtkWriter.write(timeLoop->time()/86400);
            writeTime += writeTimeStep;
        }

        if(ifWriteIterLog){
            problem->writeIter();//maxI,maxJ,error,x);
        }


        // report statistics of this time step
        timeLoop->reportTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        //if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength"))
        if (hasParam("Injection.InjectionParamFile") || hasParam("TimeLoop.EpisodeLength"))
            //if(timeLoop->isCheckPoint())
            if (timeLoop->time() == epiEnd_[0])
            {
                int injType = problem->injectionType(episodeIdx_);
                std::cout<< "\n Episode " << episodeIdx_
                         << " with injectionType " << injType
                         << " done \n" << std::endl;
                episodeIdx_++;
                problem->setEpisodeIdx(episodeIdx_);
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(dt));
                epiEnd_.erase(epiEnd_.begin());
            }

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main

catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
              "). Most likely, the DGF file name is wrong "
              "or the DGF file is corrupted, "
              "e.g. missing hash at end of file or wrong number (dimensions) of entries."
              << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
