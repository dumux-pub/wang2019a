    // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
#ifndef DUMUX_TEST_OF_SNI_PROBLEM_SI_HH
#define DUMUX_TEST_OF_SNI_PROBLEM_SI_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/1pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/CaSO4FluidSystem.hh>
#include <dumux/material/solidsystems/CaSO4SolidSystem.hh>

#include "testspatialparams.hh"


namespace Dumux {
/*!
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
    template <class TypeTag>
    class TestOfSIProblem;

    namespace Properties {
// Create new type tags
        namespace TTag {
            struct TestOfSITypeTag { using InheritsFrom = std::tuple<OnePNCMin>; };
            struct TestOfSIBoxTypeTag { using InheritsFrom = std::tuple<TestOfSITypeTag, BoxModel>; };
            struct TestOfSICCTpfaTypeTag { using InheritsFrom = std::tuple<TestOfSITypeTag, CCTpfaModel>; };
        } // end namespace TTag

// Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::TestOfSITypeTag> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::TestOfSITypeTag> { using type = TestOfSIProblem<TypeTag>; };

// Set fluid configuration
        template<class TypeTag>
        struct FluidSystem<TypeTag, TTag::TestOfSITypeTag> {
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = FluidSystems::CaSO4Fluid<Scalar>;
        };

        template<class TypeTag>
        struct SolidSystem<TypeTag, TTag::TestOfSITypeTag> {
            using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
            using type = SolidSystems::CaSO4Solid<Scalar>;
        };

// Set the spatial parameters
        template<class TypeTag>
        struct SpatialParams<TypeTag, TTag::TestOfSITypeTag>{
            using MT = GetPropType<TypeTag, ModelTraits>;
            static constexpr int numFluidComps = MT::numFluidComponents();
            static constexpr int numActiveSolidComps = MT::numSolidComps() - MT::numInertSolidComps();
            using type = TestOfSNISpatialParams<GetPropType<TypeTag, FVGridGeometry>, GetPropType<TypeTag, Scalar>, numFluidComps, numActiveSolidComps>;
        };

//Set properties here to override the default property settings
        template<class TypeTag>
        struct ReplaceCompEqIdx<TypeTag, TTag::TestOfSITypeTag> { static constexpr int value = 9; }; //!< Replace gas balance by total mass balance

// Default formulation is pw-Sn, overwrite if necessary
//        template<class TypeTag>
//        struct Formulation<TypeTag, TTag::TestOfSITypeTag>
//        { static constexpr auto value = TwoPFormulation::p0s1; };

// Enable caching or not (reference solutions created without caching)
// SET_BOOL_PROP(TestOfSITypeTag, EnableFVGridGeometryCache, true);
// SET_BOOL_PROP(TestOfSITypeTag, EnableGridVolumeVariablesCache, true);
// SET_BOOL_PROP(TestOfSITypeTag, EnableGridFluxVariablesCache, true);

    }

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water is injected to flush precipitated salt in a gas reservoir clogged due to precipitated salt.
 *
 * The domain is sized 10m times 20m and contains a vertical low-permeable layer of precipitated salt near an extraction well.
 *
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2pncmin</tt>
 */
    template <class TypeTag>
    class TestOfSIProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
        using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
//        using Chemistry = typename Dumux::MECBMReactionsChemistry<TypeTag>;
        using Indices = OnePNCIndices;

        enum {
            pressureIdx = Indices::pressureIdx,

            //Indices of the phases
            phase0Idx = FluidSystem::phase0Idx,

            //Indices of the components
            numComponents = FluidSystem::numComponents,
            comp0Idx    = FluidSystem::H2OIdx,
            comp1Idx    = FluidSystem::CaIdx,
            comp2Idx    = FluidSystem::SO4Idx,
            CaIdx       = FluidSystem::CaIdx,
            SO4Idx      = FluidSystem::SO4Idx,

            //Indices of the bio/coal volume fractions
            CaSO4Idx    = SolidSystem::CaSO4PhaseIdx + numComponents,

            //Index of the primary component of G and L phase
            conti0EqIdx = Indices::conti0EqIdx,


            // Grid and world dimension
            dim      = GridView::dimension,
            dimWorld = GridView::dimensionworld,
        };

        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
        using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
        using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
        using Element = typename GridView::template Codim<0>::Entity;
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
        using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;
        using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
        using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
        using CoordScalar = typename GridView::ctype;
        using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;


        //! property that defines whether mole or mass fractions are used
        static constexpr bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
        static constexpr auto numPhases = FluidSystem::numPhases;

    public:
        TestOfSIProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
                : ParentType(fvGridGeometry)
        {
            name_                   = "imp_"+getParam<std::string>("Problem.Name");
            temperature_            = getParam<Scalar>("Problem.Temperature");
            reservoirPressure_      = getParam<Scalar>("Problem.ReservoirPressure");

            initCaSO4_              = getParam<Scalar>("Initial.CaSO4");
            initCa_                 = getParam<Scalar>("Initial.Ca");
            initSO4_                = getParam<Scalar>("Initial.SO4");

            numInjections_          = getParam<int>("Injection.numInjections");
            injectionParameters_    = getParam<std::string>("Injection.InjectionParamFile");
            injQ_                   = getParam<Scalar>("Injection.injQ");
            CaRate_                 = getParam<Scalar>("Injection.CaRate");

            kCa                     = getParam<Scalar>("Reaction.kCa");
            kCaSO4                  = getParam<Scalar>("Reaction.kCaSO4");

            nTemperature_           = getParam<int>("FluidSystem.NTemperature");
            nPressure_              = getParam<int>("FluidSystem.NPressure");
            pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
            pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
            temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
            temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");

            std::ifstream injectionData;
            std::string row;
            injectionData.open( injectionParameters_); // open the Injection data file
            if (not injectionData.is_open())
            {
                std::cerr << "\n\t -> Could not open file '"
                          << injectionParameters_
                          << "'. <- \n\n\n\n";
                exit(1) ;
            }
            int tempType = 0;

            // print file to make sure it is the right file
            std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
            while(!injectionData.eof())
            {
                getline(injectionData, row);
                std::cout << row << std::endl;
            }
            injectionData.close();

            // read data from file
            injectionData.open(injectionParameters_);

            while(!injectionData.eof())
            {
                getline(injectionData, row);

                if(row == "InjectionTypes")
                {
                    getline(injectionData, row);
                    while(row != "#")
                    {
                        if (row != "#")
                        {
                            std::istringstream ist(row);
                            ist >> tempType;
                            injType_.push_back(tempType);
                            std::cout << "size of injType: "<<injType_.size() << std::endl;
                        }
                        getline(injectionData, row);
                    }
                }
            }

            injectionData.close();

            if (injType_.size() != numInjections_)
            {
                std::cerr <<  "numInjections from the parameterfile and the number of injection types specified in the injection data file do not match!"
                          <<"\n numInjections from parameter file = "<<numInjections_
                          <<"\n numInjTypes from injection data file = "<<injType_.size()
                          <<"\n Abort!\n";
                exit(1) ;
            }

            unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
            Kxx_.resize(fvGridGeometry->gridView().size(codim));
            Kyy_.resize(fvGridGeometry->gridView().size(codim));
            Kzz_.resize(fvGridGeometry->gridView().size(codim));
            iterNum_.resize(fvGridGeometry->gridView().size(codim));
            cellVolume_.resize(fvGridGeometry->gridView().size(codim));
            sourceTerm_.resize(fvGridGeometry->gridView().size(codim));
            resetSourceTerm();
            reactionRate_.resize(2);
            for(auto& q:reactionRate_){
                q.resize(fvGridGeometry->gridView().size(codim));
            }



            FluidSystem::init(/*Tmin=*/temperatureLow_,
                    /*Tmax=*/temperatureHigh_,
                    /*nT=*/nTemperature_,
                    /*pmin=*/pressureLow_,
                    /*pmax=*/pressureHigh_,
                    /*np=*/nPressure_);
        }

        void setTime( Scalar time )
        {
            time_ = time;
        }

        void setTimeStepSize( Scalar timeStepSize )
        {
            timeStepSize_ = timeStepSize;
        }

        void setEpisodeIdx( Scalar epiIdx )
        {
            episodeIdx_ = epiIdx;
        }

        int injectionType( Scalar epiIdx )
        {
            return injType_[epiIdx];
        }


        /*!
         * \name Problem parameters
         */


        /*!
         * \brief The problem name.
         *
         * This is used as a prefix for files generated by the simulation.
         */
        const std::string& name() const
        { return name_; }

        /*!
         * \brief Returns the temperature within the domain.
         *
         * This problem assumes a temperature of 10 degrees Celsius.
         */
        Scalar temperature() const
        { return temperature_; }

        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment.
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            // default to Neumann
            bcTypes.setAllNeumann();

//        const Scalar zMax = this->fvGridGeometry().bBoxMax()[dim-1];
//        if (globalPos[1] > zMax - eps_ )
//            bcTypes.setDirichlet(pressureIdx);

            return bcTypes;
        }

        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);

            return priVars;
        }

        /*!
         * \brief Evaluate the boundary conditions for a neumann
         *        boundary segment.
         *
         * This is the method for the case where the Neumann condition is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The neumann values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scvf The sub control volume face
         *
         * For this method, the \a values parameter stores the flux
         * in normal direction of each phase. Negative values mean influx.
         * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
         */
        NumEqVector neumann(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf) const
        {
            NumEqVector flux(0.0);

            const auto& ipGlobal = scvf.ipGlobal();
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];


            Scalar area = M_PI*0.0525*0.0525/4;

            Scalar waterFlux = injQ_/area;//*0.49/area;// [m/s]

            //! Inflow boundary at bottom

            int injProcess = injType_[episodeIdx_];

            if(ipGlobal[dimWorld-1] < eps_) // standard: no injection
            {
                if (injProcess == -99) // no injection
                {
                    flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx) / FluidSystem::molarMass(comp0Idx); //mol/[m2s]
//                std::cout<<"InFlux-99 \n"<<flux <<std::endl;

                }
                else if (injProcess == 1) //water injection
                {
                    flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux1 \n"<<flux <<std::endl;

//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
                }
                else if (injProcess == 2) //water injection with Ca
                {
                flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux2 \n"<<flux <<std::endl;
                flux[comp1Idx] = waterFlux * CaRate_ / area; //[m3/s * mol/m3 / m2]

                }
                else if (injProcess == 0) // water injection
                {
                    flux = 0.0;
//                flux[comp0Idx] = 0;
//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
                }
            }
            else
                flux = 0.0; // kg/m/s

            // no-flow everywhere except at the top boundary
            if(ipGlobal[dimWorld-1] < this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps_)
                return flux;

            //! Outflow boundary at top

            // set a fixed pressure on the top of the domain
            const Scalar dirichletPressure = reservoirPressure_;

            // evaluate the gradient
            const auto gradient = [&](int phaseIdx)->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();
                const Scalar scvCenterPressureSol = volVars.pressure(phaseIdx);
//            auto grad = ipGlobal - scvCenter;
                auto grad = scvf.unitOuterNormal();
//            grad /= grad.two_norm2(); // ?
                grad *= -(dirichletPressure - scvCenterPressureSol)/ (ipGlobal -scvCenter).two_norm();
                return grad;
            };

            const Tensor K = volVars.permeability();
            for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx )
            {
                Scalar tpfaFlux = 0;
                for (int compIdx = 0; compIdx < numComponents; ++compIdx )
                {
                    const Scalar density = useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx);

                    // calculate the flux
                    tpfaFlux = vtmv(gradient(phaseIdx), K, scvf.unitOuterNormal());
                    auto gravity = this->gravity();
                    gravity *= volVars.density(phaseIdx);
                    tpfaFlux += vtmv(gravity, K, scvf.unitOuterNormal());
                    tpfaFlux *=  density * volVars.mobility(phaseIdx) * scvf.area();
                    //for safety: enforce outflow only in case some newton iteration goes wrong
                    if (tpfaFlux < 0)
                        std::cout<<"tpfaFlux"<<std::endl;
//                   tpfaFlux = 0;

                    // emulate an outflow condition for the component transport on the top side
                    tpfaFlux  *= (useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx));
                    flux[compIdx] += tpfaFlux;
                }
            }
//        std::cout<<"TopFlux \n"<<flux <<std::endl;

            return flux;
        }

        /*!
         * \brief Evaluate the boundary conditions for a dirichlet
         *        boundary segment.
         */

        /*!
         * \brief Evaluate the initial value for a control volume.
         *
         * \param globalPos The global position
         *
         * For this method, the \a values parameter stores primary
         * variables.
         */
        PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
        {
            PrimaryVariables priVars(0.0);

            priVars[pressureIdx]  = reservoirPressure_;
            priVars[CaIdx] = initCa_;
            priVars[CaSO4Idx] = initCaSO4_;
            priVars[SO4Idx] = initSO4_;

            return priVars;
        }

        /*!
         * \name Volume terms
         */
        // \{

        /*!
         * \brief Evaluate the source term for all phases within a given
         *        sub-control-volume.
         *
         * This is the method for the case where the source term is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The source and sink values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scv The subcontrolvolume
         *
         * For this method, the \a values parameter stores the conserved quantity rate
         * generated or annihilate per volume unit. Positive values mean
         * that the conserved quantity is created, negative ones mean that it vanishes.
         * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
         */
        NumEqVector source(const Element &element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const SubControlVolume &scv) const
        {
            NumEqVector source(0.0);
            source = sourceTerm_[scv.dofIndex()];
//            Scalar cCa  = volVars.moleFraction(phase0Idx, CaIdx)  * volVars.molarDensity(phase0Idx);    //[mol ca/m³_waterphase]
//            Scalar cSO4 = volVars.moleFraction(phase0Idx, SO4Idx) * volVars.molarDensity(phase0Idx);    //[mol SO4/m³_waterphase]
//            Scalar CaSO4PhaseIdx = CaSO4Idx - numComponents;
//            Scalar cCaSO4 = volVars.solidVolumeFraction(CaSO4PhaseIdx) * volVars.solidComponentMolarDensity(CaSO4PhaseIdx);
//
//            source[CaIdx]  = kCaSO4 * cCaSO4 - kCa * cSO4 * cCa;
//            source[SO4Idx] = kCaSO4 * cCaSO4 - kCa * cSO4 * cCa;
//            source[CaSO4Idx] = -kCaSO4 * cCaSO4 + kCa * cSO4 * cCa;

            return source;
        }

        /*!
         * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
         */

        const std::vector<Scalar>& getKxx()
        {
            return Kxx_;
        }

        const std::vector<Scalar>& getKyy()
        {
            return Kyy_;
        }

        const std::vector<Scalar>& getKzz()
        {
            return Kzz_;
        }

        const std::vector<Scalar>& cellVolume()
        {
            return cellVolume_;
        }

        const auto& getIterNum()
        {
            return iterNum_;
        }

        void setIterNum(const int& i)
        {
            for(auto& iter : iterNum_)
            {
                iter = i;
            }
        }


        /*!
         * \brief Return how much the domain is extruded at a given sub-control volume.
         *
         * This means the factor by which a lower-dimensional (1D or 2D)
         * entity needs to be expanded to get a full dimensional cell. The
         * default is 1.0 which means that 1D problems are actually
         * thought as pipes with a cross section of 1 m^2 and 2D problems
         * are assumed to extend 1 m to the back.
         */
        template<class ElementSolution>
        Scalar extrusionFactor(const Element &element,
                               const SubControlVolume &scv,
                               const ElementSolution& elemSol) const
        {
            const auto radius = 0.0525/2;
            return M_PI*radius*radius;
        }

        void updateVtkOutput(const SolutionVector& curSol)
        {
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {
                const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    const auto dofIdxGlobal = scv.dofIndex();
                    Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                    Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
                    Kzz_[dofIdxGlobal] = volVars.permeability()[2][2];
                    cellVolume_[dofIdxGlobal] = element.geometry().volume();
                }
            }
        }

        void setSourceTerm(const std::vector<NumEqVector>& sourceTerm){
            sourceTerm_ = sourceTerm;
        }
        template<class GridVaribales>
        void calculateSourceTerm(GridVaribales& gridVariables, SolutionVector& sol){
            for (const auto& element : Dune::elements(this->gridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVariables->curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                for (const auto& scv : scvs(fvGeometry))
                {
                    const auto scvIdx = scv.dofIndex();
                    const auto& volVars = elemVolVars[scv];
                    auto& scvSolution = sol[scvIdx];

                    NumEqVector source(0.0);

                    Scalar cCa  = volVars.moleFraction(phase0Idx, CaIdx)  * volVars.molarDensity(phase0Idx);    //[mol ca/m³_waterphase]
                    Scalar cSO4 = volVars.moleFraction(phase0Idx, SO4Idx) * volVars.molarDensity(phase0Idx);    //[mol SO4/m³_waterphase]
                    Scalar CaSO4PhaseIdx = CaSO4Idx - numComponents;
                    Scalar cCaSO4 = volVars.solidVolumeFraction(CaSO4PhaseIdx) * volVars.solidComponentMolarDensity(CaSO4PhaseIdx);

                    source[CaIdx]  = kCaSO4 * cCaSO4 - kCa * cSO4 * cCa;
                    source[SO4Idx] = kCaSO4 * cCaSO4 - kCa * cSO4 * cCa;
                    source[CaSO4Idx] = -kCaSO4 * cCaSO4 + kCa * cSO4 * cCa;

                    reactionRate_[0][scv.dofIndex()] = kCaSO4 * cCaSO4;
                    reactionRate_[1][scv.dofIndex()] = kCa * cSO4 * cCa;
                    sourceTerm_[scvIdx] = source;
                }//end of scv
            }//end of element

        }

        const std::vector<Scalar>& getReactionRate(const int& i){
            return reactionRate_[i];
        }

        void resetSourceTerm()
        {
            for(auto& q:sourceTerm_){
                NumEqVector source(0.0);
                q = source;
            }
        }

        void writeIter()
        {
            // filename of the output file
            std::string fileName = this->name();
            fileName += ".dat";
            std::ofstream dataFile;
            if (writeName)
            {
                dataFile.open(fileName.c_str());
                                dataFile << "time timestep iterNum \n ";//cellNumber PVNum relativeError pv1 pv2 pv3 pv4\n";
                dataFile.close();
                writeName = false;
            }

            // const Scalar time = time_;

            dataFile.open(fileName, std::ios::app);
          
            dataFile<< time_ - timeStepSize_
                    << " "
                    << timeStepSize_
                    << " "  
                    << iterNum_[0]
                    // << " "
                    // << cellNum
                    // << " "
                    // << pvNum
                    // << " "
                    // << relativeError
                    // << " "
                    // << solution[cellNum][0]
                    // << " "
                    // << solution[cellNum][1]
                    // << " "
                    // << solution[cellNum][2]
                    // << " "
                    // << solution[cellNum][3]
                    ;
            dataFile << "\n";
            dataFile.close();
        }

void writeIterDetail(const int& iterNum, const int& cellNum, const int& pvNum, const Scalar& relativeError, const SolutionVector& solution)
        {
            // filename of the output file
            std::string fileName = this->name();
            fileName += "_iter.dat";
            std::ofstream dataFile;
            if (writeName)
            {
                dataFile.open(fileName.c_str());
                                dataFile << "time timestep iterNum cellNumber PVNum relativeError pv1 pv2 pv3 pv4\n";
                dataFile.close();
                writeName = false;
            }

            // const Scalar time = time_;

            dataFile.open(fileName, std::ios::app);
          
            dataFile<< time_ - timeStepSize_
                    << " "
                    << timeStepSize_
                    << " "  
                    << iterNum
                    << " "
                    << cellNum
                    << " "
                    << pvNum
                    << " "
                    << relativeError
                    << " "
                    << solution[cellNum][0]
                    << " "
                    << solution[cellNum][1]
                    << " "
                    << solution[cellNum][2]
                    << " "
                    << solution[cellNum][3]
                    ;
            dataFile << "\n";
            dataFile.close();
        }
    private:

        /*!
         * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
         *
         * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
         */
        static Scalar massToMoleFrac_(Scalar XwNaCl)
        {
            const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(comp0Idx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
            const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

            const Scalar X_NaCl = XwNaCl;
            /* XwNaCl: conversion from mass fraction to mol fraction */
            auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
            return xwNaCl;
        }

        int nTemperature_;
        int nPressure_;
        std::string name_;

        Scalar pressureLow_, pressureHigh_;
        Scalar temperatureLow_, temperatureHigh_;
        Scalar reservoirPressure_;
        Scalar temperature_;

        Scalar initCa_;                 // Initial mole fraction of Ca-Ion
        Scalar initSO4_;                // Initial mole fraction of SO4-Ion
        Scalar initCaSO4_;              // Initial volume fraction of CaSO4

        Scalar CaRate_;                 // concentration of ca [mol/m3]

        std::vector<int> injType_;
        int numInjections_;                // number of Injections
        std::string injectionParameters_;  // injectionParamteres from file
        Scalar injQ_;                      // injected volume flux in m3/s

        Scalar time_ = 0.0;
        Scalar timeStepSize_ = 0.0;
        Scalar episodeIdx_ = 0;

        Scalar kCaSO4;
        Scalar kCa;

        static constexpr Scalar eps_ = 1e-6;
        std::vector<double> Kxx_;
        std::vector<double> Kyy_;
        std::vector<double> Kzz_;
        std::vector<double> cellVolume_;
        std::vector<NumEqVector> sourceTerm_;
        mutable std::vector<std::vector<Scalar>> reactionRate_;
        bool writeName = true;
        std::vector<double> iterNum_;
    };

} //end namespace Dumux

#endif
