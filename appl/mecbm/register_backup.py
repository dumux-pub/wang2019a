#!/usr/bin/env python
# coding: utf-8


import sys
#import import_ipynb
import os
import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt
from collections.abc import Iterable

historyFile = "/Users/ouetsu/ma/wang2019a/build-cmake/appl/testofsni/input_history.pkl"
test_file = '../implicit/onlycoalbac.input'
downloadPath = "/Users/ouetsu/Downloads/paraview"
work_path = "/Users/ouetsu/ma/wang2019a/build-cmake/appl/testofsni"

class mecbmPython:
    def __init__(self,name,HistoryFile = historyFile, DownoadPath = downloadPath,WorkPath = work_path):
        self.name = name
        self._historyFile = HistoryFile
        self._downloadPath = DownoadPath
        self._workPath = WorkPath
        self._DataPath = os.path.join(self._downloadPath,"Data")
    
    def _correctData(self):
        data = pd.read_pickle(self._historyFile)
        data = data.replace({np.nan:None})
        data.to_pickle(self._historyFile)

    def readHistory(self):
        self._correctData()
        return pd.read_pickle(self._historyFile)

    def caseName(self):
        return self.readHistory().index

    def paramName(self):
        return self.readHistory().columns

    def _strToSet(self,keywords):
        if isinstance(keywords,str):
            return set(keywords.split(","))
        return keywords
    
    def _filterKeywords(self,keywords,obj,ex_words = None, rel = True):
        keywords = self._strToSet(keywords)
        ex_words = self._strToSet(ex_words)
        
        if rel:
            func = any
        else:
            func = all

        for item in obj:

            if ex_words:
                if func(item.find(ex_word)>=0 for ex_word in ex_words):
                    continue

            if func(item.find(keyword)>=0 for keyword in keywords):
                yield item

    def historySelectedCases(self,keywords,ex_words = None): 
        data = self.readHistory()
        selected_cases = self._filterKeywords(keywords,data.index,ex_words)
        return data.loc[selected_cases]

    def historySelectedCasesColumns(self,keywords_case,keywords_column,ex_case=None,ex_column=None):  
        data = self.historySelectedCases(keywords_case,ex_case)
        selected_cols = self._filterKeywords(keywords_column,data.columns,ex_column)
        return data.loc[:,selected_cols]

    def clearHistroy(self):
        confirmed = input("are you sure to reset history?(y/n)\n")
        if confirmed == "y":
            print("reset history file")
            pd.to_pickle(pd.DataFrame(),self._historyFile)
        else:
            print("history file is safe")




    def read_params(self,path):
        lines = []
        enddict = dict()
        with open(path,"r") as f:
            lines = f.readlines()
        for line in(lines):
            line = line.split("#")[0]
            line = line.split("[")[0]
            line = line.replace(" ","")
            line = line.rstrip("\n")
            if "=" in line:
                name = line.split("=")[0]
                value = line.split("=")[1]
                if len(value) == 0:
                    value = None
                enddict[name] = value
        param = pd.Series(enddict)
        print("current name:" + param.Name)
        param["RegisteredDate"] = datetime.datetime.now()
        return param

    def compare_two_inputs(self,Series_old,Series_new):
        Series_old.name = "old"
        Series_new.name = "new"
        compare = pd.concat([Series_old,Series_new],axis=1)
        compare.replace({np.nan:None},inplace =True)
        compare = compare.loc[compare.old != compare.new]
        for idx in compare.index:
            print(idx)
            print("old:"+str(compare.loc[idx,"old"]))
            print("new:"+str(compare.loc[idx,"new"]))
            print("------------------")

    def change_filename(self,path,old_Name,new_Name):
        lines = []
        with open(path,"r") as f:
            lines = f.readlines()
            for idx, line in enumerate(lines):
                if "Name" in line:
                    lines[idx] = line.replace(old_Name, new_Name)
        with open(path,"w") as f:
            f.writelines(lines)


    def save_input(self,inputname = "test_mecbmcolumnCoal00.input"):
        path = os.getcwd()
        path = os.path.join(path,inputname)
        print(path)
        param = self.read_params(path)
        param.name = param["Name"]
        param_name = param["Name"]
        history = self.readHistory()
        ##check if already exists

        if param.name in history.index:
            print("the input already exists in current history.")
            old_input = history.loc[param.name]
            self.compare_two_inputs(old_input,param)
            param.name = param_name
            ###rename file
            val = input("do you want to change name? [y to change]")
            if val == "y":
                print ("current name: "+param.name)
                filename = input ("the new name:")
                while (filename in history.index):
                    print ("opps again existed!")
                    print ("current name: "+filename)
                    filename = input ("the new name:")
                    if filename == "exit":
                        exit()
                self.change_filename(path,param.name,filename)
                param.name = filename
                
                
            else:
                val = input("do you want to cover the record?[y to cover]")
                if val == "y":
                    history.drop(index= param_name,inplace = True)
                else: 
                    return print("registration stops")
                    
        param = pd.DataFrame(param).T
        history = pd.concat([history,param],sort = False)
        history.to_pickle(self._historyFile)
        return print("registered")

    def update_input(self,inputname = "test_mecbmcolumnCoal00.input"):
        path = os.getcwd()
        path = os.path.join(path,inputname)
        print(path)

        para_now = pd.DataFrame(self.read_params(path)).T
        name = para_now.Name[0].split("_")[0]
        para_old = self.findCase(name,only=False)

        if len(para_old.index) == 0:
            print("no case with given name found, save input now.")
            self.save_input(path)
            exit()
    
        print("the following cases will be updated:")
        [print(ind) for ind in para_old.index]

        empty_cols = para_old.columns[para_old.isnull().any(axis=0)]
        empty_cols = [col for col in empty_cols if col in para_now.columns]
        for col in empty_cols:
            print("updating term {0} with value {1}".format(col,para_now[col][0]))
            para_old[col] = para_now[col][0]

        new_cols = [col for col in para_now.columns if col not in para_old.columns]
        for col in new_cols:
            print("updating term {0} with value {1}".format(col,para_now[col][0]))
            para_old.insert(len(para_old.columns),col,para_now[col][0])

        if len(empty_cols+new_cols) == 0:
            print("nothing is to update")
        his = self.readHistory()
        g = (new_col for new_col in para_old.columns if new_col not in his.columns)
        for col in g:
            his.insert(len(his.columns),col,np.NAN)
        his.update(para_old)
        his.to_pickle(self._historyFile)

    def findCase(self,caseNames,exwords = None,rel = True, only = False):
        caseNames = self._strToSet(caseNames)
        case_param = pd.read_pickle(self._historyFile)

        if only :
            caseNames = case_param.index.isin(caseNames)
        else:
            g = self._filterKeywords(caseNames,case_param.index,exwords,rel)
            caseNames = case_param.index.isin([name for name in g])
        return case_param[caseNames]

    def resetCase2(self):
        caseName = input("which case is to reset?\n").strip()
        inputFile = input("which input file?\n").strip()
        self.resetCase(inputFile,caseName)

    def resetCase(self,inputfile,caseName):
        
        caseName = str(caseName)

        input_param = pd.DataFrame(self.read_params(inputfile)).T
        input_name = "now"
        input_param.index = [input_name]
        
        case_param = self.findCase(caseName,only = True)
        case_name = "history_"+caseName
        case_param.index = [case_name]
        
        datas = pd.concat([input_param,case_param]).T
        datas["ifsame"] =datas.apply(lambda x:x[input_name] == x[case_name],axis=1)
        
        
        diffParams_names = datas.index[np.where(datas.ifsame == False)]
        diffParams_names = diffParams_names.drop("RegisteredDate")  
        diffParams_names = [column for column in diffParams_names if column[:3] != "cpu"]
        
        lines = []
        with open(inputfile,'r') as f:
            lines = f.readlines()
            lines_name = [line.split("=")[0].strip() for line in lines]
            diffIndex = [lines_name.index(name) for name in diffParams_names] 
           
            for num, diffPos in enumerate (diffIndex):
                oldValue = datas.at[diffParams_names[num],input_name]
                newValue = datas.at[diffParams_names[num],case_name]
                print(diffParams_names[num])
                print(oldValue)
                print(newValue)
                print("----------------")
                if newValue is None:
                    newValue =""
                line = lines[diffPos].split("=")[0] + "="+str(newValue) +"\n"
                lines[diffPos] = line#lines[diffPos].replace(oldValue,newValue)
        with open(inputfile,'w') as f:
            f.writelines(lines)
            
        print("reset case {0} in {1}".format(caseName,inputfile))
    
    def setRunTime(self,caseName,method,cpuTime):
        case_param = pd.read_pickle(self._historyFile)
        if caseName  in case_param.index:
            case_param.at[case_param.index == caseName,"cpuTime_"+method] = cpuTime
            print("cputime {2}s for case {0} by method {1} is written!".format(caseName,method,cpuTime))
        case_param.to_pickle(self._historyFile)

    def setRunTime2(self):
        name = input("which case?\n").strip()
        case_param = pd.read_pickle(self._historyFile)
        if name in case_param.index:
            print("case {0} found!\n".format(name))
        else:
            print("case {0} not found!\n".format(name))
            return None
        
        method = input("which method?\n").strip()
        methodList = ["ref","imp","exp"]
        if method in methodList:
            time = input("took cpu time?\n")
            if float(time) > 0:
                self.setRunTime(name,method,time)
                return None
            else:
                print("wrong time")
                return None
        else:
            print("wrong method")
            return None

    def _deleteCase(self):
        case_list = self.caseName()
        del_case_name = input("which case will you delete?\n")
        if del_case_name in case_list:
            if_confirmed = input("are you sure to delete?(y/n)\n")  
            
            if if_confirmed == "y":
                del_name_again = input("give the case name again.\n")
                
                if del_name_again == del_case_name:
                    data = self.readHistory()
                    data.drop(del_name_again,inplace=True)
                    data.to_pickle(self._historyFile)
                    print("case {0} is deleted".format(del_case_name))
                else:
                    print("two case names are not identical,exit!")
                    exit()
            
            else:
                print("process canceled.")
                exit()
        
        else:
            print("no case with name {0} found!".format(del_case_name))
            exit()

    def _help(self):
        text = """python tool for evaluation of mecbm model. \n
        -s to register input data \n
        -c to register the cpu time\n
        -r to reset the input data from history\n
        -l to show the list of all registered cases\n
        -d to delete the data of one case"""
        print(text)

    def _showData(self,keywords,work_path,ex_words =None):
        self._filterKeywords(keywords,os.listdir(work_path),ex_words)

    def showData(self,keywords,ex_words =None,rel=True):
        filelist = self._filterKeywords(keywords,os.listdir(self._workPath),ex_words,rel)
        file_list =[]
        for file in filelist:
            print(file)
            file_list.append(file)
        return file_list

    def calIteration(self,file_name,saveName = None):
        file_name = os.path.join(self._workPath,file_name)
        
        iter_data = pd.read_csv(file_name,sep=" ",usecols=[0,1,2])
        iter_data["startTime"] = iter_data.time-iter_data.timestep  
        
        g = iter_data.groupby(iter_data.startTime)
        iter_time_list = pd.concat([g.tail(1)])
        iter_time_list.index = iter_time_list.startTime/86400
        iter_time_list.drop(["time","startTime"],axis = 1,inplace=True)
        iter_time_list.index.name = "Time"

        if saveName:
            iter_time_list.to_csv(os.path.join(self._DataPath,saveName))

        return iter_time_list

    def plot_dat_iter(self,dat_name):
        data = self.read_dat(dat_name)
        data.time /= 86400
        plt.plot(data.time,data.iterNum)

    def pathGenerator(self,file):
        return os.path.join(self._workPath,file)
    
    def read_dat(self,dat_name):
        return pd.read_csv(os.path.join(self._workPath,dat_name),sep=" ",usecols=[0,1,2])
    
    def returnLineInPvd(self,lines):
        for line in lines:
            if line.find("DataSet timestep")>= 0:
                time = removePunctuation(line)
                time = float(time.split(" ")[1].split("=")[1])
                if int(time / 1.4 )* 1.4 == time:
                    yield line
            else:
                yield line

if __name__ == '__main__':
    self = mecbmPython("self")
    path = os.getcwd()
    # [print(arg) for arg in sys.argv]
    # print(len(sys.argv))
    if len(sys.argv)==2 and sys.argv[1]=="-r":
        self.resetCase2()
    elif len(sys.argv)==2 and sys.argv[1]=="-c":
        self.setRunTime2()
    elif len(sys.argv) == 3 and sys.argv[1]=="-s":
        self.save_input(sys.argv[2])
    elif len(sys.argv) == 2 and sys.argv[1]=="-h":
        self._help()
    elif len(sys.argv) == 2 and sys.argv[1]=="-l":
        [print(name) for name in self.caseName()]
    elif len(sys.argv) == 2 and sys.argv[1]=="-d":
        self._deleteCase()
    elif len(sys.argv) == 3 and sys.argv[1]=="-u":
        self.update_input(sys.argv[2])
    else:
        self.save_input()

