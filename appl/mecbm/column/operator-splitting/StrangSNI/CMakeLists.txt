dune_add_test(NAME StrangSNI
        SOURCES StrangSNI.cc
        COMPILE_DEFINITIONS TYPETAG=MECBMColumnCCTpfaTypeTag
        COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
        CMD_ARGS --command "${CMAKE_CURRENT_BINARY_DIR}/test1 test_mecbmcolumnCoal00.input")