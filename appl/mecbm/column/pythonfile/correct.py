#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import fnmatch
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import re


# In[ ]:


def correct_paraview(pvdfile):
    path = pvdfile
    path_folder =""
    for i in  path.split("/")[0:-1]:
        path_folder = path_folder + i +"/"
    vtpfiles = []
    with open(path, 'r') as f:
        files = f.readlines()
    for file in files:
        if "file" in file:
            file = file.split("\" file=\"")[1][0:-5]
            vtpfiles.append(path_folder+file)
    for file in vtpfiles:
        with open(file,"r") as f:
            data = f.read()
            data_corrected = re.sub(r"Float32","Float64",data)
        with open(file,"w") as f:
            f.write(data_corrected)

