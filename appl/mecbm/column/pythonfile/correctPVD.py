#!/usr/bin/env python
# coding: utf-8

# In[12]:


import os
import sys
import re


# In[13]:


def correctOnePVD(pvdfile):
    path = os.getcwd()
    # read pvd file
    path = os.path.join(path,pvdfile)
    path_folder = ""
    for i in path.split("/")[0:-1]:
        path_folder = path_folder + i + "/"

    # get vtp filenames
    vtpfiles = []
    with open(path, 'r') as f:
        files = f.readlines()
    for file in files:
        if "file" in file:
            file = file.split("\" file=\"")[1][0:-5]
            vtpfiles.append(path_folder + file)

    # correct vtp files
    for file in vtpfiles:
        with open(file, "r") as f:
            data = f.read()
            data_corrected = re.sub(r"Float32", "Float64", data)
        with open(file, "w") as f:
            f.write(data_corrected)


# In[15]:


if __name__ == '__main__':
    correctOnePVD(sys.argv[1])


# In[ ]:




