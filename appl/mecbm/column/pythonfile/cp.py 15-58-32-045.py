{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from matplotlib import pyplot as plt\n",
    "import os\n",
    "import fnmatch\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import pyplot as plt\n",
    "import re\n",
    "import correct\n",
    "import glob\n",
    "import sys,os\n",
    "sys.path.append(os.path.realpath('..'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cal_CH4_sum(data_file, p = False):\n",
    "    data = pd.read_csv(data_file,sep= \" \",index_col =False)\n",
    "    data.rename(columns={\"#time\":\"time\"},inplace=True)\n",
    "    data.dropna(axis=1,inplace=True)\n",
    "    data[\"CH4w\"] = data[\"molCH4w\"]*data[\"timeStepSize\"]\n",
    "    data[\"CH4n\"] = data[\"molCH4n\"]*data[\"timeStepSize\"]\n",
    "    data[\"molCH4w_sum\"] = data.CH4w.cumsum()\n",
    "    data[\"molCH4n_sum\"] = data.CH4n.cumsum()\n",
    "    data[\"molCH4_sum\"] = data.molCH4n_sum + data.molCH4w_sum\n",
    "    data[\"time_in_day\"] = data.time/86400\n",
    "    if p :\n",
    "        for name in data.columns:\n",
    "            print(name)\n",
    "    return data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def correctOnePVD(pvdfile):\n",
    "    # read pvd file\n",
    "    path = pvdfile\n",
    "    path_folder = \"\"\n",
    "    for i in path.split(\"/\")[0:-1]:\n",
    "        path_folder = path_folder + i + \"/\"\n",
    "\n",
    "    # get vtp filenames\n",
    "    vtpfiles = []\n",
    "    with open(path, 'r') as f:\n",
    "        files = f.readlines()\n",
    "    for file in files:\n",
    "        if \"file\" in file:\n",
    "            file = file.split(\"\\\" file=\\\"\")[1][0:-5]\n",
    "            vtpfiles.append(path_folder + file)\n",
    "\n",
    "    # correct vtp files\n",
    "    for file in vtpfiles:\n",
    "        with open(file, \"r\") as f:\n",
    "            data = f.read()\n",
    "            data_corrected = re.sub(r\"Float32\", \"Float64\", data)\n",
    "        with open(file, \"w\") as f:\n",
    "            f.write(data_corrected)\n",
    "            \n",
    "def correctPVDs(pvdlist):\n",
    "    for pvd in pvdlist:\n",
    "        correctParaview(pvd)\n",
    "        \n",
    "def correctPVDSInFolder(rootpath):\n",
    "    pvdlist = find_pvd(rootpath)\n",
    "    correctPVDlist(pvdlist)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_comparison(attribute,ref,seq,ref_label = \"ref\",seq_label = \"seq\",start = 0, end = 200, yLabelUnit = \"[mol]\" ,xticks = 10, x=None):\n",
    "    mask_ref = (ref.index>start) &(ref.index <end)\n",
    "    mask_seq= (seq.index>start)&(seq.index<end)\n",
    "    ref_plot = ref[mask_ref]\n",
    "    seq_plot = seq[mask_seq]\n",
    "    plt.figure()\n",
    "    if x == None:\n",
    "        plt.scatter(ref_plot.index,ref_plot[attribute])\n",
    "        plt.scatter(seq_plot.index,seq_plot[attribute],c=\"red\")\n",
    "    else:\n",
    "        plt.scatter(ref_plot[x],ref_plot[attribute])\n",
    "        plt.scatter(seq_plot[x],seq_plot[attribute],c=\"red\")\n",
    "    plt.xlabel(xlabel=\"time[d]\",fontsize = 15)\n",
    "    plt.ylabel(ylabel= attribute+yLabelUnit,fontsize = 15)\n",
    "    plt.xticks = xticks\n",
    "    plt.xlim([start,end])\n",
    "    ymin = min(ref_plot[attribute].min(),seq_plot[attribute].min())\n",
    "    ymax = max(ref_plot[attribute].max(),seq_plot[attribute].max())\n",
    "    plt.ylim([ymin,ymax])\n",
    "    plt.legend(labels = [ref_label, seq_label], loc = 'best',fontsize = 15)\n",
    "    plt.show()\n",
    "    \n",
    "def returnParasDict(inputfile):\n",
    "    #read inputfile\n",
    "    save_para = False\n",
    "    paras = []\n",
    "    with open(inputfile) as inputdata:\n",
    "        for line in inputdata:\n",
    "            line = line.rstrip()\n",
    "            if (line == \"[BioCoefficients]\"):\n",
    "                save_para = True\n",
    "                continue\n",
    "            if ((save_para) and line ==\"\"):\n",
    "                break\n",
    "            if ((save_para)):\n",
    "                paras.append(line)\n",
    "    # creat the dictionarz\n",
    "    names = []\n",
    "    values = []\n",
    "    for para in paras:\n",
    "        name = para.split(\"=\")[0]\n",
    "        name = name.rstrip()\n",
    "        value = para.split(\"=\")[1]\n",
    "        value = value.split(\"#\")[0]\n",
    "        value = value.rstrip()\n",
    "        value = float(value)\n",
    "        names.append(name)\n",
    "        values.append(value)\n",
    "    return dict(zip(names,values))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def returntime(day):\n",
    "    print(\"{:.6E}\".format(day*86400))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def returnday(day):\n",
    "    print(\"{:.6}\".format(day/86400))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_pvd(path):\n",
    "    file = []\n",
    "    for i in os.listdir(path):\n",
    "        if \"pvd\" in i:\n",
    "            print(i)\n",
    "            file.append(os.path.join(path,i))\n",
    "    return file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_dat(path):\n",
    "    file = []\n",
    "    for i in os.listdir(path):\n",
    "        if \"dat\" in i:\n",
    "            print(i)\n",
    "            file.append(os.path.join(path,i))\n",
    "    return file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def delete_files(path,keyword,confirmed = False):\n",
    "    for file in os.listdir(path):\n",
    "        if keyword in file:\n",
    "            delete_file_path = os.path.join(path,file)\n",
    "            if(confirmed):\n",
    "                os.remove(delete_file_path)\n",
    "            print(i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
