import pandas as pd
from matplotlib import pyplot as plt
import os
import fnmatch
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import re
import correct
import glob
import sys,os
sys.path.append(os.path.realpath('..'))

def cal_CH4_sum(data_file, p = False):
    data = pd.read_csv(data_file,sep= " ",index_col =False)
    data.rename(columns={"#time":"time"},inplace=True)
    data.dropna(axis=1,inplace=True)
    data["CH4w"] = data["molCH4w"]*data["timeStepSize"]
    data["CH4n"] = data["molCH4n"]*data["timeStepSize"]
    data["molCH4w_sum"] = data.CH4w.cumsum()
    data["molCH4n_sum"] = data.CH4n.cumsum()
    data["molCH4_sum"] = data.molCH4n_sum + data.molCH4w_sum
    data["time_in_day"] = data.time/86400
    if p :
        for name in data.columns:
            print(name)
    return data

def correctParaview(pvdfile):
    # read pvd file
    path = pvdfile
    path_folder = ""
    for i in path.split("/")[0:-1]:
        path_folder = path_folder + i + "/"

    # get vtp filenames
    vtpfiles = []
    with open(path, 'r') as f:
        files = f.readlines()
    for file in files:
        if "file" in file:
            file = file.split("\" file=\"")[1][0:-5]
            vtpfiles.append(path_folder + file)

    # correct vtp files
    for file in vtpfiles:
        with open(file, "r") as f:
            data = f.read()
            data_corrected = re.sub(r"Float32", "Float64", data)
        with open(file, "w") as f:
            f.write(data_corrected)
            
def correctPVDlist(pvdlist):
    for pvd in pvdlist:
        correctParaview(pvd)
        
def correctOrder(rootpath):
    pvdlist = find_pvd(rootpath)
    correctPVDlist(pvdlist)

def plot_comparison(attribute,ref,seq,ref_label = "ref",seq_label = "seq",start = 0, end = 200, yLabelUnit = "[mol]" ,xticks = 10, x=None):
    mask_ref = (ref.index>start) &(ref.index <end)
    mask_seq= (seq.index>start)&(seq.index<end)
    ref_plot = ref[mask_ref]
    seq_plot = seq[mask_seq]
    plt.figure()
    if x == None:
        plt.scatter(ref_plot.index,ref_plot[attribute])
        plt.scatter(seq_plot.index,seq_plot[attribute],c="red")
    else:
        plt.scatter(ref_plot[x],ref_plot[attribute])
        plt.scatter(seq_plot[x],seq_plot[attribute],c="red")
    plt.xlabel(xlabel="time[d]",fontsize = 15)
    plt.ylabel(ylabel= attribute+yLabelUnit,fontsize = 15)
    plt.xticks = xticks
    plt.xlim([start,end])
    ymin = min(ref_plot[attribute].min(),seq_plot[attribute].min())
    ymax = max(ref_plot[attribute].max(),seq_plot[attribute].max())
    plt.ylim([ymin,ymax])
    plt.legend(labels = [ref_label, seq_label], loc = 'best',fontsize = 15)
    plt.show()
    
def returnParasDict(inputfile):
    #read inputfile
    save_para = False
    paras = []
    with open(inputfile) as inputdata:
        for line in inputdata:
            line = line.rstrip()
            if (line == "[BioCoefficients]"):
                save_para = True
                continue
            if ((save_para) and line ==""):
                break
            if ((save_para)):
                paras.append(line)
    # creat the dictionarz
    names = []
    values = []
    for para in paras:
        name = para.split("=")[0]
        name = name.rstrip()
        value = para.split("=")[1]
        value = value.split("#")[0]
        value = value.rstrip()
        value = float(value)
        names.append(name)
        values.append(value)
    return dict(zip(names,values))

def returntime(day):
    print("{:.6E}".format(day*86400))

def returnday(day):
    print("{:.6}".format(day/86400))

def find_pvd(path):
    file = []
    for i in os.listdir(path):
        if "pvd" in i:
            print(i)
            file.append(os.path.join(path,i))
    return file


def find_dat(path):
    file = []
    for i in os.listdir(path):
        if "dat" in i:
            print(i)
            file.append(os.path.join(path,i))
    return file