//
// Created by 王悦 on 08.01.20.
//

#ifndef WANG2019A_MECBMPHASEEQUILIBRIUM_H
#define WANG2019A_MECBMPHASEEQUILIBRIUM_H

#endif //WANG2019A_MECBMPHASEEQUILIBRIUM_H



enum{
    nw = 0; //unit [mol]
    xwH2O = 1;
    xwCH4 = 2;
    xwH2 = 3;
    ng = 4;
    xgH2O = 5;
    xgCH4 = 6;
    xgH2 = 7;

    henryH20 = 0;
    henryCH4 = 1;
    henryH2 = 2;

    /// for given condition
    nf = 0; //unit [mol]
    xfH2O = 1;
    xfCH4 = 2;
    xfH2 = 3;
};

///
/// \tparam T
/// \return
template <typename T>
T GasPressure( std::vector<T>& Ans, std::vector<T>& Henry)
{
    T gasPressure = 0.0;
    for (int compIdx = 0; compIdx <3 ; ++i) {
        gasPressure += Ans[compIdx+1] * Henry[compIdx];
    }
    return gasPressure;
}

template <typename T>
T WettingSaturation(std::vector<T>& Ans, T& rhoW , T& poreVolume)
{
   return Ans[nw]/rhoW/poreVolume;
}

template <typename T>
T eq1(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[nw] + Ans[ng] - feed[nf];
}

template <typename T>
T eq1(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[nw] + Ans[ng] - feed[nf];
}

template <typename T>
T eq2(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[nw]*Ans[xwH2O] + Ans[ng]*Ans[xgH2O] - feed[nf]*feed[xfH2O];
}

template <typename T>
T eq3(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[nw]*Ans[xwCH4] + Ans[ng]*Ans[xwCH4] - feed[nf]*feed[xfCH4];
}

template <typename T>
T eq4(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[nw]*Ans[xwH2] + Ans[ng]*Ans[xgH2] - feed[nf]*feed[xfH2];
}

template <typename T>
T eq5(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[xwCH4] + Ans[xwH2O] + Ans[xwH2] - 1;
}

template <typename T>
T eq6(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[xgCH4] + Ans[xgH2O] + Ans[xgH2] - 1;
}

template <typename T>
T eq6(std::vector<T>& Ans, std::vector<T>& feed){
    return Ans[xgCH4] + Ans[xgH2O] + Ans[xgH2] - 1;
}