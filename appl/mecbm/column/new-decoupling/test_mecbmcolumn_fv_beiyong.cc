// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the two-phase n-component finite volume model used to model e.g. salt dissolution.
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "mecbmcolumnproblem.hh" // insert the needed problem file here

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TYPETAG;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    // initialize the spatialParams separately to compute the referencePorosity and referencePermeability
    problem->spatialParams().computeReferencePorosity(*fvGridGeometry, x);
    problem->spatialParams().computeReferencePermeability(*fvGridGeometry, x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    const Scalar episodeLength= getParam<Scalar>("TimeLoop.Episode");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields

    //add specific output
    vtkWriter.addField(problem->getKxx(), "Kxx");
    vtkWriter.addField(problem->getKyy(), "Kyy");
    vtkWriter.addField(problem->getKzz(), "Kzz");
    vtkWriter.addField(problem->cellVolume(), "cellVolume");
    vtkWriter.addField(problem->molOutFluxCH4w(), "molOutFluxCH4w");
    vtkWriter.addField(problem->molOutFluxCH4n(), "molOutFluxCH4n");

    // update the output fields before write
    problem->updateVtkOutput(x);
    vtkWriter.write(0.0);

    // instantiate time loop
    int episodeIdx_ = 0;
    // set the time loop with episodes of various lengths as specified in the injection file
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    // do it with episodes if an injection parameter file is specified
    if (hasParam("Injection.InjectionParamFile"))
    {
        // first read the times of the checkpoints from the injection file
        std::vector<Scalar> epiEnd_;
        std::string injectionParameters_ = getParam<std::string>("Injection.InjectionParamFile");
        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        Scalar tempTime = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while (!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while (!injectionData.eof())
        {
            getline(injectionData, row);
            if (row == "EndTimes")
            {
                getline(injectionData, row);
                while (row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempTime;
                        // injectionParameters_ contains the time in days!
                        epiEnd_.push_back(tempTime*3600*24);
                    }
                    getline(injectionData, row);
                }
            }
        }
        injectionData.close();

        // check the injection data against the number of injections specified in the parameter file
        int numInjections_ = getParam<Scalar>("Injection.numInjections");
        if (epiEnd_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection end times specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numEpisodes from injection data file = "<<epiEnd_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        // set the time loop with episodes of various lengths as specified in the injection file
//        auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
//        timeLoop->setMaxTimeStepSize(maxDt);
        // set the episode ends /check points:
        for (int epiIdx = 0; epiIdx < epiEnd_.size(); ++epiIdx)
        {
            timeLoop->setCheckPoint(epiEnd_[epiIdx]);
        }
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
    }

        // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        // set the time loop with periodic episodes of constant length
        // auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
        // timeLoop->setMaxTimeStepSize(maxDt);
        timeLoop->setPeriodicCheckPoint(tEnd/getParam<Scalar>("TimeLoop.EpisodeLength"));
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
    }    // no episodes
    else
    {
        // set the time loop without episodes
        // auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
        // timeLoop->setMaxTimeStepSize(maxDt);
        timeLoop->setCheckPoint(tEnd);
    }

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolver = NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // get some indexes here
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = TwoPNCIndices;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx   = Indices::switchIdx, //Saturation

        //Indices of the phases
                phase0Idx = FluidSystem::phase0Idx,
        phase1Idx = FluidSystem::phase1Idx,

        //Indices of the components
                numComponents = FluidSystem::numComponents,
        comp0Idx    = FluidSystem::BrineIdx,
        comp1Idx    = FluidSystem::CH4Idx,
        CH4Idx      = FluidSystem::CH4Idx,
        AcetateIdx  = FluidSystem::AcetateIdx,
        AmendmentIdx= FluidSystem::AmendmentIdx,
        RMethylIdx  = FluidSystem::RMethylIdx,
        HydrogenIdx = FluidSystem::H2Idx,
        TCIdx       = FluidSystem::TCIdx,

        numSolidComponents= SolidSystem::numComponents - SolidSystem::numInertComponents,
        CoalBacPhaseIdx   = SolidSystem::CoalBacPhaseIdx,
        AmCoalBacPhaseIdx = SolidSystem::AmCoalBacPhaseIdx,
        AcetoArchPhaseIdx = SolidSystem::AcetoArchPhaseIdx,
        HydroArchPhaseIdx = SolidSystem::HydroArchPhaseIdx,
        MethyArchPhaseIdx = SolidSystem::MethyArchPhaseIdx,
        CCoalPhaseIdx     = SolidSystem::CCoalPhaseIdx,

        //Indices of the bio/coal volume fractions
                CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
        AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
        AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
        HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
        MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
        CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,

        //Index of the primary component of G and L phase
                conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
                wPhaseOnly = Indices::firstPhaseOnly,
        nPhaseOnly = Indices::secondPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
                dim      = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    //parameters for reaction
    //growth and decay rate coefficients
    Scalar muCoalBacCoal_  = getParam<Scalar>("BioCoefficients.muCoalBacC");   // Maximum specific growth rate for Primary Bacteria on Coal
    Scalar muAmCoalBacCoal_= getParam<Scalar>("BioCoefficients.muAmCoalBacC"); // Maximum specific growth rate for Secondary Bacteria on Coal
    Scalar muAmCoalBacAm_  = getParam<Scalar>("BioCoefficients.muAmCoalBacAm");// Maximum specific growth rate for Secondary Bacteria on Amendment
    Scalar muAcetoArch_    = getParam<Scalar>("BioCoefficients.muAcetoArch");  // Maximum specific growth rate for Acetoclastic Archaea
    Scalar muHydroArch_    = getParam<Scalar>("BioCoefficients.muHydroArch");  // Maximum specific growth rate for Hydrogenotrophic Archaea
    Scalar muMethyArch_    = getParam<Scalar>("BioCoefficients.muMethyArch");  // Maximum specific growth rate for Methylotrophic Archaea
    Scalar K_              = muAmCoalBacAm_/100; //getParam<Scalar>("BioCoefficients.K");            // Decay Rate for Microbes

    //Monod half saturation constants
    Scalar Kc_     = getParam<Scalar>("BioCoefficients.Kc");           // Monod Half saturation Constant for Coal
    Scalar Kh_     = getParam<Scalar>("BioCoefficients.KH2");          // Monod Half saturation Constant for Hydrogen
    Scalar KAm_    = getParam<Scalar>("BioCoefficients.KAm");          // Monod Half saturation Constant for Amendment
    Scalar KAc_    = getParam<Scalar>("BioCoefficients.KAc");          // Monod Half saturation Constant for Acetate
    Scalar KCH3_   = getParam<Scalar>("BioCoefficients.KCH3");         // Monod Half saturation Constant for Methyl

    //Yields
    Scalar Yccbc_  = getParam<Scalar>("BioCoefficients.YCoalBacC");    // Yield of coal consuming Bacteria Biomass on Coal
    Scalar Yacbc_  = getParam<Scalar>("BioCoefficients.YAmCoalBacC");  // Yield of coal & amendment consuming Bacteria on Coal
    Scalar Yacbam_ = getParam<Scalar>("BioCoefficients.YAmCoalBacAm"); // Yield of coal & amendment consuming Bacteria on Amendment
    Scalar amFac_  = getParam<Scalar>("BioCoefficients.amFac", 1); // amendmentFactor
    Scalar YhaH2_  = getParam<Scalar>("BioCoefficients.YHydroArchH2"); // Yield of Hydrogenotrophic Archaea Biomass on Hydrogen
    Scalar YaaAc_  = getParam<Scalar>("BioCoefficients.YAcetoArchAc"); // Yield of Acetoclastic Archaea Biomass on Acetate
    Scalar YmaCH3_ = getParam<Scalar>("BioCoefficients.YMethyArchCH3");// Yield of Methylotrophic Archaea Biomass on Methyl
    Scalar YH2cc_  = getParam<Scalar>("BioCoefficients.YH2cc");        // Yield of Hydrogen from Coal
    Scalar YAccc_  = getParam<Scalar>("BioCoefficients.YAccc");        // Yield of Acetate from Coal
    Scalar YH2Am_  = getParam<Scalar>("BioCoefficients.YH2Am")*amFac_;        // Yield of Hydrogen from Amendment
    Scalar YAcAm_  = getParam<Scalar>("BioCoefficients.YAcAm")*amFac_;        // Yield of Acetate from Amendment
    Scalar YCH3Am_ = getParam<Scalar>("BioCoefficients.YCH3Am")*amFac_;       // Yield of Methyl from Amendment
    Scalar YCH4Ac_ = getParam<Scalar>("BioCoefficients.YCH4Ac");       // Yield of CH4 from Acetate
    Scalar YCH4H2_ = getParam<Scalar>("BioCoefficients.YCH4H2");       // Yield of CH4 from Hydrogen
    Scalar YCH4CH3_= getParam<Scalar>("BioCoefficients.YCH4CH3");      // Yield of CH4 from Methyl
    Scalar YCO2Ac_ = getParam<Scalar>("BioCoefficients.YCO2Ac");       // Yield of CO2 from Acetate
    Scalar YCO2CH3_ = getParam<Scalar>("BioCoefficients.YCO2CH3");       // Yield of CO2 from Methyl

    Scalar temperature_ = getParam<Scalar>("Problem.Temperature");

    auto pSat = FluidSystem::waterSatPressure(temperature_);
    auto henryCH4 = FluidSystem::CH4Henry(temperature_);
    // time loop
    timeLoop->start(); do
    {
        // set time for problem for implicit Euler scheme
        problem->setTime( timeLoop->time() + timeLoop->timeStepSize() );
        problem->setTimeStepSize( timeLoop->timeStepSize() );

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);
        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();


        std::cout<<"before" <<x[0] <<std::endl;

        ////////////////////////////
        ////// local reaction in scv
        ///////////////////////////
        int episodeNum = 1;// int (timeLoop->timeStepSize()/ episodeLength);
        auto timeStep = timeLoop->timeStepSize()/episodeNum;
        std::vector<Num>

        for (const auto& element : elements(leafGridView))
        {
            auto fvGeometry = localView(*fvGridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto scvIdx = scv.dofIndex();
                const auto& volVars = elemVolVars[scv];
//                auto& scvSolution = x[scvIdx];

                // get porosity
                auto elemSol = elementSolution(element, x, problem->fvGridGeometry());
                auto curPorosity = problem->spatialParams().porosity(element,scv,elemSol);

//                // get CH4 mol fraction
//                auto Sn = scvSolution[switchIdx];
//                auto Sw = 1-Sn;
//
//                auto pc = problem->spatialParams().getPC(Sw);
//                auto pn = scvSolution[0]+pc;
//
//                auto xCH4w = volVars.moleFraction(phase0Idx,CH4Idx);
//                auto xCH4n = xCH4w * henryCH4 / pn;

                // calculate the reaction
                for (int episode = 0; episode < episodeNum ; ++episode)
                {

//                    //mass before reaction
//                    NumEqVector mOld(0.0);
//                    for (int compIdx = 1; compIdx < numComponents+numSolidComponents; ++compIdx) {
//                        // components in fluid phase in [mol/ total m3 ]
//                        if(compIdx<numComponents)
//                        {
//                            mOld[compIdx] = Sw * xCH4w* volVars.molarDensity(phase0Idx);
//
//                            // consider the fraction in gas phase
//                            if (compIdx == CH4Idx)
//                            {
//                                auto molarDensityGas = FluidSystem::molarDensityGas(temperature_,pn);
//                                mOld[compIdx]+= Sn * xCH4n * molarDensityGas;
//                            }
//
//                            mOld[compIdx] *= curPorosity;
//                        }
//                            // solid phases in [mol/ total m3 ]
//                        else{
//                            auto solidPhaseIdx = compIdx - numComponents;
//                            mOld[compIdx] = scvSolution[compIdx]*volVars.solidComponentDensity(solidPhaseIdx);
//                        }
//                    }


                    NumEqVector q(0.0);

//                    Scalar cAcetate = scvSolution[AcetateIdx] * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AcetateIdx);    //[kg_suspended_Biomass/m³_waterphase]
//                    if(cAcetate < 0)
//                        cAcetate = 0;
//                    Scalar cCarAm   = scvSolution[AmendmentIdx] * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AmendmentIdx); //[kg_suspended_Biomass/m³_waterphase]
//                    if(cCarAm < 0)
//                        cCarAm = 0;
//                    Scalar cRMethyl = scvSolution[RMethylIdx] * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(RMethylIdx);    //[kg_suspended_Biomass/m³_waterphase]
//                    if(cRMethyl < 0)
//                        cRMethyl = 0;
//                    Scalar cHydrogen = scvSolution[HydrogenIdx] * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(HydrogenIdx); //[kg_suspended_Biomass/m³_waterphase]
//                    if(cHydrogen < 0)
//                        cHydrogen = 0;

                    Scalar cAcetate = volVars.moleFraction(phase0Idx, AcetateIdx)* volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AcetateIdx);    //[kg_suspended_Biomass/m³_waterphase]
                    if(cAcetate < 0)
                        cAcetate = 0;
                    Scalar cCarAm   = volVars.moleFraction(phase0Idx, AmendmentIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AmendmentIdx); //[kg_suspended_Biomass/m³_waterphase]
                    if(cCarAm < 0)
                        cCarAm = 0;
                    Scalar cRMethyl = volVars.moleFraction(phase0Idx, RMethylIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(RMethylIdx);    //[kg_suspended_Biomass/m³_waterphase]
                    if(cRMethyl < 0)
                        cRMethyl = 0;
                    Scalar cHydrogen = volVars.moleFraction(phase0Idx, HydrogenIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(HydrogenIdx); //[kg_suspended_Biomass/m³_waterphase]
                    if(cHydrogen < 0)
                        cHydrogen = 0;

//                    //compute biomass growth coefficients and rate
//                    Scalar massCoalBac_   = scvSolution[CoalBacIdx]*volVars.solidComponentDensity(CoalBacPhaseIdx);     //[kg_pb_Biomass/m³_total] volumetric fraction of primary bacteria attached reversibly
//                    Scalar massAmCoalBac_ = scvSolution[AmCoalBacIdx]*volVars.solidComponentDensity(AmCoalBacPhaseIdx); //[kg_sb_Biomass/m³_total] volumetric fraction of secondary bacteria attached reversibly
//                    Scalar massAcetoArch_ = scvSolution[AcetoArchIdx]*volVars.solidComponentDensity(AcetoArchPhaseIdx); //[kg_aa_Biomass/m³_total] volumetric fraction of acetoclastic archaea attached reversibly
//                    Scalar massHydroArch_ = scvSolution[HydroArchIdx]*volVars.solidComponentDensity(HydroArchPhaseIdx); //[kg_ha_Biomass/m³_total] volumetric fraction of hydrogenotrophic archaea attached reversibly
//                    Scalar massMethyArch_ = scvSolution[MethyArchIdx]*volVars.solidComponentDensity(MethyArchPhaseIdx); //[kg_ma_Biomass/m³_total] volumetric fraction of methylotrophic archaea attached reversibly
//                    Scalar massCCoal_     = scvSolution[CCoalIdx]*volVars.solidComponentDensity(CCoalPhaseIdx);         //[kg_cc_Biomass/m³_total] volumetric fraction convertible coal attached reversibly

                    //compute biomass growth coefficients and rate
                    Scalar massCoalBac_   = volVars.solidVolumeFraction(CoalBacPhaseIdx)*volVars.solidComponentDensity(CoalBacPhaseIdx);     //[kg_pb_Biomass/m³_total] volumetric fraction of primary bacteria attached reversibly
                    Scalar massAmCoalBac_ = volVars.solidVolumeFraction(AmCoalBacPhaseIdx)*volVars.solidComponentDensity(AmCoalBacPhaseIdx); //[kg_sb_Biomass/m³_total] volumetric fraction of secondary bacteria attached reversibly
                    Scalar massAcetoArch_ = volVars.solidVolumeFraction(AcetoArchPhaseIdx)*volVars.solidComponentDensity(AcetoArchPhaseIdx); //[kg_aa_Biomass/m³_total] volumetric fraction of acetoclastic archaea attached reversibly
                    Scalar massHydroArch_ = volVars.solidVolumeFraction(HydroArchPhaseIdx)*volVars.solidComponentDensity(HydroArchPhaseIdx); //[kg_ha_Biomass/m³_total] volumetric fraction of hydrogenotrophic archaea attached reversibly
                    Scalar massMethyArch_ = volVars.solidVolumeFraction(MethyArchPhaseIdx)*volVars.solidComponentDensity(MethyArchPhaseIdx); //[kg_ma_Biomass/m³_total] volumetric fraction of methylotrophic archaea attached reversibly
                    Scalar massCCoal_     = volVars.solidVolumeFraction(CCoalPhaseIdx)*volVars.solidComponentDensity(CCoalPhaseIdx);         //[kg_cc_Biomass/m³_total] volumetric fraction convertible coal attached reversibly

                    if(massCCoal_ < 0)
                        massCCoal_ = 0;

                    Scalar rgCoalBac      = muCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massCoalBac_;      //growth rate primary bacteria (MSc Irfan 3.2)
                    Scalar rdCoalBac      = K_ * massCoalBac_;                                                  //decay rate primary bacteria (MSc Irfan 3.3)
                    Scalar rgAmCoalBacCoal= muAmCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massAmCoalBac_;  //growth rate secondary bacteria on coal (MSc Irfan 3.5)
                    Scalar rgAmCoalBacAm  = muAmCoalBacAm_ * cCarAm / (KAm_ + cCarAm)*massAmCoalBac_;           //growth rate secondary bacteria on amendment (MSc Irfan 3.6)
                    Scalar rgAmCoalBac    = rgAmCoalBacCoal + rgAmCoalBacAm;                                    //growth rate secondary bacteria total (MSc Irfan 3.7)
                    Scalar rdAmCoalBac    = K_ * massAmCoalBac_;                                                //decay rate secondary bacteria total (MSc Irfan 3.8)
                    Scalar rgAcetoArch    = muAcetoArch_ * cAcetate / (KAc_ + cAcetate)*massAcetoArch_;         //growth rate acetoclastic archaea (MSc Irfan 3.13)
                    Scalar rdAcetoArch    = K_ * massAcetoArch_;                                                //decay rate acetoclastic archaea (MSc Irfan 3.14)
                    Scalar rgHydroArch    = muHydroArch_ * cHydrogen / (Kh_ + cHydrogen)*massHydroArch_;        //growth rate hydrogenotrophic archaea (MSc Irfan 3.10)
                    Scalar rdHydroArch    = K_ * massHydroArch_;                                                //decay rate hydrogenotrophic archaea (MSc Irfan 3.11)
                    Scalar rgMethyArch    = muMethyArch_ * cRMethyl / (KCH3_ + cRMethyl)*massMethyArch_;        //growth rate methylotrophic archaea (MSc Irfan 3.16)
                    Scalar rdMethyArch    = K_ * massMethyArch_;                                                //decay rate methylotrophic archaea (MSc Irfan 3.17)

                    //Phase reactions in [mol/(m3*s)]
                    //introducing *10 factor to match Irfan's Matlab porosity formulation
                    q[CoalBacIdx]     = (rgCoalBac - rdCoalBac) / 86400 / SolidSystem::molarMass(CoalBacPhaseIdx);
                    q[AmCoalBacIdx]   = (rgAmCoalBac - rdAmCoalBac) / 86400 / SolidSystem::molarMass(AmCoalBacPhaseIdx);
                    q[AcetoArchIdx]   = (rgAcetoArch - rdAcetoArch) / 86400 / SolidSystem::molarMass(AcetoArchPhaseIdx);
                    q[HydroArchIdx]   = (rgHydroArch - rdHydroArch) / 86400 / SolidSystem::molarMass(HydroArchPhaseIdx);
                    q[MethyArchIdx]   = (rgMethyArch - rdMethyArch) / 86400 / SolidSystem::molarMass(MethyArchPhaseIdx);

                    //Component reactions in mol/(m3*s)
                    q[CH4Idx]     = (rgHydroArch * YCH4H2_ / YhaH2_
                                     + rgAcetoArch * YCH4Ac_ / YaaAc_
                                     + rgMethyArch * YCH4CH3_ / YmaCH3_)
                                    /86400 / FluidSystem::molarMass(CH4Idx); //+ sorp; //TODO: add when above is solved/clear
                    q[AcetateIdx] = ((rgAmCoalBacCoal * YAccc_ / Yacbc_)
                                     + (rgAmCoalBacAm * YAcAm_ / Yacbam_)
                                     + (rgCoalBac * YAccc_ / Yccbc_)
                                     - rgAcetoArch / YaaAc_)
                                    /86400  / FluidSystem::molarMass(AcetateIdx);
                    q[AmendmentIdx]= (-rgAmCoalBacAm / Yacbam_)
                                     /86400 / FluidSystem::molarMass(AmendmentIdx);

                    q[RMethylIdx] = ((rgAmCoalBacAm * YCH3Am_ / Yacbam_)
                                     - rgMethyArch / YmaCH3_)
                                    /86400  / FluidSystem::molarMass(RMethylIdx);

                    q[HydrogenIdx] = ((rgAmCoalBacCoal * YH2cc_ / Yacbc_)
                                      + (rgAmCoalBacAm * YH2Am_ / Yacbam_)
                                      + (rgCoalBac * YH2cc_ / Yccbc_)
                                      - rgHydroArch / YhaH2_)
                                     /86400  / FluidSystem::molarMass(HydrogenIdx);
                    q[TCIdx]       = (rgAcetoArch * YCO2Ac_ / YaaAc_
                                      + (rgMethyArch * YCO2CH3_ / YmaCH3_)
                                      -(rgHydroArch * YCH4H2_ / YhaH2_) )        // consume 1 CO2 per 1 CH4
                                     /86400  / FluidSystem::molarMass(TCIdx);

                    q[CCoalIdx]    = -(rgCoalBac / Yccbc_ + rgAmCoalBacCoal / Yacbc_)
                                     / 86400 / SolidSystem::molarMass(CCoalPhaseIdx);

                    // source term
                    NumEqVector source(0.0);
                    for (int compIdx = 1; compIdx < numComponents+numSolidComponents; ++compIdx) {
                        source[compIdx]= q[compIdx] * timeStep ;
                    }


                    if(scvIdx==0){
                        std::cout << "source "<<source << std::endl;
                    }

                    /////////////////////////
                    //////  iteration of saturation
                    /////////////////////////
                    auto SnOld = Sn;
                    auto mCH4 = source[CH4Idx] + mOld[CH4Idx];

                    if(scvIdx == 0){
                        std::cout<<"initial mass " <<  mCH4  <<std::endl;
                    }


                    do{
                        auto molarDensityGas = FluidSystem::molarDensityGas(temperature_,pn);

                        //! assume porosity not changed at moment
                        Sn = mCH4/curPorosity - xCH4w * volVars.molarDensity(phase0Idx);

                        Sn /=  (xCH4n * molarDensityGas
                                -xCH4w * volVars.molarDensity(phase0Idx));

                        scvSolution[CH4Idx] = Sn;

                        // calculate parameters for the new saturation
                        Sw = 1 - Sn;
                        pc = problem->spatialParams().getPC(Sw);
                        pn = scvSolution[0]+pc;
                        xCH4w = (pn - pSat)/ (henryCH4 - pSat);
                        xCH4n = xCH4w * henryCH4 / pn;

                        SnOld = Sn;

                    }while(abs(Sn-SnOld)/Sn > 1e-5);


                    // check mass conservation
                    if(scvIdx == 0){
                        auto molarDensityGas = FluidSystem::molarDensityGas(temperature_,pn);
                        auto mCH4after = Sw * xCH4w * volVars.molarDensity(phase0Idx)
                                         + Sn * xCH4n * molarDensityGas;
                        mCH4after *= curPorosity;
                        std::cout<<"final  mass " <<  mCH4after <<std::endl;
                    }

                    //update component mole fraction
                    ///!!! todo::consider the change of saturation?
                    for (int compIdx = 2; compIdx < numComponents; ++compIdx) {
                        scvSolution[compIdx] = (mOld[compIdx] + source[compIdx])
                                               /volVars.molarDensity(phase0Idx)
                                               /Sw
                                               /curPorosity;
                    }

                    //update solid phase volume fraction
                    for (int solidIdx = numComponents; solidIdx < numComponents + numSolidComponents; ++solidIdx) {
                        auto solidPhaseIdx = solidIdx - numComponents;
                        scvSolution[solidIdx] += source[solidIdx]
                                                 /volVars.solidComponentDensity(solidPhaseIdx)
                                                 *SolidSystem::molarMass(solidPhaseIdx) ;
                    }

                }//end of episode
            }//end of scv
        }//end of element




        std::cout<<"x[0]" <<x[0]<<std::endl;

        xOld = x;

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // update the output fields before write
        problem->updateVtkOutput(x);

        //write CH4 data output file
//        problem->writeFluxes(dataFile, fileName, x);

        // write vtk output
        vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength"))
            if(timeLoop->isCheckPoint())
            {
                int injType = problem->injectionType(episodeIdx_);
                std::cout<< "\n Episode " << episodeIdx_
                         << " with injectionType " << injType
                         << " done \n" << std::endl;
                episodeIdx_++;
                problem->setEpisodeIdx(episodeIdx_);
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(dt));
            }

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
              "). Most likely, the DGF file name is wrong "
              "or the DGF file is corrupted, "
              "e.g. missing hash at end of file or wrong number (dimensions) of entries."
              << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
