 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for the tracer problem
 */
#ifndef DUMUX_MECBM_COLUMN_TRACER_SPATIAL_PARAMS_HH
#define DUMUX_MECBM_COLUMN_TRACER_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <iostream>

namespace Dumux {

/*!
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for the tracer problem
 */
template<class FVGridGeometry, class Scalar>
class MECBMTracerSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                         MECBMTracerSpatialParams<FVGridGeometry, Scalar>> {
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar,
            MECBMTracerSpatialParams<FVGridGeometry, Scalar>>;

    using CoordScalar = typename GridView::ctype;
    enum {
        dimWorld = GridView::dimensionworld
    };
    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;

public:
    // type used for the permeability (i.e. tensor or scalar)

    MECBMTracerSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
            : ParentType(fvGridGeometry) {
        porosityValue_     = getParam<Scalar>("SpatialParams.Porosity");
    }


    Scalar theta(const SubControlVolume &scv) const { return 10.0; }

    /*!
    * \brief return the parameter object for the Brooks-Corey material law which depends on the position
    *
    * \param globalPos The global position
     *
    */

    void setPorosityAtPos(const std::vector<std::vector<double>>& spatialParams)
    {
        porosity_ = spatialParams[0];
        useSpatialParams_ = true;   
    }

    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if(useSpatialParams_==true){
            return porosity_[scv.dofIndex()];}
        else{
            return porosityValue_;}
    }

    //! Fluid properties that are spatial parameters in the tracer model
    //! They can possible vary with space but are usually constants

    //! Fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume &scv) const {
        return density_[this->fvGridGeometry().elementMapper().index(element)];;
    }

    void setDensity(const std::vector <Scalar> &d) { density_ = d; }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume &scv) const { return 0.018; }

    Scalar fluidMolarMass(const GlobalPosition &globalPos) const { return 0.018; }

    //! Velocity field
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const ElementVolumeVariables &elemVolVars,
                      const SubControlVolumeFace &scvf) const { return volumeFlux_[scvf.index()]; }

    void setVolumeFlux(const std::vector <Scalar> &f) { volumeFlux_ = f; }

    //! saturation from twoPProblem
    Scalar saturation(const Element &element,
                      const SubControlVolume &scv) const { return saturation_[scv.dofIndex()]; }

    void setSaturation(const std::vector <Scalar> &s) { saturation_ = s; }

    auto flux(const int& scvfIdx){
        return volumeFlux_[scvfIdx];
    };
private:
    std::vector <Scalar> volumeFlux_;
    std::vector <Scalar> density_;
    std::vector <Scalar> saturation_;

    Scalar porosityValue_ ;


    template<class T>
    struct Compare :public std::binary_function<T,T,bool>
    {
        bool operator()(const T& lhs,const T& rhs)const {
            for (int i = 0; i < dimWorld; ++i) {
                if (lhs[i] < rhs[i])
                    return true;
            }
            return false;
        }
    };

    std::vector<double> porosity_;

    mutable bool useSpatialParams_= false;
};
} // end namespace Dumux

#endif
