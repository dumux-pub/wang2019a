// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TracerTests
 * \brief  A 2p problem with multiple tracer bands in a porous groundwater reservoir with a lens
 */
#ifndef DUMUX_MECBM_COLUMN_PROBLEM_TRACER_HH
#define DUMUX_MECBM_COLUMN_PROBLEM_TRACER_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/tracer/model.hh>


#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/components/ccoal.hh>
#include <dumux/material/components/acetate.hh>
#include <dumux/material/components/caram.hh>
#include <dumux/material/components/rmethyl.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/material/binarycoefficients/h2o_ch4.hh>
#include <dumux/material/binarycoefficients/h2o_h2.hh>
#include <dumux/material/binarycoefficients/brine_co2.hh>
#include <test/porousmediumflow/co2/implicit/co2tables.hh>

//#include <dumux/material/solidsystems/mecbm.hh>

#include "mecbmcolumnspatialparams_tracer.hh"

namespace Dumux {
/*!
 * \ingroup TracerTests
 * \brief A 2p problem with multiple tracer bands in a porous groundwater reservoir with a lens
 */
    template <class TypeTag>
    class MECBMColumnTracerProblem;

    namespace Properties {
//Create new type tags
        namespace TTag {
            struct MECBMColumnTracer { using InheritsFrom = std::tuple<Tracer>; };
            struct MECBMColumnTracerTpfa { using InheritsFrom = std::tuple<MECBMColumnTracer, CCTpfaModel>; };
        } // end namespace TTag

// Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::MECBMColumnTracer> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::MECBMColumnTracer> { using type = MECBMColumnTracerProblem<TypeTag>; };


//! A simple fluid system with one tracer component


        template<class TypeTag>
        class TracerFluidSystem : public FluidSystems::Base<GetPropType<TypeTag, Properties::Scalar>,
                                  TracerFluidSystem<TypeTag>>
    {
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using Problem = GetPropType<TypeTag, Properties::Problem>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using Element = typename GridView::template Codim<0>::Entity;
        using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;


        public:
        //! If the fluid system only contains tracer components
        static constexpr bool isTracerFluidSystem()
        { return true; }

        //! No component is the main component
        static constexpr int getMainComponent(int phaseIdx)
        { return -1; }

        //! The number of components
        static constexpr int numComponents= 5;
        static constexpr int AcetateIdx   = 0;
        static constexpr int AmendmentIdx = 1;
        static constexpr int RMethylIdx   = 2;
        static constexpr int H2Idx        = 3;
        static constexpr int TCIdx        = 4;
        static constexpr int CO2Idx       = TCIdx;

        using Acetate   = Components::Acetate<Scalar>;
        using Amendment = Components::CarAm<Scalar>;
        using RMethyl   = Components::RMethyl<Scalar>;
        using H2        = Components::H2<Scalar>;
        using CO2       = Dumux::Components::CO2<Scalar, HeterogeneousCO2Tables::CO2Tables>;

        using Brine_CO2 = BinaryCoeff::Brine_CO2<Scalar, HeterogeneousCO2Tables::CO2Tables>;
        using H2O_CH4   = BinaryCoeff::H2O_CH4;
        using H2O_H2    = BinaryCoeff::H2O_H2;

        //! Human readable component name (index compIdx) (for vtk output)
        static std::string componentName(int compIdx)
        {
            assert(0 <= compIdx && compIdx < numComponents);

            static std::string name[] = { Acetate::name(),
                                          Amendment::name(),
                                          RMethyl::name(),
                                          H2::name(),
                                          CO2::name() };
            return name[compIdx];}

        //! Molar mass in kg/mol of the component with index compIdx
        static Scalar molarMass(unsigned int compIdx)
        {
            assert(0 <= compIdx && compIdx < numComponents);
            static const Scalar M[] = { Acetate::molarMass(),
                                        Amendment::molarMass(),
                                        RMethyl::molarMass(),
                                        H2::molarMass(),
                                        CO2::molarMass() };
            return M[compIdx];
        }

        //! Binary diffusion coefficient
        //! (might depend on spatial parameters like pressure / temperature)
        static Scalar binaryDiffusionCoefficient(unsigned int compJIdx,
                                                 const Problem& problem,
                                                 const Element& element,
                                                 const SubControlVolume& scv)
        {
            Scalar result = 0;
            Scalar temperature = 298.15;
            Scalar pressure   = 1.01325E5;

            if (compJIdx == AcetateIdx)
                result = 0.67e-9; // glucose value from internet //TODO ACTUAL SOURCE GOES HERE!
            else if (compJIdx == AmendmentIdx)
                result = 0.67e-9; // arbitrary value
            else if (compJIdx == RMethylIdx)
                result = 0.67e-9; // arbitrary value
            else if (compJIdx == H2Idx)
                result = H2O_H2::liquidDiffCoeff(temperature, pressure);
            else if (compJIdx == TCIdx)
            {
                // result = 1.92e-9; // value for 25°C at atmospheric pressure (https://onlinelibrary.wiley.com/doi/pdf/10.1002/jrs.4742)
                //if (compIIdx == BrineOrH2OIdx && compJIdx == CO2Idx)
                result =  Brine_CO2::liquidDiffCoeff(temperature, pressure);
            }
            return result;
        }
    };

    template<class TypeTag>
    struct FluidSystem<TypeTag, TTag::MECBMColumnTracer> {
        using type = TracerFluidSystem<TypeTag>;
        static constexpr int numComponents = TracerFluidSystem<TypeTag>::numComponents;
    };

//    template<class TypeTag>
//    struct SolidSystem<TypeTag, TTag::MECBMColumnTracer> {
//        using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
//        using type = SolidSystems::MECBMSolidPhase<Scalar>;
//        static constexpr int numComponents = SolidSystems::MECBMSolidPhase<Scalar>::numComponents;
//        static constexpr int numInertComponents = SolidSystems::MECBMSolidPhase<Scalar>::numInertComponents;
//    };

// Set the spatial parameters
    template<class TypeTag>
    struct SpatialParams<TypeTag, TTag::MECBMColumnTracer>
    {
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using type = MECBMTracerSpatialParams<FVGridGeometry, Scalar>;
    };

// Define whether mole(true) or mass (false) fractions are used
    template<class TypeTag>
    struct UseMoles<TypeTag, TTag::MECBMColumnTracer> { static constexpr bool value = true; };
    template<class TypeTag>
    struct SolutionDependentMolecularDiffusion<TypeTag, TTag::MECBMColumnTracer> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \ingroup TracerTests
 *
 * \brief Definition of a problem, for the tracer problem:
 * A lens of contaminant tracer is diluted by diffusion and a base groundwater flow
 *
 * This problem uses the \ref TracerModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_2ptracer -ParameterFile ./params.input</tt> or
 * <tt>./test_2ptracer -ParameterFile ./params.input</tt>
 */
template <class TypeTag>
class MECBMColumnTracerProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
//    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Chemistry = typename Dumux::MECBMReactionsChemistry<TypeTag>;
    using Indices = TwoPNCIndices;

    enum {

        //Indices of the components
        numComponents = FluidSystem::numComponents,
        AcetateIdx  = FluidSystem::AcetateIdx,
        AmendmentIdx= FluidSystem::AmendmentIdx,
        RMethylIdx  = FluidSystem::RMethylIdx,
        H2Idx       = FluidSystem::H2Idx,
        TCIdx       = FluidSystem::TCIdx,

//        //Indices of the bio/coal volume fractions
//        CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
//        AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
//        AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
//        HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
//        MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
//        CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,

        //Index of the primary component of G and L phase
                conti0EqIdx = Indices::conti0EqIdx,


        // Grid and world dimension
                dim      = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using CoordScalar = typename GridView::ctype;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;


    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static constexpr auto numPhases = 2;// FluidSystem::numPhases;

public:
    MECBMColumnTracerProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
            : ParentType(fvGridGeometry)
    {
        name_                   = getParam<std::string>("Problem.Name");
        temperature_            = getParam<Scalar>("Problem.Temperature");
        reservoirPressure_      = getParam<Scalar>("Problem.ReservoirPressure");

        initxwCH4_              = getParam<Scalar>("Initial.initxwCH4"); // Initial wetting mole fraction of CH4
        initxwH2_               = getParam<Scalar>("Initial.initxwH2"); // Initial wetting mole fraction of Hydrogen
        initMassConcAmendment   = getParam<Scalar>("Initial.initMassConcAmendment"); // Initial mass concentration of Amendment
        initxwAcetate_          = getParam<Scalar>("Initial.initxwAcetate"); // Initial wetting mole fraction of Acetate
        initxwRMethyl_          = getParam<Scalar>("Initial.initxwRMethyl"); // Initial wetting mole fraction of Methyl
        initxwTC_               = getParam<Scalar>("Initial.initxwTC"); // Initial wetting mole fraction of total inorganic carbon
        initPhiCCoal_           = getParam<Scalar>("Initial.initPhiCCoal"); // Initial volume fraction of bioavailable coal
        initPhiCoalBac_         = getParam<Scalar>("Initial.initPhiCoalBac"); // Initial volume fraction of coal consuming bacteria
        initPhiAmCoalBac_       = getParam<Scalar>("Initial.initPhiAmCoalBac"); // Initial volume fraction of amendment and coal consuming bacteria
        initPhiAcetoArch_       = getParam<Scalar>("Initial.initPhiAcetoArch"); // Initial volume fraction of acetoclastics archaea
        initPhiHydroArch_       = getParam<Scalar>("Initial.initPhiMethyArch"); // Initial volume fraction of methylotrophic archaea
        initPhiMethyArch_       = getParam<Scalar>("Initial.initPhiHydroArch"); // Initial volume fraction of hydrogenotrophic archaea

        numInjections_          = getParam<int>("Injection.numInjections");
        injectionParameters_    = getParam<std::string>("Injection.InjectionParamFile");
        injQ_                   = getParam<Scalar>("Injection.injQ");

        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");

        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        int tempType = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while(!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while(!injectionData.eof())
        {
            getline(injectionData, row);

            if(row == "InjectionTypes")
            {
                getline(injectionData, row);
                while(row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempType;
                        injType_.push_back(tempType);
                        std::cout << "size of injType: "<<injType_.size() << std::endl;
                    }
                    getline(injectionData, row);
                }
            }
        }

        injectionData.close();

        if (injType_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection types specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numInjTypes from injection data file = "<<injType_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        Kxx_.resize(fvGridGeometry->gridView().size(codim));
        Kyy_.resize(fvGridGeometry->gridView().size(codim));
        Kzz_.resize(fvGridGeometry->gridView().size(codim));
        cellVolume_.resize(fvGridGeometry->gridView().size(codim));


//        FluidSystem::init(/*Tmin=*/temperatureLow_,
//                /*Tmax=*/temperatureHigh_,
//                /*nT=*/nTemperature_,
//                /*pmin=*/pressureLow_,
//                /*pmax=*/pressureHigh_,
//                /*np=*/nPressure_);
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    void setEpisodeIdx( Scalar epiIdx )
    {
        episodeIdx_ = epiIdx;
    }

    int injectionType( Scalar epiIdx )
    {
        return injType_[epiIdx];
    }


    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        bcTypes.setAllNeumann();
        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The neumann values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub control volume face
     *
     * For this method, the \a values parameter stores the flux
     * in normal direction of each phase. Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        Scalar area = M_PI*0.0525*0.0525/4;

        Scalar waterFlux = injQ_/area;//*0.49/area;// [m/s]

        //! Inflow boundary at bottom


        if(ipGlobal[dimWorld-1] < eps_) // standard: no injection
        {
            if (injectionType_ == 2) //water injection with Amendment
            {
                flux[AmendmentIdx] = -1e-6* initMassConcAmendment // 15000 kg/m3liq
                                       / FluidSystem::molarMass(AmendmentIdx) // 0.113 kg/mol -> mol/m3liq
                                       / (61 *86400);
            }
        }
        else
            flux = 0.0; // kg/m/s

        // no-flow everywhere except at the top boundary
        if(ipGlobal[dimWorld-1] < this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps_)
            return flux;


        // outflow at top
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx )
        {
            Scalar tpfaFlux = 0;
            for (int compIdx = 0; compIdx < numComponents; ++compIdx )
            {
                const Scalar density = useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx);

                tpfaFlux = fluxAtTop_;

                // emulate an outflow condition for the component transport on the top side
                tpfaFlux  *= (useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx));
                flux[compIdx] += tpfaFlux;
            }
        }
//        std::cout<<"TopFlux \n"<<flux <<std::endl;

        return flux;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {

        PrimaryVariables priVars(0.0);
        return priVars;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);

        // components
        priVars[H2Idx]        = initxwH2_;                  // Initial wetting mole fraction of Hydrogen
        priVars[AmendmentIdx] = 0;                          // set via injection file
        priVars[AcetateIdx]   = initxwAcetate_;             // Initial wetting mole fraction of Acetate
        priVars[RMethylIdx]   = initxwRMethyl_;             // Initial wetting mole fraction of Methyl
        priVars[TCIdx]        = initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon
        // biofilm/coal volume fractions
//        priVars[CCoalIdx]     = initPhiCCoal_;              // Initial volume fraction of bioavailable coal
//        priVars[CoalBacIdx]   = initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
//        priVars[AmCoalBacIdx] = initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
//        priVars[AcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
//        priVars[MethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
//        priVars[HydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction of methylotrophic archaea

        return priVars;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

//        Chemistry chemistry;
//        const auto& volVars = elemVolVars[scv];
//        chemistry.reactionSource(source, volVars, timeStepSize_);
        return source;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */

    const std::vector<Scalar>& getKxx()
    {
        return Kxx_;
    }

    const std::vector<Scalar>& getKyy()
    {
        return Kyy_;
    }

    const std::vector<Scalar>& getKzz()
    {
        return Kzz_;
    }

    const std::vector<Scalar>& cellVolume()
    {
        return cellVolume_;
    }


    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element &element,
                           const SubControlVolume &scv,
                           const ElementSolution& elemSol) const
    {
        const auto radius = 0.0525/2;
        return M_PI*radius*radius;
    }

    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
                Kzz_[dofIdxGlobal] = volVars.permeability()[2][2];
                cellVolume_[dofIdxGlobal] = element.geometry().volume();
            }
        }
    }

    void setInjectionType(const int& injectionType){
        injectionType_ = injectionType;
    }

    void setFlux(const double& flux){
        fluxAtTop_ = flux;
    };
private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massToMoleFrac_(Scalar XwNaCl)
    {
        const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(comp0Idx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
        const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

        const Scalar X_NaCl = XwNaCl;
        /* XwNaCl: conversion from mass fraction to mol fraction */
        auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
        return xwNaCl;
    }

    int nTemperature_;
    int nPressure_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar reservoirPressure_;
    Scalar temperature_;

    Scalar initxwCH4_;                 // Initial wetting mole fraction of methane
    Scalar initxwH2_;                  // Initial wetting mole fraction of Hydrogen
    Scalar initMassConcAmendment;      // Initial wetting mole fraction of Amendment
    Scalar initxwAcetate_;             // Initial wetting mole fraction of Acetate
    Scalar initxwRMethyl_;             // Initial wetting mole fraction of Methyl
    Scalar initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon

    // biofilm/coal volume fractions
    Scalar initPhiCCoal_;              // Initial volume fraction of bioavailable coal
    Scalar initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
    Scalar initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
    Scalar initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
    Scalar initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
    Scalar initPhiHydroArch_;          // Initial volume fraction of methylotrophic archaea

    std::vector<int> injType_;
    int numInjections_;                // number of Injections
    std::string injectionParameters_;  // injectionParamteres from file
    Scalar injQ_;                      // injected volume flux in m3/s

    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    Scalar episodeIdx_ = 0;

    static constexpr Scalar eps_ = 1e-6;
    std::vector<double> Kxx_;
    std::vector<double> Kyy_;
    std::vector<double> Kzz_;
    std::vector<double> cellVolume_;

    mutable int injectionType_ = -99;
    mutable double fluxAtTop_;
};
} //end namespace Dumux

#endif
