// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the two-phase n-component finite volume model used to model e.g. salt dissolution.
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <math.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "mecbmcolumnproblem_tracer.hh.hh" // insert the needed problem file here
#include "../2p2c/mecbmcolumn2p2cproblem.hh" // insert the needed problem file here

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    //! define the type tags for this problem
    using TwoPTypeTag = Properties::TTag::MECBMColumnFlowCCTpfaTypeTag;
    using TracerTypeTag = Properties::TTag::MECBMColumnTracerTpfa;
    //! initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    //! print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TwoPTypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // set 2p Problem
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the reaction and transport problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    using ReactionProblem = GetPropType<ReactionTypeTag, Properties::Problem>;
    auto reactionProblem = std::make_shared<ReactionProblem>(fvGridGeometry);

    // the solution vector (use same solution vector for flow/transport and reaction)
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    using ReactionSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    ReactionSolutionVector r(fvGridGeometry->numDofs());
    reactionProblem->applyInitialSolution(r);
    auto rOld = r;

    // initialize the spatialParams separately to compute the referencePorosity and referencePermeability
    problem->spatialParams().computeReferencePorosity(*fvGridGeometry, x);
    problem->spatialParams().computeReferencePermeability(*fvGridGeometry, x);
    reactionProblem->spatialParams().computeReferencePorosity(*fvGridGeometry, r);
    reactionProblem->spatialParams().computeReferencePermeability(*fvGridGeometry, r);

    // the grid variables for both problems
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    using ReactionGridVariables = GetPropType<ReactionTypeTag, Properties::GridVariables>;
    auto reactionGridVariables = std::make_shared<ReactionGridVariables>(reactionProblem, fvGridGeometry);
    reactionGridVariables->init(r);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    const auto reactionMaxDt = getParam<Scalar>("TimeLoop.ReactionMaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    auto reactionDtInitial = getParam<Scalar>("TimeLoop.ReactionDtInitial");

    auto reactionDt = reactionDtInitial;
    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());

    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields

    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<ReactionGridVariables, ReactionSolutionVector> reactionVtkWriter(*reactionGridVariables, r, reactionProblem->name());
    IOFields::initOutputModule(reactionVtkWriter); //!< Add model specific output fields

    //add specific output for k, cell-volume, moloutflux
    vtkWriter.addField(problem->getKxx(), "Kxx");
    vtkWriter.addField(problem->getKyy(), "Kyy");
    vtkWriter.addField(problem->getKzz(), "Kzz");
    vtkWriter.addField(problem->cellVolume(), "cellVolume");
    vtkWriter.addField(problem->molOutFluxCH4w(), "molOutFluxCH4w");
    vtkWriter.addField(problem->molOutFluxCH4n(), "molOutFluxCH4n");

    reactionVtkWriter.addField(reactionProblem->getKxx(), "Kxx");
    reactionVtkWriter.addField(reactionProblem->getKyy(), "Kyy");
    reactionVtkWriter.addField(reactionProblem->getKzz(), "Kzz");
    reactionVtkWriter.addField(reactionProblem->cellVolume(), "cellVolume");
    reactionVtkWriter.addField(reactionProblem->molOutFluxCH4w(), "molOutFluxCH4w");
    reactionVtkWriter.addField(reactionProblem->molOutFluxCH4n(), "molOutFluxCH4n");

    // update the output fields before write
    problem->updateVtkOutput(x);
    reactionProblem->updateVtkOutput(r);
    vtkWriter.write(0.0);
    reactionVtkWriter.write(0.0);

    // instantiate time loop
    int episodeIdx_ = 0;
    int reactionEpisodeIdx_ = 0;
    Scalar timeLoopEpisodeLength = 0;
    // set the time loop with episodes of various lengths as specified in the injection file
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    std::vector<Scalar> epiEnd_;

    // do it with episodes if an injection parameter file is specified
    // read parameters and set checkpoint
    if (hasParam("Injection.InjectionParamFile"))
    {
        //read injection parameters
        // first read the times of the checkpoints from the injection file
        std::string injectionParameters_ = getParam<std::string>("Injection.InjectionParamFile");
        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        Scalar tempTime = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while (!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while (!injectionData.eof())
        {
            getline(injectionData, row);
            if (row == "EndTimes")
            {
                getline(injectionData, row);
                while (row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempTime;
                        // injectionParameters_ contains the time in days!
                        epiEnd_.push_back(tempTime*3600*24);
                    }
                    getline(injectionData, row);
                }
            }
        }
        injectionData.close();

        // check the injection data against the number of injections specified in the parameter file
        int numInjections_ = getParam<Scalar>("Injection.numInjections");
        if (epiEnd_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection end times specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numEpisodes from injection data file = "<<epiEnd_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        // set the episode ends /check points:
        for (int epiIdx = 0; epiIdx < epiEnd_.size(); ++epiIdx)
        {
            timeLoop->setCheckPoint(epiEnd_[epiIdx]);
        }
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
        reactionProblem->setEpisodeIdx(reactionEpisodeIdx_);
    }

        // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        timeLoopEpisodeLength = getParam<Scalar>("TimeLoop.EpisodeLength");
        timeLoop->setPeriodicCheckPoint(tEnd/timeLoopEpisodeLength);
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
        reactionProblem->setEpisodeIdx(reactionEpisodeIdx_);
    }    // no episodes
    else
    {
        timeLoop->setCheckPoint(tEnd);
    }

    //set Assembler, LinearSolver, NewtonSolver for transport problem
    // the assembler with time loop for transport problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver for transport problem
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver for transport problem
    using PMNewtonSolver = NewtonSolver<Assembler, LinearSolver>;
    PMNewtonSolver nonLinearSolver(assembler, linearSolver);

    // time loop
    timeLoop->start(); do
    {

        // transport start
        // set time for problem for implicit Euler scheme
        problem->setTime(timeLoop->time() + timeLoop->timeStepSize());
        problem->setTimeStepSize(timeLoop->timeStepSize());

        //print time info for transport
        std::cout<<"\nTransport               "
                 <<"\nProblem time            " << timeLoop->time()
                 <<"\nProblem stepsize        " << timeLoop->timeStepSize()
                 <<"\nProblem end             " << timeLoop->time()+ timeLoop->timeStepSize() << std::endl;

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);
        gridVariables->advanceTimeStep();

        // set the solver for reaction problem
        rOld = x;
        reactionGridVariables->advanceTimeStep();

        // build reaction timeloop
        Scalar reactionrestartTime = timeLoop->time();
        Scalar reactionTEnd = timeLoop->time()+ timeLoop->timeStepSize();
        reactionDt = std::min(timeLoop->timeStepSize(),reactionMaxDt);
//        if(hasParam("TimeLoop.ReactionDtEpisode")){
//            reactionDt = std::max(timeLoop->timeStepSize()/reactionDtEpisode,reactionDtInitial);
//        }

        auto reactionTimeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(reactionrestartTime,reactionDt,reactionTEnd);
        reactionTimeLoop->setMaxTimeStepSize(reactionMaxDt);

        //set Assembler, LinearSolver, NewtonSolver for reaction problem
        using ReactionAssembler = FVAssembler<ReactionTypeTag, DiffMethod::numeric>;
        auto  reactionAssembler = std::make_shared<ReactionAssembler>(reactionProblem, fvGridGeometry, reactionGridVariables, reactionTimeLoop);

        using ReactionLinearSolver = AMGBackend<ReactionTypeTag>;
        auto  reactionLinearSolver = std::make_shared<ReactionLinearSolver>(leafGridView, fvGridGeometry->dofMapper());

        using ReactionNewtonSolver = NewtonSolver<ReactionAssembler, ReactionLinearSolver>;
        ReactionNewtonSolver reactionNonLinearSolver(reactionAssembler, reactionLinearSolver);

        //set time parameters for reaction timeloop
        if (hasParam("Injection.InjectionParamFile"))
        {
            // set the episode ends /check points:
            for (int reactionEpiIdx = 0; reactionEpiIdx < epiEnd_.size(); ++reactionEpiIdx)
            {
                if(epiEnd_[reactionEpiIdx]> reactionTimeLoop->time()) {
                    reactionTimeLoop->setCheckPoint(epiEnd_[reactionEpiIdx]);
                }
            }

        }

            // fixed episode lengths for convenience in output only if this is specified
            //Todo: new variable reactionTimeLoopEpisodeLength_
        else if (timeLoopEpisodeLength>0)
        {
            // set the time loop with periodic episodes of constant length
            // auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
            reactionTimeLoop->setPeriodicCheckPoint(reactionTEnd/timeLoopEpisodeLength);
        }    // no episodes
        else
        {
            // set the time loop without episodes
            // auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
            // timeLoop->setMaxTimeStepSize(maxDt);
            reactionTimeLoop->setCheckPoint(reactionTEnd);
        }

        //print time info for reaction
        std::cout<< " \nstart calculating reactions"<< "\n"
                 << "reaction time           " << reactionTimeLoop->time() << "\n"
                 << "reaction timestepsize   " << reactionTimeLoop->timeStepSize()<< "\n"
                 << "reaction time end       " << reactionTEnd
                 <<std::endl;

        //start reaction
        reactionTimeLoop->start(); do
        {
            // set time for problem for implicit Euler scheme
            reactionProblem->setTime(reactionTimeLoop->time() + reactionTimeLoop->timeStepSize() );
            reactionProblem->setTimeStepSize(reactionTimeLoop->timeStepSize());

            reactionAssembler->setPreviousSolution(rOld);
            reactionNonLinearSolver.solve(r,*reactionTimeLoop);

            // make the new solution the old solution
            rOld = r;
            reactionGridVariables->advanceTimeStep();

            // advance to the time loop to the next step
            reactionTimeLoop->advanceTimeStep();

            // update the output fields before write
            reactionProblem->updateVtkOutput(r);

            // write vtk output
            reactionVtkWriter.write(reactionTimeLoop->time());

            // report statistics of this time step
            reactionTimeLoop->reportTimeStep();

//            //inherite the timestepsize
//            if(reactionTimeLoop->timeStepSize()>0) {
//                reactionDt = reactionTimeLoop->timeStepSize();
//            }
            // report and count episodes and tell the problem when advancing to the next episode
            if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength"))
            {
                if(reactionTimeLoop->isCheckPoint())
                {
                    int injType = reactionProblem->injectionType(reactionEpisodeIdx_);
                    std::cout<< "\n Episode " << reactionEpisodeIdx_
                             << " with injectionType " << injType
                             << " done for reaction \n" << std::endl;
                    reactionEpisodeIdx_++;
                    reactionProblem->setEpisodeIdx(reactionEpisodeIdx_);

//                    double reactionNumber = pow(10,int(log10(reactionDtMax)));
//                    std::cout<< reactionNumber << std::endl;
//                    reactionNumber = std::max(reactionNumber,reactionDtInitial);
//                    reactionDt = reactionNonLinearSolver.suggestTimeStepSize(reactionNumber);
//                    reactionDt = reactionNonLinearSolver.suggestTimeStepSize(reactionDtInitial/reactionDtFactor );
                }
                else{
                    std::cout<< "\n not checkpoint " <<  std::endl;
                    // set new dt as suggested by the newton solve
                    reactionTimeLoop->setTimeStepSize(reactionNonLinearSolver.suggestTimeStepSize(reactionTimeLoop->timeStepSize()));
                }
            }



        } while(!reactionTimeLoop->finished());

        // set the solver back to transport
        xOld = r;
        gridVariables->advanceTimeStep();

        reactionTimeLoop->finalize(leafGridView.comm());
        std::cout<< "finish calculating reactions\n "<< std::endl;

        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // update the output fields before write
        problem->updateVtkOutput(x);

        // write vtk output
        // reaction output as final output
//        vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength")) {
            if (timeLoop->isCheckPoint()) {
                int injType = problem->injectionType(episodeIdx_);
                std::cout << "\n Episode " << episodeIdx_
                          << " with injectionType " << injType
                          << " done \n" << std::endl;
                episodeIdx_++;
                problem->setEpisodeIdx(episodeIdx_);
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(dt));
            } else {
                // set new dt as suggested by the newton solver
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
                std::cout << "\n" << std::endl;
            }
        }


    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main

catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
              "). Most likely, the DGF file name is wrong "
              "or the DGF file is corrupted, "
              "e.g. missing hash at end of file or wrong number (dimensions) of entries."
              << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
