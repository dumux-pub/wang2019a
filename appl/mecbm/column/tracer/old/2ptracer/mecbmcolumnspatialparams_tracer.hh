 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for the tracer problem
 */
#ifndef DUMUX_MECBM_COLUMN_TRACER_SPATIAL_PARAMS_HH
#define DUMUX_MECBM_COLUMN_TRACER_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>
#include <iostream>

namespace Dumux {

/*!
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for the tracer problem
 */
template<class FVGridGeometry, class Scalar, int numFluidComps, int numActiveSolidComps>
class MECBMTracerSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                         MECBMTracerSpatialParams<FVGridGeometry, Scalar, numFluidComps,
                                 numActiveSolidComps>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar,
                                       MECBMTracerSpatialParams<FVGridGeometry, Scalar, numFluidComps, numActiveSolidComps>>;

    using EffectiveLaw = RegularizedBrooksCorey<Scalar>;

    using CoordScalar = typename GridView::ctype;
    enum { dimWorld=GridView::dimensionworld };
    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;

    using PoroLaw = PorosityPrecipitation<Scalar, numFluidComps, numActiveSolidComps>;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Tensor;
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    MECBMTracerSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry) {
        solubilityLimit_     = getParam<Scalar>("SpatialParams.SolubilityLimit", 0.26);
        initialPorosity_     = getParam<Scalar>("SpatialParams.Porosity", 0.11);
        initialPermeability_ = getParam<Scalar>("SpatialParams.Permeability", 2.23e-14);
        irreducibleLiqSat_   = getParam<Scalar>("SpatialParams.IrreducibleLiqSat", 0.2);
        irreducibleGasSat_   = getParam<Scalar>("SpatialParams.IrreducibleGasSat", 1e-3);
        pEntry1_             = getParam<Scalar>("SpatialParams.Pentry1", 500);
        bcLambda1_           = getParam<Scalar>("SpatialParams.BCLambda1", 2);

        // residual saturations
        materialParams_.setSwr(irreducibleLiqSat_);
        materialParams_.setSnr(irreducibleGasSat_);

        // parameters of Brooks & Corey Law
        materialParams_.setPe(pEntry1_);
        materialParams_.setLambda(bcLambda1_);
    }

    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    { return 1-referencePorosity(element, scv); }

    /*!
     *  \brief Define the initial porosity \f$[-]\f$ distribution
     *  For this special case, the initialPorosity is precalculated value
     *  as a referencePorosity due to an initial volume fraction of biofilm.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    {
        const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
        return referencePorosity_[eIdx][scv.indexInElement()];
    }

    /*!
   * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
   *  This value calculates the correct porosity that needs to be set
   *  in for the initialPorosity when an initial volume fraction of
   *  biofilm or other precipitate is in the system.
   *  This function makes use of evaluatePorosity from the porosityprecipitation law.
   *  The reference porosity is calculated as:
   *  \f[referencePorosity = initialPorosity + \sum \phi \f]
   *  which in making use of already available functions is
   *
   *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
   *
   * \param fvGridGeometry The fvGridGeometry
   * \param sol The (initial) solution vector
   */
    template<class SolutionVector>
    void computeReferencePorosity(const FVGridGeometry& fvGridGeometry,
                                  const SolutionVector& sol)
    {
        referencePorosity_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
            auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePorosity_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& globalPos = scv.dofPosition();
                const bool isGasTrap = isGasTrap_(globalPos);
                auto phi = isGasTrap ? 0.95 : initialPorosity_;
                auto phiEvaluated = poroLaw_.evaluatePorosity(element, scv, elemSol, phi);
                referencePorosity_[eIdx][scv.indexInElement()] = calculatephiRef(phi, phiEvaluated);
            }
        }
    }

    /*!
   *  \brief Define the minimum porosity \f$[-]\f$ after clogging caused by mineralization
   *
   *  \param element The finite element
   *  \param scv The sub-control volume
   */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        const auto refPoro = referencePorosity(element, scv);
        return poroLaw_.evaluatePorosity(element, scv, elemSol, refPoro);
    }

    /*!
     *  \brief Define the initial permeability \f$[m^2]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    PermeabilityType referencePermeability(const Element& element,
                                           const SubControlVolume &scv) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isGasTrap_(globalPos))
            return 1e-2;

        const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
        return referencePermeability_[eIdx][scv.indexInElement()];
    }

    /*!
   * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
   *  This value calculates the correct porosity that needs to be set
   *  in for the initialPorosity when an initial volume fraction of
   *  biofilm or other precipitate is in the system.
   *  This function makes use of evaluatePorosity from the porosityprecipitation law.
   *  The reference porosity is calculated as:
   *  \f[referencePorosity = initialPorosity + \sum \phi \f]
   *  which in making use of already available functions is
   *
   *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
   *
   * \param fvGridGeometry The fvGridGeometry
   * \param sol The (initial) solution vector
   */
    template<class SolutionVector>
    void computeReferencePermeability(const FVGridGeometry& fvGridGeometry,
                                      const SolutionVector& sol)
    {
        referencePermeability_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->fvGridGeometry().elementMapper().index(element);

            const auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePermeability_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                auto kInit = initialPermeability_;
                PermeabilityType K (0.0);
                for (int i = 0; i < dimWorld; ++i)
                    K[i][i] = kInit;
                const auto refPoro = referencePorosity(element, scv);
                const auto poro = porosity(element, scv, elemSol);
                auto kEvaluated = permLaw_.evaluatePermeability(K, refPoro, poro);
                K *= K[0][0]/kEvaluated[0][0];
                referencePermeability_[eIdx][scv.indexInElement()] = K;
            }
        }
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto refPerm = referencePermeability(element, scv);
        const auto refPoro = referencePorosity(element, scv);
        const auto poro = porosity(element, scv, elemSol);
        return permLaw_.evaluatePermeability(refPerm, refPoro, poro);
    }

    Scalar solidity(const SubControlVolume &scv) const
    { return 1.0 - porosityAtPos(scv.center()); }

    Scalar solubilityLimit() const
    { return solubilityLimit_; }

    Scalar theta(const SubControlVolume &scv) const
    { return 10.0; }

    /*!
    * \brief return the parameter object for the Brooks-Corey material law which depends on the position
    *
    * \param globalPos The global position
    */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    { return materialParams_; }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The position of the center of the element
     * \note we set a hydrophobic material
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

    //! Fluid properties that are spatial parameters in the tracer model
    //! They can possible vary with space but are usually constants

    //! Fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume& scv) const
    { return density_[this->fvGridGeometry().elementMapper().index(element)];; }

    void setDensity(const std::vector<Scalar>& d)
    { density_ = d; }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume& scv) const
    { return 0.018; }

    Scalar fluidMolarMass(const GlobalPosition &globalPos) const
    { return 0.018; }

    //! Velocity field
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry& fvGeometry,
                      const ElementVolumeVariables& elemVolVars,
                      const SubControlVolumeFace& scvf) const
    { return volumeFlux_[scvf.index()]; }

    void setVolumeFlux(const std::vector<Scalar>& f)
    { volumeFlux_ = f; }

    //! saturation from twoPProblem
    Scalar saturation(const Element &element,
                      const SubControlVolume& scv) const
    { return saturation_[scv.dofIndex()]; }

    void setSaturation(const std::vector<Scalar>& s)
    { saturation_ = s; }

private:
    std::vector<Scalar> volumeFlux_;
    std::vector<Scalar> density_;
    std::vector<Scalar> saturation_;


        Scalar calculatephiRef(Scalar phiInit, Scalar phiEvaluated)
        { return 2*phiInit - phiEvaluated;}

        bool isGasTrap_(const GlobalPosition &globalPos) const
        { return globalPos[dimWorld-1] > 0.13858;}

        MaterialLawParams materialParams_;

        PermeabilityKozenyCarman<PermeabilityType> permLaw_;
        PoroLaw poroLaw_;

        Scalar solubilityLimit_;

        Scalar initialPorosity_;
        std::vector< std::vector<Scalar> > referencePorosity_;
        Scalar initialPermeability_;
        std::vector< std::vector<PermeabilityType> > referencePermeability_;

        Scalar irreducibleLiqSat_;
        Scalar irreducibleGasSat_;
        Scalar pEntry1_;
        Scalar bcLambda1_;
};

} // end namespace Dumux

#endif
