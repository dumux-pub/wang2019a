// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Definition of the spatial parameters for the injection problem
 *        which uses the isothermal two-phase two-component fully implicit model.
 */

#ifndef DUMUX_MECBM_COLUMN_FLOW_SPATIAL_PARAMS_HH
#define DUMUX_MECBM_COLUMN_FLOW_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>
namespace Dumux {

/*!
 * \ingroup TwoPTwoCTests
 * \brief Definition of the spatial parameters for the injection problem
 *        which uses the isothermal two-phase two-component fully implicit model.
 */
    template<class FVGridGeometry, class Scalar>
    class MECBMColumnFlowSpatialParams
            : public FVSpatialParams<FVGridGeometry, Scalar,
                    MECBMColumnFlowSpatialParams<FVGridGeometry, Scalar>>
    {
        using GridView = typename FVGridGeometry::GridView;
        using FVElementGeometry = typename FVGridGeometry::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;
        using Element = typename GridView::template Codim<0>::Entity;
        using ParentType = FVSpatialParams<FVGridGeometry, Scalar,MECBMColumnFlowSpatialParams<FVGridGeometry, Scalar>>;

        static constexpr int dimWorld = GridView::dimensionworld;

        using EffectiveLaw = RegularizedBrooksCorey<Scalar>;

        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    public:
        //! Export the type used for the permeability
        using CoordScalar = typename GridView::ctype;
        using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;
        using PermeabilityType = Tensor;
        //! Export the material law type used
        using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
        using MaterialLawParams = typename MaterialLaw::Params;

        MECBMColumnFlowSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
                : ParentType(fvGridGeometry)
        {
            solubilityLimit_     = getParam<Scalar>("SpatialParams.SolubilityLimit", 0.26);
            irreducibleLiqSat_   = getParam<Scalar>("SpatialParams.IrreducibleLiqSat", 0.2);
            irreducibleGasSat_   = getParam<Scalar>("SpatialParams.IrreducibleGasSat", 1e-3);
            pEntry1_             = getParam<Scalar>("SpatialParams.Pentry1", 500);
            bcLambda1_           = getParam<Scalar>("SpatialParams.BCLambda1", 2);

            // residual saturations
            materialParams_.setSwr(irreducibleLiqSat_);
            materialParams_.setSnr(irreducibleGasSat_);

            // parameters of Brooks & Corey Law
            materialParams_.setPe(pEntry1_);
            materialParams_.setLambda(bcLambda1_);
        }


        /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
         *  on the position in the domain
         *
         * \param globalPos The global position
         */
        template<class ElementSolution>
        PermeabilityType permeability(const Element& element,
                                      const SubControlVolume& scv,
                                      const ElementSolution& elemSol) const
        {
            return permeability_[scv.dofIndex()];
        }

        /*!
         * \brief Returns the porosity \f$[-]\f$
         *
         * \param globalPos The global position
         */

        template<class ElementSolution>
        Scalar porosity(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
        {
            return porosity_[scv.dofIndex()];
        }


        Scalar solidity(const SubControlVolume &scv) const
        { return 1.0 - porosityAtPos(scv.center()); }

        Scalar solubilityLimit() const
        { return solubilityLimit_; }

        Scalar theta(const SubControlVolume &scv) const
        { return 10.0; }

        /*!
         * \brief return the parameter object for the Brooks-Corey material law which depends on the position
         *
         * \param globalPos The global position
         */
        const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
        { return materialParams_; }



        /*!
         * \brief Function for defining which phase is to be considered as the wetting phase.
         *
         * \param globalPos The position of the center of the element
         * \return The wetting phase index
         */
        template<class FluidSystem>
        int wettingPhaseAtPos(const GlobalPosition& globalPos) const
        { return FluidSystem::phase0Idx; }

        using  PermeabilityMatrix = std::map<int, PermeabilityType>;
        using  PorosityMatrix = std::map<int, Scalar>;

        void setPermeability(const PermeabilityMatrix& permeability){
            permeability_ = permeability;
        }

        void setPorosity(const PorosityMatrix porosity){
            porosity_ = porosity;
        }
    private:
        std::vector<Scalar> volumeFlux_;
        std::vector<Scalar> density_;
        std::vector<Scalar> saturation_;

        bool isGasTrap_(const GlobalPosition &globalPos) const
        { return globalPos[dimWorld-1] > 0.13858;}

        MaterialLawParams materialParams_;


        Scalar solubilityLimit_;

        Scalar irreducibleLiqSat_;
        Scalar irreducibleGasSat_;
        Scalar pEntry1_;
        Scalar bcLambda1_;

        mutable PermeabilityMatrix permeability_;
        mutable PorosityMatrix porosity_;
    };

} // end namespace Dumux

#endif
