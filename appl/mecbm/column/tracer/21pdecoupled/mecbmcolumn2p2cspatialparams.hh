// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Definition of the spatial parameters for the injection problem
 *        which uses the isothermal two-phase two-component fully implicit model.
 */

#ifndef DUMUX_MECBM_COLUMN_2P2C_SPATIAL_PARAMS_HH
#define DUMUX_MECBM_COLUMN_2P2C_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCTests
 * \brief Definition of the spatial parameters for the injection problem
 *        which uses the isothermal two-phase two-component fully implicit model.
 */
    template<class FVGridGeometry, class Scalar>
    class MECBMColumnFlowSpatialParams
            : public FVSpatialParams<FVGridGeometry, Scalar,
                    MECBMColumnFlowSpatialParams<FVGridGeometry, Scalar>>
    {
        using GridView = typename FVGridGeometry::GridView;
        using FVElementGeometry = typename FVGridGeometry::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;
        using Element = typename GridView::template Codim<0>::Entity;
        using ParentType = FVSpatialParams<FVGridGeometry, Scalar,MECBMColumnFlowSpatialParams<FVGridGeometry, Scalar>>;

        static constexpr int dimWorld = GridView::dimensionworld;

        using EffectiveLaw = RegularizedBrooksCorey<Scalar>;

        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;


    public:
        //! Export the type used for the permeability
        using CoordScalar = typename GridView::ctype;
        using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;
        using PermeabilityType = Tensor;
        //! Export the material law type used
        using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
        using MaterialLawParams = typename MaterialLaw::Params;

        MECBMColumnFlowSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
                : ParentType(fvGridGeometry)
        {
            solubilityLimit_     = getParam<Scalar>("SpatialParams.SolubilityLimit");
            irreducibleLiqSat_   = getParam<Scalar>("SpatialParams.IrreducibleLiqSat");
            irreducibleGasSat_   = getParam<Scalar>("SpatialParams.IrreducibleGasSat");
            pEntry1_             = getParam<Scalar>("SpatialParams.Pentry1");
            bcLambda1_           = getParam<Scalar>("SpatialParams.BCLambda1");

            permeablityValue          = getParam<Scalar>("SpatialParams.Permeability");
            porosityValue       = getParam<Scalar>("SpatialParams.Porosity");
            // residual saturations
            materialParams_.setSwr(irreducibleLiqSat_);
            materialParams_.setSnr(irreducibleGasSat_);

            // parameters of Brooks & Corey Law
            materialParams_.setPe(pEntry1_);
            materialParams_.setLambda(bcLambda1_);
        }


        /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
         *  on the position in the domain
         *
         * \param globalPos The global position
         */
        template<class ElementSolution>
        PermeabilityType permeability(const Element& element,
                                      const SubControlVolume& scv,
                                      const ElementSolution& elemSol) const
        {
            if(useSpatialParams_ == true)
            {return Kxx_[scv.dofIndex()];}
            else {return permeablityValue;}
        }


        /*!
         * \brief Returns the porosity \f$[-]\f$
         *
         * \param globalPos The global position
         */

        template<class ElementSolution>
        Scalar porosity(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
        {
            if(useSpatialParams_ == true)
            {return porosity_[scv.dofIndex()];}
            else {return porosityValue;}
        }


        Scalar solidity(const SubControlVolume &scv) const
        { return 1.0 - porosityAtPos(scv.center()); }

        Scalar solubilityLimit() const
        { return solubilityLimit_; }

        Scalar theta(const SubControlVolume &scv) const
        { return 10.0; }

        /*!
         * \brief return the parameter object for the Brooks-Corey material law which depends on the position
         *
         * \param globalPos The global position
         */
        const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
        { return materialParams_; }

        const auto relativePermeability(const Scalar& saturation)
        {
           return  MaterialLaw::krw(materialParams_, saturation);
        }

        /*!
         * \brief Function for defining which phase is to be considered as the wetting phase.
         *
         * \param globalPos The position of the center of the element
         * \return The wetting phase index
         */
        template<class FluidSystem>
        int wettingPhaseAtPos(const GlobalPosition& globalPos) const
        { return FluidSystem::phase0Idx; }


        void setSpatialParams(const std::vector<std::vector<double>>& spatialParms){
            porosity_ = spatialParms[0];
            Kxx_ = spatialParms[1];
            Kyy_ = spatialParms[2];
            Kzz_ = spatialParms[3];
            useSpatialParams_ = true;
        }

    private:

        bool isGasTrap_(const GlobalPosition &globalPos) const
        { return globalPos[dimWorld-1] > 0.13858;}

        MaterialLawParams materialParams_;

        Scalar solubilityLimit_;

        Scalar irreducibleLiqSat_;
        Scalar irreducibleGasSat_;
        Scalar pEntry1_;
        Scalar bcLambda1_;

        Scalar permeablityValue;
        Scalar porosityValue;

        std::vector<double> porosity_;
        std::vector<double> Kxx_;
        std::vector<double> Kyy_;
        std::vector<double> Kzz_;

        mutable bool useSpatialParams_= false;
    };

} // end namespace Dumux

#endif
