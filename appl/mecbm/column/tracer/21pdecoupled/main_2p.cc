// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TracerTests
 * \brief Test for the 2p tracer CC model
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

//#include <test/porousmediumflow/2p/implicit/incompressible/problem.hh>
#include "mecbmcolumn2p2cproblem.hh"
#include "mecbmcolumntracerproblem.hh"
#include "mecbmcolumnreactionproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    //! define the type tags for this problem
    using TwoPTypeTag = Properties::TTag::MECBMColumnFlowCCTpfaTypeTag;
    using TracerTypeTag = Properties::TTag::MECBMColumnTracerTpfa;
    using ReactionTypeTag = Properties::TTag::MECBMColumnReactionCCTpfaTypeTag;
    //! initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    //! print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TwoPTypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    /////////////////
    //// set 2p before timeloop
    ////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TwoPTypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (initial and boundary conditions)
    using TwoPProblem = GetPropType<TwoPTypeTag, Properties::Problem>;
    auto twoPProblem = std::make_shared<TwoPProblem>(fvGridGeometry);

    using GridView = GetPropType<TwoPTypeTag, Properties::GridView>;
    ////////////////////////////
    // set timeloop
    ////////////////////////////

    // get some time loop parameters
    using Scalar = GetPropType<TwoPTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // instantiate time loop
    int episodeIdx_ = 0;
    Scalar timeLoopEpisodeLength = 0;
    // set the time loop with episodes of various lengths as specified in the injection file
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    std::vector<Scalar> epiEnd_;

    // do it with episodes if an injection parameter file is specified
    // read parameters and set checkpoint
    if (hasParam("Injection.InjectionParamFile"))
    {
        //read injection parameters
        // first read the times of the checkpoints from the injection file
        std::string injectionParameters_ = getParam<std::string>("Injection.InjectionParamFile");
        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        Scalar tempTime = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while (!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while (!injectionData.eof())
        {
            getline(injectionData, row);
            if (row == "EndTimes")
            {
                getline(injectionData, row);
                while (row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempTime;
                        // injectionParameters_ contains the time in days!
                        epiEnd_.push_back(tempTime*3600*24);
                    }
                    getline(injectionData, row);
                }
            }
        }
        injectionData.close();

        // check the injection data against the number of injections specified in the parameter file
        int numInjections_ = getParam<Scalar>("Injection.numInjections");
        if (epiEnd_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection end times specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numEpisodes from injection data file = "<<epiEnd_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        // set the episode ends /check points:
        for (int epiIdx = 0; epiIdx < epiEnd_.size(); ++epiIdx)
        {
            timeLoop->setCheckPoint(epiEnd_[epiIdx]);
        }
        // set the initial episodeIdx in the problem to zero
        twoPProblem->setEpisodeIdx(episodeIdx_);
    }

        // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        timeLoopEpisodeLength = getParam<Scalar>("TimeLoop.EpisodeLength");
        timeLoop->setPeriodicCheckPoint(tEnd/timeLoopEpisodeLength);
        // set the initial episodeIdx in the problem to zero
        twoPProblem->setEpisodeIdx(episodeIdx_);
    }    // no episodes
    else
    {
        timeLoop->setCheckPoint(tEnd);
    }



    ////////////////////////////////////////////////////////////
    // set 2p Problem
    ////////////////////////////////////////////////////////////


    // the solution vector
    using TwoPSolutionVector = GetPropType<TwoPTypeTag, Properties::SolutionVector>;
    TwoPSolutionVector p(fvGridGeometry->numDofs());
    twoPProblem->applyInitialSolution(p);
    auto pOld = p;

    // maybe update the interface parameters
//    if (ENABLEINTERFACESOLVER)
//        twoPProblem->spatialParams().updateMaterialInterfaceParams(p);

    // the grid variables
    using TwoPGridVariables = GetPropType<TwoPTypeTag, Properties::GridVariables>;
    auto twoPGridVariables = std::make_shared<TwoPGridVariables>(twoPProblem, fvGridGeometry);
    twoPGridVariables->init(p);

    // intialize the vtk output module
    using TwoPIOFields = GetPropType<TwoPTypeTag, Properties::IOFields>;

    // use non-conforming output for the test with interface solver
    const auto ncOutput = getParam<bool>("Problem.UseNonConformingOutput", false);
    VtkOutputModule<TwoPGridVariables, TwoPSolutionVector> twoPVtkWriter(*twoPGridVariables, p, twoPProblem->name()+"_2p", "",
                                                                        (ncOutput ? Dune::VTK::nonconforming : Dune::VTK::conforming));
    using TwoPVelocityOutput = GetPropType<TwoPTypeTag, Properties::VelocityOutput>;
    twoPVtkWriter.addVelocityOutput(std::make_shared<TwoPVelocityOutput>(*twoPGridVariables));
    TwoPIOFields::initOutputModule(twoPVtkWriter); //!< Add model specific output fields
    //add specific output for k, cell-volume, moloutflux
    twoPVtkWriter.addField(twoPProblem->getKxx(), "Kxx");
    twoPVtkWriter.addField(twoPProblem->getKyy(), "Kyy");
    twoPVtkWriter.addField(twoPProblem->getKzz(), "Kzz");
    twoPVtkWriter.addField(twoPProblem->cellVolume(), "cellVolume");
    twoPVtkWriter.addField(twoPProblem->molOutFluxCH4w(), "molOutFluxCH4w");
    twoPVtkWriter.addField(twoPProblem->molOutFluxCH4n(), "molOutFluxCH4n");
    twoPProblem->updateVtkOutput(p);
    twoPVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using TwoPAssembler = FVAssembler<TwoPTypeTag, DiffMethod::numeric>;
    auto twoPAssembler = std::make_shared<TwoPAssembler>(twoPProblem, fvGridGeometry, twoPGridVariables, timeLoop, pOld);

    // the linear solver
    using TwoPLinearSolver = AMGBackend<TwoPTypeTag>;
    auto twoPLinearSolver = std::make_shared<TwoPLinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<TwoPAssembler, TwoPLinearSolver>;
    NewtonSolver nonLinearSolver(twoPAssembler, twoPLinearSolver);

    std::cout<<"2pproblem initialized"<<std::endl;
    ////////////////////////////////////////////////////////////
    // set tracer Problem
    ////////////////////////////////////////////////////////////

    //! the problem (initial and boundary conditions)
    using TracerProblem = GetPropType<TracerTypeTag, Properties::Problem>;
    auto tracerProblem = std::make_shared<TracerProblem>(fvGridGeometry);

    //! the solution vector
    using TracerSolutionVector = GetPropType<TracerTypeTag, Properties::SolutionVector>;
    TracerSolutionVector x(leafGridView.size(0));
    tracerProblem->applyInitialSolution(x);
    auto xOld = x;

    //! initialize the flux, density and saturation vectors
    std::vector<Scalar> volumeFlux_(fvGridGeometry->numScvf(), 0.0);
    std::vector<Scalar> density_(fvGridGeometry->numScv(), 0.);
    std::vector<Scalar> saturation_(fvGridGeometry->numScv(), getParam<Scalar>("Initial.initxwCH4"));
    std::vector<Scalar> relativePermeability_(fvGridGeometry->numScv(), 0);

    // set the flux, density and saturation from the 2p problem
    tracerProblem->spatialParams().setDensity(density_);
    tracerProblem->spatialParams().setSaturation(saturation_);



    //! the grid variables
    using TracerGridVariables = GetPropType<TracerTypeTag, Properties::GridVariables>;
    auto tracerGridVariables = std::make_shared<TracerGridVariables>(tracerProblem, fvGridGeometry);
    tracerGridVariables->init(x);

    // the linear solver
    using TracerLinearSolver = AMGBackend<TracerTypeTag>;
    auto tracerLinearSolver = std::make_shared<TracerLinearSolver>(leafGridView, fvGridGeometry->dofMapper());

     //! the linear system
    using JacobianMatrix = GetPropType<TracerTypeTag, Properties::JacobianMatrix>;
    auto A = std::make_shared<JacobianMatrix>();
    auto r = std::make_shared<TracerSolutionVector>();

    //! the assembler with time loop for instationary problem
    using TracerAssembler = FVAssembler<TracerTypeTag, DiffMethod::analytic, /*implicit=*/false>;
    auto tracerAssembler = std::make_shared<TracerAssembler>(tracerProblem, fvGridGeometry, tracerGridVariables, timeLoop, xOld);
    tracerAssembler->setLinearSystem(A, r);


    //! initialize the vtk output module
    VtkOutputModule<TracerGridVariables, TracerSolutionVector> vtkWriter(*tracerGridVariables, x, tracerProblem->name());
    using TracerIOFields = GetPropType<TracerTypeTag, Properties::IOFields>;
    TracerIOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    using TracerVelocityOutput = GetPropType<TracerTypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<TracerVelocityOutput>(*tracerGridVariables));
    vtkWriter.addField(tracerProblem->getKxx(), "Sw");
    vtkWriter.write(0.0);

    std::cout<<"tracer problem initialized"<<std::endl;
    ///////////////////////////////////////
    ///////reaction problem////////////////
    ///////////////////////////////////////

    //! the problem (initial and boundary conditions)
    using ReactionProblem = GetPropType<ReactionTypeTag, Properties::Problem>;
    auto reactionProblem = std::make_shared<ReactionProblem>(fvGridGeometry);

    //! the solution vector
    using ReactionSolutionVector = GetPropType<ReactionTypeTag, Properties::SolutionVector>;
    ReactionSolutionVector reactionSolutionVector(leafGridView.size(0));
    reactionProblem->applyInitialSolution(reactionSolutionVector);
    auto reactionSolutionVectorOld = reactionSolutionVector;

    //! the grid variables
    using ReactionGridVariables = GetPropType<ReactionTypeTag, Properties::GridVariables>;
    auto reactionGridVariables = std::make_shared<ReactionGridVariables>(reactionProblem, fvGridGeometry);
    reactionGridVariables->init(reactionSolutionVector);

    //! compute the reference
    reactionProblem->spatialParams().computeReferencePorosity(*fvGridGeometry, reactionSolutionVector);
    reactionProblem->spatialParams().computeReferencePermeability(*fvGridGeometry, reactionSolutionVector);


    /// intialize the vtk output module
    using ReactionIOFields = GetPropType<ReactionTypeTag, Properties::IOFields>;
    VtkOutputModule<ReactionGridVariables, ReactionSolutionVector> reactionVtkWriter(*reactionGridVariables,reactionSolutionVector, reactionProblem->name());

    reactionProblem->updateVtkOutput(reactionSolutionVector);
    ReactionIOFields::initOutputModule(reactionVtkWriter);
    reactionVtkWriter.write(0.0);

    // the linear solver
    using ReactionLinearSolver =  AMGBackend<ReactionTypeTag>;
    auto reactionLinearSolver = std::make_shared<ReactionLinearSolver>(leafGridView, fvGridGeometry->dofMapper());


    std::cout<<"reaction problem initialized"<<std::endl;
    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem
    ////////////////////////////////////////////////////////////

    // time loop
    timeLoop->start(); do
    {

        //get the spatial parameters from reaction problem
        reactionProblem->updateVtkOutput(reactionSolutionVector);
        twoPProblem->spatialParams().setSpatialParams(
                reactionProblem->getSpatialParams()
                );
        tracerProblem->spatialParams().setSpatialParam(
                reactionProblem->getSpatialParams()
                );

        // get the time for reaction timeloop
        auto reactiontimeStart = timeLoop->time();
        auto reactiontimeEnd   = timeLoop->time() + timeLoop->timeStepSize();
        auto reactionDt        = timeLoop->timeStepSize();
        std::cout<<"Start 2p2c"<<"\n"
                 <<"time         " << timeLoop->time() <<"\n"
                 <<"timeStepSize " << timeLoop->timeStepSize() <<std::endl;
        // set time for problem for implicit Euler scheme
        twoPProblem->setTime(timeLoop->time() + timeLoop->timeStepSize());
        twoPProblem->setTimeStepSize(timeLoop->timeStepSize());

        twoPAssembler->setPreviousSolution(pOld);
        // solve the non-linear system with time step control
        nonLinearSolver.solve(p, *timeLoop);

        // make the new solution the old solution
        pOld = p;
        twoPGridVariables->advanceTimeStep();

        // update the output fields before write
        twoPProblem->updateVtkOutput(p);

        // write vtk output
        twoPVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();


        // loop over elements to compute fluxes, saturations, densities for tracer
        using FluxVariables = GetPropType<TwoPTypeTag, Properties::FluxVariables>;
        auto upwindTerm = [](const auto& volVars) { return volVars.mobility(0.0); };
        for (const auto& element : elements(leafGridView))
        {
            ////////////////////////////////////////////////////////////
            // compute volume fluxes for the tracer model
            ///////////////////////////////////////////////////////////

            auto fvGeometry = localView(*fvGridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(twoPGridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, p);

            auto elemFluxVars = localView(twoPGridVariables->gridFluxVarsCache());
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto idx = scvf.index();

                if (!scvf.boundary())
                {
                    FluxVariables fluxVars;
                    fluxVars.init(*twoPProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                            volumeFlux_[idx] = fluxVars.advectiveFlux(0, upwindTerm);
                }
                else
                {
                    const auto bcTypes = twoPProblem->boundaryTypes(element, scvf);
                    if (bcTypes.hasOnlyDirichlet())
                    {
                        FluxVariables fluxVars;
                        fluxVars.init(*twoPProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                                volumeFlux_[idx] = fluxVars.advectiveFlux(0, upwindTerm);
                    }
                    if (bcTypes.hasOnlyNeumann())
                    {
                        const auto& ipGlobal = scvf.ipGlobal();
                        static constexpr Scalar eps_ = 1e-6;
                        if(ipGlobal[GridView::dimensionworld-1] < eps_){
                            volumeFlux_[idx] = twoPProblem->waterFluxAtBtm()*30;
                            std::cout<<"set flux::"<<volumeFlux_[idx]<<std::endl;
                        }
                    }
                }
            }

            std::cout<<"volume flux 0 ::" << volumeFlux_[0]<<std::endl;

//            std::cout<<"volume flux 0"<< volumeFlux_[0] <<"\n"
//                     <<"volume flux -1"<< volumeFlux_[80] <<"\n"<<std::endl;

            ////////////////////////////////////////////////////////////
            // compute densities and saturations for the tracer model
            ///////////////////////////////////////////////////////////
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const auto idx = scv.dofIndex();

                density_[idx] = volVars.density(0);
                saturation_[idx] = volVars.saturation(0);
                relativePermeability_[idx] = twoPProblem->spatialParams().relativePermeability(
                        saturation_[idx]
                        );
            }
        }

        ////////////////////////////////////////////////////////////
        // solve tracer problem on the same grid
        ////////////////////////////////////////////////////////////

        // set the flux from the 2p problem
        tracerProblem->spatialParams().setDensity(density_);
        tracerProblem->spatialParams().setSaturation(saturation_);
        tracerProblem->spatialParams().setRelativePermeability(relativePermeability_);
        tracerProblem->setFlowAtTop(twoPProblem->outFlowAtTop());
        tracerProblem->setFlowArea(twoPProblem->inFlowArea());

        tracerProblem->setInjectionType(twoPProblem->injectionType(episodeIdx_));
        tracerProblem->showInjection();

        std::cout<<"Start tracer"<<"\n"
                 <<"time         " << timeLoop->time() <<"\n"
                 <<"timeStepSize " << timeLoop->timeStepSize() <<std::endl;
        Dune::Timer tracerAssembleTimer;
        tracerAssembler->assembleJacobianAndResidual(x);
        tracerAssembleTimer.stop();

        // solve the linear system A(xOld-xNew) = r
        Dune::Timer solveTimer;
        TracerSolutionVector xDelta(x);
        tracerLinearSolver->solve(*A, xDelta, *r);
        solveTimer.stop();

        // update solution and grid variables
        Dune::Timer updateTimer;
        updateTimer.reset();
        x -= xDelta;
        tracerGridVariables->update(x);
        updateTimer.stop();

        // statistics
        Dune::Timer assembleTimer;
        const auto elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
        std::cout << "Assemble/solve/update time: "
                  <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
                  <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
                  <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
                  <<  std::endl;

        // make the new solution the old solution
        xOld = x;
        tracerGridVariables->advanceTimeStep();

        // write vtk output
        vtkWriter.write(timeLoop->time());

        // advance the time loop to the next step
        timeLoop->advanceTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength")) {
            if (timeLoop->isCheckPoint()) {
                int injType = twoPProblem->injectionType(episodeIdx_);
                std::cout << "\n Episode " << episodeIdx_
                          << " with injectionType " << injType
                          << " done \n" << std::endl;
                episodeIdx_++;
                twoPProblem->setEpisodeIdx(episodeIdx_);
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(dt));
            } else {
                // set new dt as suggested by the newton solver
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
                std::cout << "\n" << std::endl;
            }
        }


        ////////////////////////////////////////
        ///set assembler and solover for reaction
        ////////////////////////////////////////

        // set the time loop with episodes of various lengths as specified in the injection file
        auto reactionTimeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(reactiontimeStart, reactionDt, reactiontimeEnd);
        reactionTimeLoop->setMaxTimeStepSize(maxDt);

        // the assembler with time loop for instationary problem
        using ReactionAssembler = FVAssembler<ReactionTypeTag, DiffMethod::numeric>;
        auto reactionAssembler = std::make_shared<ReactionAssembler>(reactionProblem, fvGridGeometry, reactionGridVariables, reactionTimeLoop);

        // the non-linear solver
        using ReactionNewtonSolver = Dumux::NewtonSolver<ReactionAssembler, ReactionLinearSolver>;
        ReactionNewtonSolver reactionNonLinearSolver(reactionAssembler, reactionLinearSolver);
        std::vector<double> saturationForReaction;

        /// give the aqueous concentration to reaction
        std::cout<<"transfer value"<<std::endl;

        unsigned int maxSCV = twoPProblem->maxSCV();

        int pressureIdx = 0;
        int SnIdx = 1;
        int CH4Idx = 1;

        saturationForReaction.resize(maxSCV);

        for (int scvIndex = 0; scvIndex < maxSCV; ++scvIndex) {
            //pressure
            reactionSolutionVectorOld[scvIndex][pressureIdx] = p[scvIndex][pressureIdx];

            //saturation
            reactionSolutionVectorOld[scvIndex][CH4Idx] = p[scvIndex][CH4Idx];

            //tracer components
            for (int componentIdx = 0; componentIdx < 5 ; ++componentIdx) {
                reactionSolutionVectorOld[scvIndex][2+componentIdx] = x[scvIndex][componentIdx];
            }


        }

        std::cout<<"transfer value finished"<<std::endl;


        //! apply reaction problem
        // time loop
        reactionTimeLoop->start(); do
        {

            reactionProblem->setTime(reactionTimeLoop->time() + reactionTimeLoop->timeStepSize());
            reactionProblem->setTimeStepSize(reactionTimeLoop->timeStepSize());

            std::cout<<"Start reaction"<<"\n"
                     <<"time          " << reactionTimeLoop->time() <<"\n"
                     <<"timeStepSize  " << reactionTimeLoop->timeStepSize() <<std::endl;
            reactionAssembler->setPreviousSolution(reactionSolutionVectorOld);
            reactionNonLinearSolver.solve(reactionSolutionVector,*reactionTimeLoop);

            // make the new solution the old solution
            reactionSolutionVectorOld = reactionSolutionVector;


            reactionGridVariables->advanceTimeStep();

            // update the output fields before write
            reactionProblem->updateVtkOutput(reactionSolutionVector);

            // write vtk output
//            reactionVtkWriter.write(reactionTimeLoop->time());

            // report statistics of this time step
            // advance the time loop to the next step
            reactionTimeLoop->advanceTimeStep();

            reactionTimeLoop->setTimeStepSize(reactionNonLinearSolver.suggestTimeStepSize(reactionTimeLoop->timeStepSize()));
//        reactionTimeLoop->reportTimeStep();

        }while (!reactionTimeLoop->finished());


// write vtk output
        reactionVtkWriter.write(reactionTimeLoop->time());


        for (int scvIndex = 0; scvIndex < maxSCV; ++scvIndex) {
            /// give pressure to 2p2c
            pOld[scvIndex][pressureIdx] = reactionSolutionVectorOld[scvIndex][pressureIdx];
            pOld[scvIndex][CH4Idx] = reactionSolutionVectorOld[scvIndex][CH4Idx];
            /// give concentration to tracer
            for (int componentIdx = 0; componentIdx < 5 ; ++componentIdx) {
                xOld[scvIndex][componentIdx] = reactionSolutionVectorOld[scvIndex][2+componentIdx];

            }
        }
        reactionTimeLoop->finalize(leafGridView.comm());
        x = xOld;
        p = pOld;



    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    //! print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main

catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
