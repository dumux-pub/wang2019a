// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TracerTests
 * \brief  A 2p problem with multiple tracer bands in a porous groundwater reservoir with a lens
 */
#ifndef DUMUX_TWOP_TRACER_MECBM_PROBLEM_HH
#define DUMUX_TWOP_TRACER_MECBM_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/base.hh>

#include "spatialparams_tracer.hh"

namespace Dumux {
/*!
 * \ingroup TracerTests
 * \brief A 2p problem with multiple tracer bands in a porous groundwater reservoir with a lens
 */
template <class TypeTag>
class TwoPTracerTestProblem;

namespace Properties {
//Create new type tags
namespace TTag {
struct TwoPTracerTest { using InheritsFrom = std::tuple<Tracer>; };
struct TwoPTracerTestTpfa { using InheritsFrom = std::tuple<TwoPTracerTest, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPTracerTest> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPTracerTest> { using type = TwoPTracerTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPTracerTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TwoPTracerTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::TwoPTracerTest> { static constexpr bool value = true; };
template<class TypeTag>
struct SolutionDependentMolecularDiffusion<TypeTag, TTag::TwoPTracerTestTpfa> { static constexpr bool value = false; };

//! A simple fluid system with one tracer component
template<class TypeTag>
class TracerFluidSystem : public FluidSystems::Base<GetPropType<TypeTag, Properties::Scalar>,
                                                                TracerFluidSystem<TypeTag>>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

public:
    //! If the fluid system only contains tracer components
    static constexpr bool isTracerFluidSystem()
    { return true; }

    //! No component is the main component
    static constexpr int getMainComponent(int phaseIdx)
    { return -1; }

    //! The number of components
    static constexpr int numComponents = 4;

    using Acetate   = Components::Acetate<Scalar>;
    using Amendment = Components::CarAm<Scalar>;
    using RMethyl   = Components::RMethyl<Scalar>;
    using CO2       = Dumux::Components::CO2<Scalar, HeterogeneousCO2Tables::CO2Tables>;

    static constexpr int AcetateIdx = 0;
    static constexpr int AmendmentIdx= 1;
    static constexpr int RMethylIdx  = 2;
    static constexpr int CO2dx       = 3;
    //! Human readable component name (index compIdx) (for vtk output)
    static std::string componentName(int compIdx)
    {
        static std::string name[] = { Acetate::name(),
                                      Amendment::name(),
                                      RMethyl::name(),
                                      CO2::name() };
        return name[compIdx];
    }

    //! Molar mass in kg/mol of the component with index compIdx
    static Scalar molarMass(unsigned int compIdx)
    {
        static const Scalar M[] = {Acetate::molarMass(),
                                   Amendment::molarMass(),
                                   RMethyl::molarMass(),
                                   CO2::molarMass() };
        return M[compIdx]; }

    //! Binary diffusion coefficient
    //! (might depend on spatial parameters like pressure / temperature)
    static Scalar binaryDiffusionCoefficient(unsigned int compIdx,
                                             const Problem& problem,
                                             const Element& element,
                                             const SubControlVolume& scv)
    {
        return result = 0.67e-9;
    }
};

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPTracerTest> { using type = TracerFluidSystem<TypeTag>; };

} // end namespace Properties

/*!
 * \ingroup TracerTests
 *
 * \brief Definition of a problem, for the tracer problem:
 * A lens of contaminant tracer is diluted by diffusion and a base groundwater flow
 *
 * This problem uses the \ref TracerModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_2ptracer -ParameterFile ./params.input</tt> or
 * <tt>./test_2ptracer -ParameterFile ./params.input</tt>
 */
template <class TypeTag>
class TwoPTracerTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();

    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    enum{
        //Indices of the components
        numComponents = FluidSystem::numComponents,
        AcetateIdx  = FluidSystem::AcetateIdx,
        AmendmentIdx= FluidSystem::AmendmentIdx,
        RMethylIdx  = FluidSystem::RMethylIdx,
        CO2dx       = FluidSystem::CO2Ix,
    };

public:
    TwoPTracerTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions" << '\n';
        else
            std::cout<<"problem uses mass fractions" << '\n';

        injQ_  = getParam<Scalar>("Injection.injQ");
        initMassConcAmendment   = getParam<Scalar>("Initial.initMassConcAmendment"); // Initial mass concentration of Amendment

    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);
        return initialValues;
    }

    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];


        Scalar area = M_PI*0.0525*0.0525/4;

        Scalar waterFlux = injQ_/area;//*0.49/area;// [m/s]

        //! Inflow boundary at bottom

        int injProcess = injType_[episodeIdx_];

        if(ipGlobal[dimWorld-1] < eps_) // standard: no injection
        {
            if (injProcess == 2) //water injection with Amendment
            {
                flux[AmendmentIdx] = - 1e-6* initMassConcAmendment // 15000 kg/m3liq
                                         / FluidSystem::molarMass(AmendmentIdx) // 0.113 kg/mol -> mol/m3liq
                                         / (61 *86400); // 61 days streched out injection to emulate cloquing sec injection duration --> mol/(m3tot * s)
            }
            else if (injProcess == 0) // water injection
            {
                flux = 0.0;
//                flux[comp0Idx] = 0;
//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
            }
        }
        else
            flux = 0.0; // kg/m/s

        // no-flow everywhere except at the top boundary
        if(ipGlobal[dimWorld-1] < this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps_)
            return flux;

    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);

        if (onStripe1_(globalPos) || onStripe2_(globalPos) || onStripe3_(globalPos))
        {
            if (useMoles)
                initialValues = 1e-9;
            else
                initialValues = 1e-9*FluidSystem::molarMass(0)/this->spatialParams().fluidMolarMass(globalPos);
        }
        return initialValues;
    }

    void setInjectionType(const std::vector<int>& injt){
        injType_ = injt;
    }

    void setEpisode(const int& episode){
        episodeIdx_ = episode;
    }
private:
    static constexpr Scalar eps_ = 1e-6;
    Scalar injQ_;
    std::vector<int> injType_;
    Scalar episodeIdx_ = 0;
    Scalar initMassConcAmendment;      // Initial wetting mole fraction of Amendment



};

} //end namespace Dumux

#endif
