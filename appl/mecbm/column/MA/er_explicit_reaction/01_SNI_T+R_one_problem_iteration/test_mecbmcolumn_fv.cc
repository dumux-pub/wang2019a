// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the two-phase n-component finite volume model used to model e.g. salt dissolution.
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "mecbmcolumnproblem.hh" // insert the needed problem file here
#include "mecbmcolumnreaction.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <cassert>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::MECBMColumnCCTpfaTypeTag;
    using ReactionTag = Properties::TTag::MECBMColumnReactionCCTpfaTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();


    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    using Reaction = GetPropType<ReactionTag, Properties::Problem>;
    auto reaction = std::make_shared<Reaction>(fvGridGeometry);

    std::cout<< "problem finished" << std::endl;
    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    using SolutionVectorReaction = GetPropType<ReactionTag, Properties::SolutionVector>;
    SolutionVectorReaction r(fvGridGeometry->numDofs());
    reaction->applyInitialSolution(r);
    auto rOld = r;

    // initialize the spatialParams separately to compute the referencePorosity and referencePermeability
    problem->spatialParams().computeReferencePorosity(*fvGridGeometry, x);
    problem->spatialParams().computeReferencePermeability(*fvGridGeometry, x);

    reaction->spatialParams().computeReferencePorosity(*fvGridGeometry, r);
    reaction->spatialParams().computeReferencePermeability(*fvGridGeometry, r);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    using GridVariablesReaction = GetPropType<ReactionTag, Properties::GridVariables>;
    auto gridVariablesReaction = std::make_shared<GridVariablesReaction>(reaction, fvGridGeometry);
    gridVariablesReaction->init(r);

    std::cout<< "grid variables finished" << std::endl;
    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    const Scalar episodeLength= getParam<Scalar>("TimeLoop.Episode");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");


    std::cout<< "configuring vtkwriter" << std::endl;

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    std::cout<< "configuring vtkwriter1" << std::endl;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    std::cout<< "configuring vtkwriter2" << std::endl;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    std::cout<< "configuring vtkwriter3" << std::endl;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    std::cout<< "configuring vtkwriter4" << std::endl;

    std::cout<<x<<std::endl;
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields

    std::cout<< "add vtkwriter output" << std::endl;
    //add specific output
    vtkWriter.addField(problem->getKxx(), "Kxx");
    vtkWriter.addField(problem->getKyy(), "Kyy");
    vtkWriter.addField(problem->getKzz(), "Kzz");
    vtkWriter.addField(problem->cellVolume(), "cellVolume");
    vtkWriter.addField(problem->molOutFluxCH4w(), "molOutFluxCH4w");
    vtkWriter.addField(problem->molOutFluxCH4n(), "molOutFluxCH4n");

    // update the output fields before write
    problem->updateVtkOutput(x);
    vtkWriter.write(0.0);

    std::cout<< "setting timeloop " << std::endl;
    // instantiate time loop
    int episodeIdx_ = 0;
    // set the time loop with episodes of various lengths as specified in the injection file
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    // do it with episodes if an injection parameter file is specified
    if (hasParam("Injection.InjectionParamFile"))
    {
        // first read the times of the checkpoints from the injection file
        std::vector<Scalar> epiEnd_;
        std::string injectionParameters_ = getParam<std::string>("Injection.InjectionParamFile");
        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        Scalar tempTime = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while (!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while (!injectionData.eof())
        {
            getline(injectionData, row);
            if (row == "EndTimes")
            {
                getline(injectionData, row);
                while (row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempTime;
                        // injectionParameters_ contains the time in days!
                        epiEnd_.push_back(tempTime*3600*24);
                    }
                    getline(injectionData, row);
                }
            }
        }
        injectionData.close();

        // check the injection data against the number of injections specified in the parameter file
        int numInjections_ = getParam<Scalar>("Injection.numInjections");
        if (epiEnd_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection end times specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numEpisodes from injection data file = "<<epiEnd_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        // set the time loop with episodes of various lengths as specified in the injection file
//        auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
//        timeLoop->setMaxTimeStepSize(maxDt);
        // set the episode ends /check points:
        for (int epiIdx = 0; epiIdx < epiEnd_.size(); ++epiIdx)
        {
            timeLoop->setCheckPoint(epiEnd_[epiIdx]);
        }
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
    }

        // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        // set the time loop with periodic episodes of constant length
        // auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
        // timeLoop->setMaxTimeStepSize(maxDt);
        timeLoop->setPeriodicCheckPoint(tEnd/getParam<Scalar>("TimeLoop.EpisodeLength"));
        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
    }    // no episodes
    else
    {
        // set the time loop without episodes
        // auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
        // timeLoop->setMaxTimeStepSize(maxDt);
        timeLoop->setCheckPoint(tEnd);
    }

    auto timeLoopReaction = timeLoop;

    std::cout<< "time loop finished" << std::endl;

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    using AssemblerReaction = FVAssembler<ReactionTag, DiffMethod::numeric>;
    auto assemblerReaction = std::make_shared<AssemblerReaction>(reaction, fvGridGeometry, gridVariablesReaction, timeLoop);

    // the linear solver
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    using LinearSolverReaction = AMGBackend<ReactionTag>;
    auto linearSolverReaction = std::make_shared<LinearSolverReaction>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolverPrp = NewtonSolver<Assembler, LinearSolver>;
    NewtonSolverPrp nonLinearSolver(assembler, linearSolver);

    using NewtonSolverReaction = NewtonSolver<AssemblerReaction, LinearSolverReaction>;
    NewtonSolverReaction nonLinearSolverReaction(assemblerReaction, linearSolverReaction);

    std::cout<< "solvers finished" << std::endl;

    // get some indexes here
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = TwoPNCIndices;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx   = Indices::switchIdx, //Saturation

        //Indices of the phases
                phase0Idx = FluidSystem::phase0Idx,
        phase1Idx = FluidSystem::phase1Idx,

        //Indices of the components
                numComponents = FluidSystem::numComponents,
        comp0Idx    = FluidSystem::BrineIdx,
        comp1Idx    = FluidSystem::CH4Idx,
        CH4Idx      = FluidSystem::CH4Idx,
        AcetateIdx  = FluidSystem::AcetateIdx,
        AmendmentIdx= FluidSystem::AmendmentIdx,
        RMethylIdx  = FluidSystem::RMethylIdx,
        HydrogenIdx = FluidSystem::H2Idx,
        TCIdx       = FluidSystem::TCIdx,

        numSolidComponents= SolidSystem::numComponents - SolidSystem::numInertComponents,
        CoalBacPhaseIdx   = SolidSystem::CoalBacPhaseIdx,
        AmCoalBacPhaseIdx = SolidSystem::AmCoalBacPhaseIdx,
        AcetoArchPhaseIdx = SolidSystem::AcetoArchPhaseIdx,
        HydroArchPhaseIdx = SolidSystem::HydroArchPhaseIdx,
        MethyArchPhaseIdx = SolidSystem::MethyArchPhaseIdx,
        CCoalPhaseIdx     = SolidSystem::CCoalPhaseIdx,

        //Indices of the bio/coal volume fractions
                CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
        AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
        AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
        HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
        MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
        CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,

        //Index of the primary component of G and L phase
                conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
                wPhaseOnly = Indices::firstPhaseOnly,
        nPhaseOnly = Indices::secondPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
                dim      = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    //parameters for reaction
    //growth and decay rate coefficients
    Scalar muCoalBacCoal_  = getParam<Scalar>("BioCoefficients.muCoalBacC");   // Maximum specific growth rate for Primary Bacteria on Coal
    Scalar muAmCoalBacCoal_= getParam<Scalar>("BioCoefficients.muAmCoalBacC"); // Maximum specific growth rate for Secondary Bacteria on Coal
    Scalar muAmCoalBacAm_  = getParam<Scalar>("BioCoefficients.muAmCoalBacAm");// Maximum specific growth rate for Secondary Bacteria on Amendment
    Scalar muAcetoArch_    = getParam<Scalar>("BioCoefficients.muAcetoArch");  // Maximum specific growth rate for Acetoclastic Archaea
    Scalar muHydroArch_    = getParam<Scalar>("BioCoefficients.muHydroArch");  // Maximum specific growth rate for Hydrogenotrophic Archaea
    Scalar muMethyArch_    = getParam<Scalar>("BioCoefficients.muMethyArch");  // Maximum specific growth rate for Methylotrophic Archaea
    Scalar K_              = muAmCoalBacAm_/100; //getParam<Scalar>("BioCoefficients.K");            // Decay Rate for Microbes

    //Monod half saturation constants
    Scalar Kc_     = getParam<Scalar>("BioCoefficients.Kc");           // Monod Half saturation Constant for Coal
    Scalar Kh_     = getParam<Scalar>("BioCoefficients.KH2");          // Monod Half saturation Constant for Hydrogen
    Scalar KAm_    = getParam<Scalar>("BioCoefficients.KAm");          // Monod Half saturation Constant for Amendment
    Scalar KAc_    = getParam<Scalar>("BioCoefficients.KAc");          // Monod Half saturation Constant for Acetate
    Scalar KCH3_   = getParam<Scalar>("BioCoefficients.KCH3");         // Monod Half saturation Constant for Methyl

    //Yields
    Scalar Yccbc_  = getParam<Scalar>("BioCoefficients.YCoalBacC");    // Yield of coal consuming Bacteria Biomass on Coal
    Scalar Yacbc_  = getParam<Scalar>("BioCoefficients.YAmCoalBacC");  // Yield of coal & amendment consuming Bacteria on Coal
    Scalar Yacbam_ = getParam<Scalar>("BioCoefficients.YAmCoalBacAm"); // Yield of coal & amendment consuming Bacteria on Amendment
    Scalar amFac_  = getParam<Scalar>("BioCoefficients.amFac", 1); // amendmentFactor
    Scalar YhaH2_  = getParam<Scalar>("BioCoefficients.YHydroArchH2"); // Yield of Hydrogenotrophic Archaea Biomass on Hydrogen
    Scalar YaaAc_  = getParam<Scalar>("BioCoefficients.YAcetoArchAc"); // Yield of Acetoclastic Archaea Biomass on Acetate
    Scalar YmaCH3_ = getParam<Scalar>("BioCoefficients.YMethyArchCH3");// Yield of Methylotrophic Archaea Biomass on Methyl
    Scalar YH2cc_  = getParam<Scalar>("BioCoefficients.YH2cc");        // Yield of Hydrogen from Coal
    Scalar YAccc_  = getParam<Scalar>("BioCoefficients.YAccc");        // Yield of Acetate from Coal
    Scalar YH2Am_  = getParam<Scalar>("BioCoefficients.YH2Am")*amFac_;        // Yield of Hydrogen from Amendment
    Scalar YAcAm_  = getParam<Scalar>("BioCoefficients.YAcAm")*amFac_;        // Yield of Acetate from Amendment
    Scalar YCH3Am_ = getParam<Scalar>("BioCoefficients.YCH3Am")*amFac_;       // Yield of Methyl from Amendment
    Scalar YCH4Ac_ = getParam<Scalar>("BioCoefficients.YCH4Ac");       // Yield of CH4 from Acetate
    Scalar YCH4H2_ = getParam<Scalar>("BioCoefficients.YCH4H2");       // Yield of CH4 from Hydrogen
    Scalar YCH4CH3_= getParam<Scalar>("BioCoefficients.YCH4CH3");      // Yield of CH4 from Methyl
    Scalar YCO2Ac_ = getParam<Scalar>("BioCoefficients.YCO2Ac");       // Yield of CO2 from Acetate
    Scalar YCO2CH3_ = getParam<Scalar>("BioCoefficients.YCO2CH3");       // Yield of CO2 from Methyl

    Scalar temperature_ = getParam<Scalar>("Problem.Temperature");
    Scalar CCoalLimit = getParam<Scalar>("Limit.CCoal");
    Scalar HydrogenLimit = getParam<Scalar>("Limit.H2");
    Scalar timeLimit = getParam<Scalar>("Limit.time");

    NumEqVector zero(0.0);
    std::vector<NumEqVector> sourceMatrix(fvGridGeometry->numScv(), zero);

    auto xOldOld = xOld;
    Scalar tOld;

    bool ifReducedTimeStep = false;
    int writeTimeStep = getParam<Scalar>("Limit.writeTime"); // write vtk at certain time
    int writeTime = writeTimeStep;


    enum{
        nw = 0, //unit [mol]
        xwH2O = 1,
        xwCH4 = 2,
        xwH2 = 3,
        ng = 4,
        xgH2O = 5,
        xgCH4 = 6,
        xgH2 = 7,

        //henry
        henryH20 = 0,
        henryCH4 = 1,
        henryH2 = 2,

        /// feedstate;
        nf = 0, //unit [mol]
        xfH2O = 1, //unit[mol/mol]
        xfCH4 = 2,
        xfH2 = 3,
        rhoW = 4,
        porVol = 5,
        pW = 6,
    };
    //todo:: CO2 pressure here
    std::vector<Scalar> pressureCo(numComponents);
    for (int compIdx = 0; compIdx < numComponents; ++compIdx) {
        if (compIdx == 0) {
            pressureCo[compIdx] = FluidSystem::waterSatPressure(temperature_);
        }
        else{
            pressureCo[compIdx] = FluidSystem::getHenry(compIdx,temperature_);
        }
    }

    problem->setHenry(pressureCo);

    std::cout << "pressure co" << std::endl;
    for (int compIdx = 0; compIdx < numComponents; ++compIdx) {
        std::cout<< FluidSystem::componentName(compIdx) << ": " << pressureCo[compIdx]<< std::endl;
    }

    // time loop
    timeLoop->start(); do
    {


//        auto refTimeStepSize = timeLoop -> timeStepSize();
//
//
//        ////////
//        /// R/2
//        ///////
//        timeLoop->setTimeStepSize(refTimeStepSize/2);
//
//        reaction->setTime( timeLoop->time() + timeLoop->timeStepSize());
//        reaction->setTimeStepSize( timeLoop->timeStepSize());
//
//        reaction->initialSourceTerm();
//        reaction->calReaction(gridVariablesReaction,rOld);
//
//        assemblerReaction->setPreviousSolution(rOld);
//        nonLinearSolverReaction.solve(r, *timeLoop);
//        gridVariablesReaction->advanceTimeStep();
//
//        rOld = r;
//
//        ////////
//        /// T
//        ///////
//        timeLoop->setTimeStepSize(refTimeStepSize);
        problem->setTime( timeLoop->time() + timeLoop->timeStepSize() );
        problem->setTimeStepSize( timeLoop->timeStepSize());
        // set previous solution for storage evaluations
//        assembler->setPreviousSolution(r);
        assembler->setPreviousSolution(xOld);
        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);
        gridVariables->advanceTimeStep();

        problem->calReaction(gridVariables,x);

        xOld = x;

//        reaction->initialSourceTerm();
//        reaction->calReaction(gridVariables,x);
//        // set previous solution for storage evaluations
//        assemblerReaction->setPreviousSolution(x);
//        // solve the non-linear system with time step control
//        nonLinearSolverReaction.solve(r, *timeLoop);
//        gridVariablesReaction->advanceTimeStep();

        //////////////
        /// 1/2R
        ////////////
//        timeLoop->setTimeStepSize(refTimeStepSize/2);
//
//        reaction->setTime( timeLoop->time() + timeLoop->timeStepSize() );
//        reaction->setTimeStepSize( timeLoop->timeStepSize() );




//        timeLoop->setTimeStepSize(refTimeStepSize);
        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // update the output fields before write
        problem->updateVtkOutput(x);

        //write CH4 data output file
//        problem->writeFluxes(dataFile, fileName, x);


        if(timeLoop->time() >= writeTime)
        {
            // write vtk output
            vtkWriter.write(timeLoop->time());
            writeTime += writeTimeStep;
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        if (!hasParam("Injection.InjectionParamFile") || !hasParam("TimeLoop.EpisodeLength"))
            if(timeLoop->isCheckPoint())
            {
                int injType = problem->injectionType(episodeIdx_);
                std::cout<< "\n Episode " << episodeIdx_
                         << " with injectionType " << injType
                         << " done \n" << std::endl;
                episodeIdx_++;
                problem->setEpisodeIdx(episodeIdx_);
                timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(dt));
            }


        if(ifReducedTimeStep)
        {
            timeLoop->setTimeStepSize(timeLimit);
        }else{
            // set new dt as suggested by the newton solver
            timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
        }

    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
              "). Most likely, the DGF file name is wrong "
              "or the DGF file is corrupted, "
              "e.g. missing hash at end of file or wrong number (dimensions) of entries."
              << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
