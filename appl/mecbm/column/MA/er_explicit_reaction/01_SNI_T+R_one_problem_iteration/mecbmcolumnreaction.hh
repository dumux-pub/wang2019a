// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
#ifndef DUMUX_MECBM_COLUMN_REACTION_HH
#define DUMUX_MECBM_COLUMN_REACTION_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/2pncmin/modelos.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/brinech4co2.hh>
#include <dumux/material/solidsystems/mecbm.hh>

#include "mecbmcolumnspatialparams.hh"
#include <dumux/material/chemistry/biogeochemistry/mecbmreactions.hh>

#include <test/porousmediumflow/co2/implicit/co2tables.hh>
#include <functional>
#include <map>
#include <cassert>
namespace Dumux {
/*!
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
    template <class TypeTag>
    class MECBMColumnReaction;

    namespace Properties {
// Create new type tags
        namespace TTag {
            struct MECBMColumnReactionTypeTag { using InheritsFrom = std::tuple<TwoPNCMinOS>; };
            struct MECBMColumnReactionBoxTypeTag { using InheritsFrom = std::tuple<MECBMColumnReactionTypeTag, BoxModel>; };
            struct MECBMColumnReactionCCTpfaTypeTag { using InheritsFrom = std::tuple<MECBMColumnReactionTypeTag, CCTpfaModel>; };
        } // end namespace TTag

// Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::MECBMColumnReactionTypeTag> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::MECBMColumnReactionTypeTag> { using type = MECBMColumnReaction<TypeTag>; };

// Set fluid configuration
        template<class TypeTag>
        struct FluidSystem<TypeTag, TTag::MECBMColumnReactionTypeTag> {
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = FluidSystems::BrineCH4CO2<Scalar,
            HeterogeneousCO2Tables::CO2Tables,
            Components::TabulatedComponent<Components::H2O<GetPropType<TypeTag, Properties::Scalar>>>,
            FluidSystems::BrineCH4CO2DefaultPolicy</*constantSalinity=*/true, false>>;
        };

        template<class TypeTag>
        struct SolidSystem<TypeTag, TTag::MECBMColumnReactionTypeTag> {
            using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
            using type = SolidSystems::MECBMSolidPhase<Scalar>;
        };

// Set the spatial parameters
        template<class TypeTag>
        struct SpatialParams<TypeTag, TTag::MECBMColumnReactionTypeTag>{
            using MT = GetPropType<TypeTag, ModelTraits>;
            static constexpr int numFluidComps = MT::numFluidComponents();
            static constexpr int numActiveSolidComps = MT::numSolidComps() - MT::numInertSolidComps();
            using type = MECBMColumnSpatialParams<GetPropType<TypeTag, FVGridGeometry>, GetPropType<TypeTag, Scalar>, numFluidComps, numActiveSolidComps>;
        };

//Set properties here to override the default property settings
        template<class TypeTag>
        struct ReplaceCompEqIdx<TypeTag, TTag::MECBMColumnReactionTypeTag> { static constexpr int value = 9; }; //!< Replace gas balance by total mass balance

// Default formulation is pw-Sn, overwrite if necessary
        template<class TypeTag>
        struct Formulation<TypeTag, TTag::MECBMColumnReactionTypeTag>
        { static constexpr auto value = TwoPFormulation::p0s1; };

// Enable caching or not (reference solutions created without caching)
// SET_BOOL_PROP(MECBMColumnReactionTypeTag, EnableFVGridGeometryCache, true);
// SET_BOOL_PROP(MECBMColumnReactionTypeTag, EnableGridVolumeVariablesCache, true);
// SET_BOOL_PROP(MECBMColumnReactionTypeTag, EnableGridFluxVariablesCache, true);

    }

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water is injected to flush precipitated salt in a gas reservoir clogged due to precipitated salt.
 *
 * The domain is sized 10m times 20m and contains a vertical low-permeable layer of precipitated salt near an extraction well.
 *
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2pncmin</tt>
 */
    template <class TypeTag>
    class MECBMColumnReaction : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
        using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
        using Chemistry = typename Dumux::MECBMReactionsChemistry<TypeTag>;
        using Indices = TwoPNCIndices;
//        using SpatialParams = typename GetPropType<TypeTag, Properties::SpatialParams>;

        enum {
            pressureIdx = Indices::pressureIdx,
            switchIdx   = Indices::switchIdx, //Saturation

            //Indices of the phases
                    phase0Idx = FluidSystem::phase0Idx,
            phase1Idx = FluidSystem::phase1Idx,

            //Indices of the components
            numComponents = FluidSystem::numComponents,
            comp0Idx    = FluidSystem::BrineIdx,
            comp1Idx    = FluidSystem::CH4Idx,
            AcetateIdx  = FluidSystem::AcetateIdx,
            AmendmentIdx= FluidSystem::AmendmentIdx,
            RMethylIdx  = FluidSystem::RMethylIdx,
            H2Idx       = FluidSystem::H2Idx,
            TCIdx       = FluidSystem::TCIdx,
            HydrogenIdx  = FluidSystem::H2Idx,
            CH4Idx       = FluidSystem::CH4Idx,

            //Indices of the bio/coal volume fractions
            CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
            AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
            AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
            HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
            MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
            CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,
            numSolidComponents= SolidSystem::numComponents - SolidSystem::numInertComponents,

            CoalBacPhaseIdx   = SolidSystem::CoalBacPhaseIdx,
            AmCoalBacPhaseIdx = SolidSystem::AmCoalBacPhaseIdx,
            AcetoArchPhaseIdx = SolidSystem::AcetoArchPhaseIdx,
            HydroArchPhaseIdx = SolidSystem::HydroArchPhaseIdx,
            MethyArchPhaseIdx = SolidSystem::MethyArchPhaseIdx,
            CCoalPhaseIdx     = SolidSystem::CCoalPhaseIdx,

            //Index of the primary component of G and L phase
            conti0EqIdx = Indices::conti0EqIdx,

            // Phase State
            wPhaseOnly = Indices::firstPhaseOnly,
            nPhaseOnly = Indices::secondPhaseOnly,
            bothPhases = Indices::bothPhases,

            // Grid and world dimension
            dim      = GridView::dimension,
            dimWorld = GridView::dimensionworld,
        };

        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
        using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
        using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
        using Element = typename GridView::template Codim<0>::Entity;
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
        using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;
        using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
        using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
        using CoordScalar = typename GridView::ctype;
        using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;


        //! property that defines whether mole or mass fractions are used
        static constexpr bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
        static constexpr auto numPhases = FluidSystem::numPhases;

    public:
        MECBMColumnReaction(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
                : ParentType(fvGridGeometry)
        {
            name_                   = getParam<std::string>("Problem.Name");
            temperature_            = getParam<Scalar>("Problem.Temperature");
            reservoirPressure_      = getParam<Scalar>("Problem.ReservoirPressure");

            initxwCH4_              = getParam<Scalar>("Initial.initxwCH4"); // Initial wetting mole fraction of CH4
            initxwH2_               = getParam<Scalar>("Initial.initxwH2"); // Initial wetting mole fraction of Hydrogen
            initMassConcAmendment   = getParam<Scalar>("Initial.initMassConcAmendment"); // Initial mass concentration of Amendment
            initxwAcetate_          = getParam<Scalar>("Initial.initxwAcetate"); // Initial wetting mole fraction of Acetate
            initxwRMethyl_          = getParam<Scalar>("Initial.initxwRMethyl"); // Initial wetting mole fraction of Methyl
            initxwTC_               = getParam<Scalar>("Initial.initxwTC"); // Initial wetting mole fraction of total inorganic carbon
            initPhiCCoal_           = getParam<Scalar>("Initial.initPhiCCoal"); // Initial volume fraction of bioavailable coal
            initPhiCoalBac_         = getParam<Scalar>("Initial.initPhiCoalBac"); // Initial volume fraction of coal consuming bacteria
            initPhiAmCoalBac_       = getParam<Scalar>("Initial.initPhiAmCoalBac"); // Initial volume fraction of amendment and coal consuming bacteria
            initPhiAcetoArch_       = getParam<Scalar>("Initial.initPhiAcetoArch"); // Initial volume fraction of acetoclastics archaea
            initPhiHydroArch_       = getParam<Scalar>("Initial.initPhiMethyArch"); // Initial volume fraction of methylotrophic archaea
            initPhiMethyArch_       = getParam<Scalar>("Initial.initPhiHydroArch"); // Initial volume fraction of hydrogenotrophic archaea

            numInjections_          = getParam<int>("Injection.numInjections");
            injectionParameters_    = getParam<std::string>("Injection.InjectionParamFile");
            injQ_                   = getParam<Scalar>("Injection.injQ");

            nTemperature_           = getParam<int>("FluidSystem.NTemperature");
            nPressure_              = getParam<int>("FluidSystem.NPressure");
            pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
            pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
            temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
            temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");
            bothphases              = getParam<bool>("Initial.bothphases");


            //growth and decay rate coefficients
            muCoalBacCoal_  = getParam<Scalar>("BioCoefficients.muCoalBacC");   // Maximum specific growth rate for Primary Bacteria on Coal
            muAmCoalBacCoal_= getParam<Scalar>("BioCoefficients.muAmCoalBacC"); // Maximum specific growth rate for Secondary Bacteria on Coal
            muAmCoalBacAm_  = getParam<Scalar>("BioCoefficients.muAmCoalBacAm");// Maximum specific growth rate for Secondary Bacteria on Amendment
            muAcetoArch_    = getParam<Scalar>("BioCoefficients.muAcetoArch");  // Maximum specific growth rate for Acetoclastic Archaea
            muHydroArch_    = getParam<Scalar>("BioCoefficients.muHydroArch");  // Maximum specific growth rate for Hydrogenotrophic Archaea
            muMethyArch_    = getParam<Scalar>("BioCoefficients.muMethyArch");  // Maximum specific growth rate for Methylotrophic Archaea
            K_              = muAmCoalBacAm_/100; //getParam<Scalar>("BioCoefficients.K");            // Decay Rate for Microbes

            //Monod half saturation constants
            Kc_     = getParam<Scalar>("BioCoefficients.Kc");           // Monod Half saturation Constant for Coal
            Kh_     = getParam<Scalar>("BioCoefficients.KH2");          // Monod Half saturation Constant for Hydrogen
            KAm_    = getParam<Scalar>("BioCoefficients.KAm");          // Monod Half saturation Constant for Amendment
            KAc_    = getParam<Scalar>("BioCoefficients.KAc");          // Monod Half saturation Constant for Acetate
            KCH3_   = getParam<Scalar>("BioCoefficients.KCH3");         // Monod Half saturation Constant for Methyl

            //Yields
            Yccbc_  = getParam<Scalar>("BioCoefficients.YCoalBacC");    // Yield of coal consuming Bacteria Biomass on Coal
            Yacbc_  = getParam<Scalar>("BioCoefficients.YAmCoalBacC");  // Yield of coal & amendment consuming Bacteria on Coal
            Yacbam_ = getParam<Scalar>("BioCoefficients.YAmCoalBacAm"); // Yield of coal & amendment consuming Bacteria on Amendment
            amFac_  = getParam<Scalar>("BioCoefficients.amFac", 1); // amendmentFactor
            YhaH2_  = getParam<Scalar>("BioCoefficients.YHydroArchH2"); // Yield of Hydrogenotrophic Archaea Biomass on Hydrogen
            YaaAc_  = getParam<Scalar>("BioCoefficients.YAcetoArchAc"); // Yield of Acetoclastic Archaea Biomass on Acetate
            YmaCH3_ = getParam<Scalar>("BioCoefficients.YMethyArchCH3");// Yield of Methylotrophic Archaea Biomass on Methyl
            YH2cc_  = getParam<Scalar>("BioCoefficients.YH2cc");        // Yield of Hydrogen from Coal
            YAccc_  = getParam<Scalar>("BioCoefficients.YAccc");        // Yield of Acetate from Coal
            YH2Am_  = getParam<Scalar>("BioCoefficients.YH2Am")*amFac_;        // Yield of Hydrogen from Amendment
            YAcAm_  = getParam<Scalar>("BioCoefficients.YAcAm")*amFac_;        // Yield of Acetate from Amendment
            YCH3Am_ = getParam<Scalar>("BioCoefficients.YCH3Am")*amFac_;       // Yield of Methyl from Amendment
            YCH4Ac_ = getParam<Scalar>("BioCoefficients.YCH4Ac");       // Yield of CH4 from Acetate
            YCH4H2_ = getParam<Scalar>("BioCoefficients.YCH4H2");       // Yield of CH4 from Hydrogen
            YCH4CH3_= getParam<Scalar>("BioCoefficients.YCH4CH3");      // Yield of CH4 from Methyl
            YCO2Ac_ = getParam<Scalar>("BioCoefficients.YCO2Ac");       // Yield of CO2 from Acetate
            YCO2CH3_ = getParam<Scalar>("BioCoefficients.YCO2CH3");       // Yield of CO2 from Methyl


            unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
            Kxx_.resize(fvGridGeometry->gridView().size(codim));
            Kyy_.resize(fvGridGeometry->gridView().size(codim));
            Kzz_.resize(fvGridGeometry->gridView().size(codim));
            cellVolume_.resize(fvGridGeometry->gridView().size(codim));
            molOutFluxCH4w_.resize(fvGridGeometry->gridView().size(codim));
            molOutFluxCH4n_.resize(fvGridGeometry->gridView().size(codim));
            NumEqVector zero(0.0);
            sourceMatrix_.resize(fvGridGeometry->numScv(), zero);


            FluidSystem::init(/*Tmin=*/temperatureLow_,
                    /*Tmax=*/temperatureHigh_,
                    /*nT=*/nTemperature_,
                    /*pmin=*/pressureLow_,
                    /*pmax=*/pressureHigh_,
                    /*np=*/nPressure_);


        }

        void setTime( Scalar time )
        {
            time_ = time;
        }

        void setTimeStepSize( Scalar timeStepSize )
        {
            timeStepSize_ = timeStepSize;
        }

        void setEpisodeIdx( Scalar epiIdx )
        {
            episodeIdx_ = epiIdx;
        }



        /*!
         * \name Problem parameters
         */


        /*!
         * \brief The problem name.
         *
         * This is used as a prefix for files generated by the simulation.
         */
        const std::string& name() const
        { return name_; }

        /*!
         * \brief Returns the temperature within the domain.
         *
         * This problem assumes a temperature of 10 degrees Celsius.
         */
        Scalar temperature() const
        { return temperature_; }

        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment.
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            // default to Neumann
            bcTypes.setAllNeumann();

//        const Scalar zMax = this->fvGridGeometry().bBoxMax()[dim-1];
//        if (globalPos[1] > zMax - eps_ )
//            bcTypes.setDirichlet(pressureIdx);

            return bcTypes;
        }

        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);

            return priVars;
        }

        /*!
         * \brief Evaluate the boundary conditions for a neumann
         *        boundary segment.
         *
         * This is the method for the case where the Neumann condition is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The neumann values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scvf The sub control volume face
         *
         * For this method, the \a values parameter stores the flux
         * in normal direction of each phase. Negative values mean influx.
         * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
         */
        NumEqVector neumann(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf) const
        {
            NumEqVector flux(0.0);

            return flux;
        }

        /*!
         * \brief Evaluate the boundary conditions for a dirichlet
         *        boundary segment.
         */

        /*!
         * \brief Evaluate the initial value for a control volume.
         *
         * \param globalPos The global position
         *
         * For this method, the \a values parameter stores primary
         * variables.
         */
        PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
        {
            PrimaryVariables priVars(0.0);
//            if (bothphases){
                priVars.setState(bothPhases);
//            }else{
//                priVars.setState(wPhaseOnly);
//            }

            priVars[pressureIdx]  = reservoirPressure_;
            priVars[switchIdx]    = initxwCH4_;                 // Sw primary variable

            // components
            priVars[H2Idx]        = initxwH2_;                  // Initial wetting mole fraction of Hydrogen
            priVars[AmendmentIdx] = 0;                          // set via injection file
            priVars[AcetateIdx]   = initxwAcetate_;             // Initial wetting mole fraction of Acetate
            priVars[RMethylIdx]   = initxwRMethyl_;             // Initial wetting mole fraction of Methyl
            priVars[TCIdx]        = initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon
            // biofilm/coal volume fractions
            priVars[CCoalIdx]     = initPhiCCoal_;              // Initial volume fraction of bioavailable coal
            priVars[CoalBacIdx]   = initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
            priVars[AmCoalBacIdx] = initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
            priVars[AcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
            priVars[MethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
            priVars[HydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction of methylotrophic archaea

            return priVars;
        }

        /*!
         * \name Volume terms
         */
        // \{

        /*!
         * \brief Evaluate the source term for all phases within a given
         *        sub-control-volume.
         *
         * This is the method for the case where the source term is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The source and sink values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scv The subcontrolvolume
         *
         * For this method, the \a values parameter stores the conserved quantity rate
         * generated or annihilate per volume unit. Positive values mean
         * that the conserved quantity is created, negative ones mean that it vanishes.
         * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
         */
        NumEqVector source(const Element &element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const SubControlVolume &scv) const
        {
            NumEqVector source(0.0);
            source = sourceMatrix_[scv.dofIndex()];
//            Chemistry chemistry;
//            const auto& volVars = elemVolVars[scv];
//            chemistry.reactionSource(source, volVars, timeStepSize_);
            return source;
        }

        /*!
         * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
         */

        const std::vector<Scalar>& getKxx()
        {
            return Kxx_;
        }

        const std::vector<Scalar>& getKyy()
        {
            return Kyy_;
        }

        const std::vector<Scalar>& getKzz()
        {
            return Kzz_;
        }

        const std::vector<Scalar>& cellVolume()
        {
            return cellVolume_;
        }

        const auto& molOutFluxCH4w()
        {
            return molOutFluxCH4w_;
        }

        const auto& molOutFluxCH4n()
        {
            return molOutFluxCH4n_;
        }
        /*!
         * \brief Return how much the domain is extruded at a given sub-control volume.
         *
         * This means the factor by which a lower-dimensional (1D or 2D)
         * entity needs to be expanded to get a full dimensional cell. The
         * default is 1.0 which means that 1D problems are actually
         * thought as pipes with a cross section of 1 m^2 and 2D problems
         * are assumed to extend 1 m to the back.
         */
        template<class ElementSolution>
        Scalar extrusionFactor(const Element &element,
                               const SubControlVolume &scv,
                               const ElementSolution& elemSol) const
        {
            const auto radius = 0.0525/2;
            return M_PI*radius*radius;
        }

        void updateVtkOutput(const SolutionVector& curSol)
        {
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {
                const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    const auto dofIdxGlobal = scv.dofIndex();
                    Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                    Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
                    Kzz_[dofIdxGlobal] = volVars.permeability()[2][2];
                    cellVolume_[dofIdxGlobal] = element.geometry().volume();
                    molOutFluxCH4w_[dofIdxGlobal] = molOutFlux_[phase0Idx][comp1Idx];
                    molOutFluxCH4n_[dofIdxGlobal] = molOutFlux_[phase1Idx][comp1Idx];
                }
            }

        }

        void setSourceTerm(const std::vector<NumEqVector>& source)
        {
            sourceMatrix_ = source;
        }

        void initialSourceTerm()
        {
            for(auto& sourceItem : sourceMatrix_)
            {
                NumEqVector q(0.0);
                sourceItem = q;
            }
        }

        template<class GridVariables, class SolutionVector>
        void calReaction(GridVariables& gridVariables, SolutionVector& sol){
            for (const auto& element : Dune::elements(this->gridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVariables->curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);


                for (const auto& scv : scvs(fvGeometry))
                {
                    const auto scvIdx = scv.dofIndex();
                    const auto& volVars = elemVolVars[scv];


//                std::cout<<'\n' <<"current scv" << scvIdx <<std::endl;

                    //////////////////////
                    //// mass before reaction
                    ///////////////////////

                    //mass before reaction in unit [mol/total m3]
                    NumEqVector mOld(0.0);
                    NumEqVector mOldWet(0.0);
                    NumEqVector mOldGas(0.0);

                    for (int compIdx = 0; compIdx < numComponents+numSolidComponents; ++compIdx) {
                        ///aqueous components
                        if(compIdx<numComponents){

                            mOldWet[compIdx] = volVars.porosity()
                                               * volVars.saturation(phase0Idx)
                                               * volVars.molarDensity(phase0Idx)
                                               * volVars.moleFraction(phase0Idx, compIdx);

                            mOldGas[compIdx] += volVars.porosity()
                                                * volVars.saturation(phase1Idx)
                                                * volVars.molarDensity(phase1Idx)
                                                * volVars.moleFraction(phase1Idx, compIdx);
                            mOld[compIdx] = mOldGas[compIdx] + mOldWet[compIdx];
                        }
                            ///biofilm
                        else{
                            auto solidPhaseIdx = compIdx - numComponents;
                            mOld[compIdx] = volVars.solidVolumeFraction(solidPhaseIdx)
                                            * volVars.solidComponentMolarDensity(solidPhaseIdx);
                        }

                        // components in [mol/ total m3 ]
                    }//end of initial mass

                    //////////////////////////
                    ///explicit reaction
                    //////////////////////////

                    NumEqVector q(0.0);

                    Scalar cAcetate = volVars.moleFraction(phase0Idx, AcetateIdx)* volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AcetateIdx);    //[kg_suspended_Biomass/m³_waterphase]
                    if(cAcetate < 0)
                        cAcetate = 0;
                    Scalar cCarAm   = volVars.moleFraction(phase0Idx, AmendmentIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AmendmentIdx); //[kg_suspended_Biomass/m³_waterphase]
                    if(cCarAm < 0)
                        cCarAm = 0;
                    Scalar cRMethyl = volVars.moleFraction(phase0Idx, RMethylIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(RMethylIdx);    //[kg_suspended_Biomass/m³_waterphase]
                    if(cRMethyl < 0)
                        cRMethyl = 0;
                    Scalar cHydrogen = volVars.moleFraction(phase0Idx, HydrogenIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(HydrogenIdx); //[kg_suspended_Biomass/m³_waterphase]
                    if(cHydrogen < 0)
                        cHydrogen = 0;

                    //compute biomass growth coefficients and rate
                    Scalar massCoalBac_   = volVars.solidVolumeFraction(CoalBacPhaseIdx)*volVars.solidComponentDensity(CoalBacPhaseIdx);     //[kg_pb_Biomass/m³_total] volumetric fraction of primary bacteria attached reversibly
                    Scalar massAmCoalBac_ = volVars.solidVolumeFraction(AmCoalBacPhaseIdx)*volVars.solidComponentDensity(AmCoalBacPhaseIdx); //[kg_sb_Biomass/m³_total] volumetric fraction of secondary bacteria attached reversibly
                    Scalar massAcetoArch_ = volVars.solidVolumeFraction(AcetoArchPhaseIdx)*volVars.solidComponentDensity(AcetoArchPhaseIdx); //[kg_aa_Biomass/m³_total] volumetric fraction of acetoclastic archaea attached reversibly
                    Scalar massHydroArch_ = volVars.solidVolumeFraction(HydroArchPhaseIdx)*volVars.solidComponentDensity(HydroArchPhaseIdx); //[kg_ha_Biomass/m³_total] volumetric fraction of hydrogenotrophic archaea attached reversibly
                    Scalar massMethyArch_ = volVars.solidVolumeFraction(MethyArchPhaseIdx)*volVars.solidComponentDensity(MethyArchPhaseIdx); //[kg_ma_Biomass/m³_total] volumetric fraction of methylotrophic archaea attached reversibly
                    Scalar massCCoal_     = volVars.solidVolumeFraction(CCoalPhaseIdx)*volVars.solidComponentDensity(CCoalPhaseIdx);         //[kg_cc_Biomass/m³_total] volumetric fraction convertible coal attached reversibly

                    if(massCCoal_ < 0)
                        massCCoal_ = 0;

                    Scalar rgCoalBac      = muCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massCoalBac_;      //growth rate primary bacteria (MSc Irfan 3.2)
                    Scalar rdCoalBac      = K_ * massCoalBac_;                                                  //decay rate primary bacteria (MSc Irfan 3.3)
                    Scalar rgAmCoalBacCoal= muAmCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massAmCoalBac_;  //growth rate secondary bacteria on coal (MSc Irfan 3.5)
                    Scalar rgAmCoalBacAm  = muAmCoalBacAm_ * cCarAm / (KAm_ + cCarAm)*massAmCoalBac_;           //growth rate secondary bacteria on amendment (MSc Irfan 3.6)
                    Scalar rgAmCoalBac    = rgAmCoalBacCoal + rgAmCoalBacAm;                                    //growth rate secondary bacteria total (MSc Irfan 3.7)
                    Scalar rdAmCoalBac    = K_ * massAmCoalBac_;                                                //decay rate secondary bacteria total (MSc Irfan 3.8)
                    Scalar rgAcetoArch    = muAcetoArch_ * cAcetate / (KAc_ + cAcetate)*massAcetoArch_;         //growth rate acetoclastic archaea (MSc Irfan 3.13)
                    Scalar rdAcetoArch    = K_ * massAcetoArch_;                                                //decay rate acetoclastic archaea (MSc Irfan 3.14)

                    /// considering the hydrogen
                    Scalar rgHydroArch    = muHydroArch_ * cHydrogen / (Kh_ + cHydrogen)*massHydroArch_;        //growth rate hydrogenotrophic archaea (MSc Irfan 3.10)
                    Scalar rdHydroArch    = K_ * massHydroArch_;                                                //decay rate hydrogenotrophic archaea (MSc Irfan 3.11)

                    Scalar qHydrogen = ((rgAmCoalBacCoal * YH2cc_ / Yacbc_)
                                        + (rgAmCoalBacAm * YH2Am_ / Yacbam_)
                                        + (rgCoalBac * YH2cc_ / Yccbc_)
                                        - rgHydroArch / YhaH2_)
                                       /86400  / FluidSystem::molarMass(HydrogenIdx)
                                       * timeStepSize_;

                    //if too much hydrogen is consumed
                    if (mOldWet[HydrogenIdx] + qHydrogen < 0){
                        rgHydroArch =(mOldWet[HydrogenIdx] *0.98
                                      / timeStepSize_
                                      * 86400
                                      * FluidSystem::molarMass(HydrogenIdx))
                                     +
                                     ((rgAmCoalBacCoal * YH2cc_ / Yacbc_)
                                      + (rgAmCoalBacAm * YH2Am_ / Yacbam_)
                                      + (rgCoalBac * YH2cc_ / Yccbc_));
                        rgHydroArch *= YhaH2_;
                        std::cout << "kinetic modified" <<std::endl;
                    }



                    Scalar rgMethyArch    = muMethyArch_ * cRMethyl / (KCH3_ + cRMethyl)*massMethyArch_;        //growth rate methylotrophic archaea (MSc Irfan 3.16)
                    Scalar rdMethyArch    = K_ * massMethyArch_;                                                //decay rate methylotrophic archaea (MSc Irfan 3.17)

                    //Phase reactions in [mol/(m3*s)]
                    //introducing *10 factor to match Irfan's Matlab porosity formulation
                    q[CoalBacIdx]     = (rgCoalBac - rdCoalBac) / 86400 / SolidSystem::molarMass(CoalBacPhaseIdx);
                    q[AmCoalBacIdx]   = (rgAmCoalBac - rdAmCoalBac) / 86400 / SolidSystem::molarMass(AmCoalBacPhaseIdx);
                    q[AcetoArchIdx]   = (rgAcetoArch - rdAcetoArch) / 86400 / SolidSystem::molarMass(AcetoArchPhaseIdx);
                    q[HydroArchIdx]   = (rgHydroArch - rdHydroArch) / 86400 / SolidSystem::molarMass(HydroArchPhaseIdx);
                    q[MethyArchIdx]   = (rgMethyArch - rdMethyArch) / 86400 / SolidSystem::molarMass(MethyArchPhaseIdx);

                    //Component reactions in mol/(m3*s)
                    q[CH4Idx]     = (rgHydroArch * YCH4H2_ / YhaH2_
                                     + rgAcetoArch * YCH4Ac_ / YaaAc_
                                     + rgMethyArch * YCH4CH3_ / YmaCH3_)
                                    /86400 / FluidSystem::molarMass(CH4Idx); //+ sorp; //TODO: add when above is solved/clear
                    q[AcetateIdx] = ((rgAmCoalBacCoal * YAccc_ / Yacbc_)
                                     + (rgAmCoalBacAm * YAcAm_ / Yacbam_)
                                     + (rgCoalBac * YAccc_ / Yccbc_)
                                     - rgAcetoArch / YaaAc_)
                                    /86400  / FluidSystem::molarMass(AcetateIdx);
                    q[AmendmentIdx]= (-rgAmCoalBacAm / Yacbam_)
                                     /86400 / FluidSystem::molarMass(AmendmentIdx);

                    q[RMethylIdx] = ((rgAmCoalBacAm * YCH3Am_ / Yacbam_)
                                     - rgMethyArch / YmaCH3_)
                                    /86400  / FluidSystem::molarMass(RMethylIdx);

                    q[HydrogenIdx] = ((rgAmCoalBacCoal * YH2cc_ / Yacbc_)
                                      + (rgAmCoalBacAm * YH2Am_ / Yacbam_)
                                      + (rgCoalBac * YH2cc_ / Yccbc_)
                                      - rgHydroArch / YhaH2_)
                                     /86400  / FluidSystem::molarMass(HydrogenIdx);
                    q[TCIdx]       = (rgAcetoArch * YCO2Ac_ / YaaAc_
                                      + (rgMethyArch * YCO2CH3_ / YmaCH3_)
                                      -(rgHydroArch * YCH4H2_ / YhaH2_) )        // consume 1 CO2 per 1 CH4
                                     /86400  / FluidSystem::molarMass(TCIdx);

                    q[CCoalIdx]    = -(rgCoalBac / Yccbc_ + rgAmCoalBacCoal / Yacbc_)
                                     / 86400 / SolidSystem::molarMass(CCoalPhaseIdx);


                    sourceMatrix_[scvIdx] = q;
                }//end of scv
            }//end of element
        }

        using ScalarVec = std::vector<Scalar>;

    private:

        /*!
         * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
         *
         * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
         */
        static Scalar massToMoleFrac_(Scalar XwNaCl)
        {
            const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(comp0Idx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
            const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

            const Scalar X_NaCl = XwNaCl;
            /* XwNaCl: conversion from mass fraction to mol fraction */
            auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
            return xwNaCl;
        }




        int nTemperature_;
        int nPressure_;
        std::string name_;

        Scalar pressureLow_, pressureHigh_;
        Scalar temperatureLow_, temperatureHigh_;
        Scalar reservoirPressure_;
        Scalar temperature_;

        Scalar initxwCH4_;                 // Initial wetting mole fraction of methane
        Scalar initxwH2_;                  // Initial wetting mole fraction of Hydrogen
        Scalar initMassConcAmendment;      // Initial wetting mole fraction of Amendment
        Scalar initxwAcetate_;             // Initial wetting mole fraction of Acetate
        Scalar initxwRMethyl_;             // Initial wetting mole fraction of Methyl
        Scalar initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon

        // biofilm/coal volume fractions
        Scalar initPhiCCoal_;              // Initial volume fraction of bioavailable coal
        Scalar initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
        Scalar initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
        Scalar initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
        Scalar initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
        Scalar initPhiHydroArch_;          // Initial volume fraction of methylotrophic archaea

        std::vector<int> injType_;
        int numInjections_;                // number of Injections
        std::string injectionParameters_;  // injectionParamteres from file
        Scalar injQ_;                      // injected volume flux in m3/s

        Scalar time_ = 0.0;
        Scalar timeStepSize_ = 0.0;
        Scalar episodeIdx_ = 0;

        static constexpr Scalar eps_ = 1e-6;
        std::vector<double> Kxx_;
        std::vector<double> Kyy_;
        std::vector<double> Kzz_;
        std::vector<double> cellVolume_;
        std::vector<double> molOutFluxCH4w_;
        std::vector<double> molOutFluxCH4n_;
        mutable std::vector<NumEqVector> sourceMatrix_;
        mutable bool bothphases = true;
        Scalar CH4_rate;

        mutable std::array<NumEqVector,numPhases> molOutFlux_;

        template <class T>
        void print(T& txt){
            std::cout<< txt <<std::endl;
        }

        Scalar K_;

        Scalar muCoalBacCoal_;
        Scalar muAmCoalBacCoal_;
        Scalar muAmCoalBacAm_ ;
        Scalar muAcetoArch_ ;
        Scalar muHydroArch_;
        Scalar muMethyArch_ ;

        Scalar Kc_ ;
        Scalar Kh_ ;
        Scalar KAm_ ;
        Scalar KAc_ ;
        Scalar KCH3_;

        Scalar Yccbc_ ;
        Scalar Yacbc_ ;
        Scalar Yacbam_ ;
        Scalar amFac_;
        Scalar YhaH2_ ;
        Scalar YaaAc_ ;
        Scalar YmaCH3_ ;
        Scalar YH2cc_ ;
        Scalar YAccc_;
        Scalar YH2Am_ ;
        Scalar YAcAm_ ;
        Scalar YCH3Am_;
        Scalar YCH4Ac_ ;
        Scalar YCH4H2_ ;
        Scalar YCH4CH3_;
        Scalar YCO2Ac_ ;
        Scalar YCO2CH3_ ;
    };

} //end namespace Dumux

#endif
