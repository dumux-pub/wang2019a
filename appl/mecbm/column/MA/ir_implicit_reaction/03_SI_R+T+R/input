# Parameter file for test case 2pmecbmbatch.
# Everything behind a '#' is a comment.
# Type "./test_mecbmbatch_box --help" for more information.
[Limit]
CCoal = 0#2e-4
H2 = 3e-8
time = 1000
writeTime = 10800


[TimeLoop]
TEnd = 1.728e7                     # duration of the simulation [s]
DtInitial = 0.1                   # initial time step size [s]
MaxTimeStepSize = 500          # maximum time step size
ReactionMaxTimeStepSize = 54000
ReactionDtEpisode = 1
ReactionDtInitial = 0.1
Episode = 1
[SI]
FirstCorrection = true
IfFixedRate = true
SIError = 1e-2
Factor = 1e-2
MaxIteration = 20

[Grid]
UpperRight = 0 0 0.13858  # actual length: 0.13858 + HEAD Space
Cells = 80
ClosureType = None

[FluidSystem]
NTemperature =  200                # [-] number of tabularization entries
NPressure = 200                    # [-] number of tabularization entries
PressureLow = 1e4                  # [Pa] low end for tabularization of fluid properties
PressureHigh = 3e7                 # [Pa] high end for tabularization of fluid properties
TemperatureLow = 273.15            # [K] low end for tabularization of fluid properties
TemperatureHigh = 310.15           # [K] high end for tabularization of fluid properties

[Brine]
Salinity = 1e-1

[Problem]
Name                =SSNI_R+T+R_implicit_03 # [-]  name for output files
#bothphase with gas injection
#only_coalbac: only growth of coalbac, no injection of amendment, no initial acetate, no growth of sec. archaea.
ReservoirPressure   = 1.01325E5    # [Pa] Initial reservoir pressure
Temperature         = 298.15       # [K]  reservoir temperature
EnableGravity = true

[Initial]
initxwCH4           = 1.2e-1         # [-] initial gas saturation : was initSatGas
initxwH2            = 0            # [-] initial mole fraction Hydrogen (minimum value estimate)
initMassConcAmendment = 15000   # [kg/m3] set 1 for initial mass of carbon amendment: 1 ml with 15 g/L Amendment
initxwAcetate       = 1e-6		   # [-] initial mole fraction acetate (minimum value estimate) 1.3e-6 for testing directly
initxwRMethyl       = 0            # [-] initial mole fraction methyl (minimum value estimate)
initxwTC            = 0            # [-] initial mole fraction of total inorganic carbon
initPhiCCoal        = 8e-4         # [m3 biomass/m3 total] initial volume fraction of convertible coal 100kg/1m3, with rho= 1250 kg/m3, 1% bioconvertable)
initPhiCoalBac      =                   1.127312601852942E-005 #         # [m3 biomass/m3 total]
initPhiAmCoalBac    =                   1.127312601852942E-005 #         # [m3 biomass/m3 total]
initPhiAcetoArch    =                   9.972493843430048E-006 #         # [m3 biomass/m3 total]
initPhiHydroArch    =                   9.972493843430048E-006 #         # [m3 biomass/m3 total]
initPhiMethyArch    =                   9.972493843430048E-006 #         # [m3 biomass/m3 total]
bothphases          = true
[Injection]
numInjections = 4 #
InjectionParamFile = injections/2ColumnAmInj.dat #
injQ = 8.33E-11 #[m3/s] Experiment: 0.005 ml/min
CH4Rate = 1e-6

[BioCoefficients]
rhoBiofilm          = 10           # [kg/m3] Dry density of biofilm, value fitted by A. Ebigbo 2012 WRR
K                   = 2.0179541e-3 # [-] Decay rate for microbes
muCoalBacC          =                      1.933168452018782E-001 # [d-1]  0.3...0.8 Maximum specific growth rate for coal bacteria on coal]
muAmCoalBacC        =                      1.933168452018782E-001 # [d-1]  0.5...1.0 Maximum specific growth rate for amendment & coal bacteria on coal
muAmCoalBacAm       =                      3.753139436536319E-001 # [d-1]  0.5...1.0 +larger than muAmCoalBacC Maximum specific growth rate for amendment & coal bacteria on amendment
muAcetoArch         = 8            # [d-1] * Maximum specific growth rate for acetoclastic archaea
muHydroArch         = 1.66         # [d-1] * Maximum specific growth rate for hydrogenotrophic archaea
muMethyArch         = 2.9          # [d-1] * Maximum specific growth rate for methylotrophic archaea
Kc                  =                      1.002464927688167E+000 # [g/L] * Monod half saturation constant for coal
KH2                 =                      4.444444444444440E-005 # [g/L] * Monod half saturation constant for hydrogen
KAm                 =                      1.599387440487446E-001 # [g/L] 0.6...15 Monod half saturation constant for amendment (estimate ranging from 0.6 ... 15 g/L)
KAc                 =                      1.999956622258312E-001 # [g/L] * Monod half saturation constant for acetate
KCH3                = 2e-3         # [g/L] * Monod half saturation constant for methyl
YCoalBacC           =                      2.894402565092672E-002 # [-] Yield of primary bacteria biomass on coal  //TODO
YAmCoalBacC         =                      2.894402565092672E-002 # [-] * Yield of secondary bacteria on coal
YAmCoalBacAm        =                      8.030129036538075E-002 # [-]  Yield of secondary bacteria on amendment
amFac               = 1
YHydroArchH2        = 1.217        # [-] * Yield of hydrogenotrophic archaea biomass on hydrogen
YAcetoArchAc        = 0.035        # [-] * Yield of acetoclastic Archaea biomass on acetate
YMethyArchCH3       = 0.02         # [-] * Yield of methylotrophic archaea biomass on methyl
YH2cc               =                      9.866928245839084E-003 # [-]  0.1...0.001 Yield of hydrogen from coal
YAccc               =                      1.595959392795908E-001 # [-] 0.1...0.001 Yield of acetate from coal
YH2Am               =                      6.276901115386366E-002
YAcAm               =                      7.848900851529305E-001
YCH3Am              = 0.0          # [-] * Yield of methyl from amendment
YCH4Ac              =                      2.401767541095876E-001 # [-] * Yield of methane from acetate
YCH4H2              =                      1.569076486197504E+000 # [-] * Yield of methane from hydrogen
YCH4CH3             = 1.0          # [-] * Yield of methane from methyl
YCO2Ac              = 0.2058       # [-] * Yield of methane from acetate
YCO2CH3             = 7.26E-3      # [-] * Yield of methane from methyl

[SorptionCoefficients]
useAdsorption       = 1            # Using Ad-/Desorption in general (1: yes, 0: no)
useEL               = 1            # Using Extended Langmuir Ad-/Desorption formulation (1: yes, 0: no=simple Langmuir), useAdsorption must be 1!
VCH4                = 0.8          # [mol/l] or m3/kg? values from "Malgo's table [67]"
bCH4                = 2.3e-7       # [1/Pa] or maybe use 1/MPa !conversion 1/b=Pl with Pl =167.5 psia
VCO2                = 1.6          # [mol/l] or m3/kg?
bCO2                = 4.64e-7      # [1/Pa] !conversion 1/b=Pl with Pl =167.5 psia

[SpatialParams]
SolubilityLimit     = 0.295        # [-]  solubility limit of salt in brine
Porosity            = 0.48         # [-]  initial porosity coal 100kg/m3, with rho= 1250 kg/m3
Permeability        = 2.23e-14
IrreducibleLiqSat   = 1e-3         # [-]  irreducible liquid saturation
IrreducibleGasSat   = 1e-5         # [-]  irreducible gas saturation
Pentry1             = 500          # [Pa]
BCLambda1           = 2            # [-]

[Vtk]
AddVelocity         = 1            # Add extra information

[LinearSolver]
ResidualReduction = 1e-5

[Newton]
EnablePartialReassembly = false
EnableResidualCriterion = true
ResidualReduction = 1e-8

