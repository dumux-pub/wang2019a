// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief A Fluidsystem with Brine/water as wetting phase and mainly CH4 or CO2 as gas phase.
 */
#ifndef DUMUX_BRINE_CH4_CO2_SYSTEM_HH
#define DUMUX_BRINE_CH4_CO2_SYSTEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/material/idealgas.hh>
#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/fluidsystems/brine.hh>
#include <dumux/material/fluidstates/adapter.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/brine.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/ccoal.hh>
#include <dumux/material/components/acetate.hh>
#include <dumux/material/components/caram.hh>
#include <dumux/material/components/rmethyl.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/biofilm.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/material/binarycoefficients/h2o_ch4.hh>
#include <dumux/material/binarycoefficients/h2o_h2.hh>
#include <dumux/material/binarycoefficients/brine_co2.hh>

#include <dumux/io/name.hh>

namespace Dumux {

// include the default tables for CO2
#ifndef DOXYGEN // hide tables from doxygen
#include <dumux/material/components/co2tables.inc>
#endif

namespace FluidSystems {
    namespace Detail {

        /*!
         * \brief Class that exports some indices that should
         *        be provided by the BrineCH4CO2 fluid system.
         *        The indices are chosen dependent on the policy,
         *        i.e. if a simplified pseudo component Brine is
         *        used or salt is considered an individual component.
         */
        template<bool useConstantSalinity>
        struct BrineCH4CO2Indices;

        /*!
         * \brief Specialization for the case of brine being
         *        a pseudo component with a constant salinity.
         * \note This specialization exports brine as component
         */
        template<>
        struct BrineCH4CO2Indices<true>
        {
            static constexpr int BrineIdx = 0;
        };

        /*!
         * \brief Specialization for the case of brine being
         *        a fluid system with NaCl as individual component.
         * \note This specialization exports both water and NaCl as components
         */
        template<>
        struct BrineCH4CO2Indices<false>
        {
            static constexpr int H2OIdx = 0;
            static constexpr int NaClIdx = 7;
            static constexpr int comp2Idx = 7;
        };
    } // end namespace Detail

/*!
 * \ingroup Fluidsystems
 * \brief Default policy for the Brine-CH4-CO2 fluid system
 */
template<bool salinityIsConstant, bool fastButSimplifiedRelations = false>
struct BrineCH4CO2DefaultPolicy
{
    static constexpr bool useConstantSalinity() { return salinityIsConstant; }
    static constexpr bool useCO2GasDensityAsGasMixtureDensity() { return fastButSimplifiedRelations; }
};

/*!
 * \ingroup Fluidsystems
 * \brief A compositional fluid with brine (H2O & NaCl) and CH4, CO2,... as
 *        components in both the liquid and the gas phase.
 *
 * \note Depending on the chosen policy, the salinity is assumed to be constant
 *       (in which case Brine is used as a pseudo component) or salt (here NaCl)
 *       is considered as an individual component.
 * \note This implemetation always assumes NaCl stays in the liquid phase.
 */
template<class Scalar,
         class CO2Table,
         class H2OType = Components::TabulatedComponent<Components::H2O<Scalar> >,
         class Policy = BrineCH4CO2DefaultPolicy</*constantSalinity?*/true> >
class BrineCH4CO2
: public Base<Scalar, BrineCH4CO2<Scalar, CO2Table, H2OType, Policy> >
, public Detail::BrineCH4CO2Indices<Policy::useConstantSalinity()>
{
    using ThisType = BrineCH4CO2<Scalar, CO2Table, H2OType, Policy>;
    using Base = Dumux::FluidSystems::Base<Scalar, ThisType>;

    // binary coefficients
    using Brine_CO2 = BinaryCoeff::Brine_CO2<Scalar, CO2Table>;
    using H2O_CH4   = BinaryCoeff::H2O_CH4;
    using H2O_H2    = BinaryCoeff::H2O_H2;

    // use constant salinity brine?
    static constexpr bool useConstantSalinity = Policy::useConstantSalinity();

    // The possible brine types
    using VariableSalinityBrine = Dumux::FluidSystems::Brine<Scalar, H2OType>;
    using ConstantSalinityBrine = Dumux::Components::Brine<Scalar, H2OType>;
    using BrineType = typename std::conditional_t< useConstantSalinity,
                                                   ConstantSalinityBrine,
                                                   VariableSalinityBrine >;

    /////////////////////////////////////////////////////////////////////////////////
    //! The following two indices are only used internally and are not part of the
    //! public interface. Depending on the chosen policy, i.e. if brine is used as
    //! a pseudo component or a fluid system with NaCl as a separate component, the
    //! indices that are part of the public interface are chosen by inheritance from
    //! Detail::BrineCH4CO2Indices (see documentation).
    //!
    //! depending on the implementation this is either brine (pseudo-component) or H2O
    static constexpr int BrineOrH2OIdx = 0;
    //! if the implementation considers NaCl as a real compoent, it gets the index 7
    static constexpr int NaClIdx = 7;


public:
    using ParameterCache = NullParameterCache;

    using H2O       = H2OType;
    using Brine     = BrineType;
    using CH4       = Components::CH4<Scalar>;
    using IdealGas  = Dumux::IdealGas<Scalar>;
    using Acetate   = Components::Acetate<Scalar>;
    using Amendment = Components::CarAm<Scalar>;
    using RMethyl   = Components::RMethyl<Scalar>;
    using H2        = Components::H2<Scalar>;
    using NaCl      = Components::NaCl<Scalar>;
    using CO2       = Dumux::Components::CO2<Scalar, CO2Table>;

    /****************************************
     * Fluid phase related static parameters
     ****************************************/
    static constexpr int numPhases  = 2;
    static constexpr int liquidPhaseIdx = 0; //!< index of the liquid phase
    static constexpr int gasPhaseIdx = 1;    //!< index of the gas phase
    static constexpr int phase0Idx = liquidPhaseIdx; //!< index of the first phase
    static constexpr int phase1Idx = gasPhaseIdx;    //!< index of the second phase


    /****************************************
     * Component related static parameters
     ****************************************/
    static constexpr int numComponents = useConstantSalinity ? 7 : 8;
    static constexpr int comp0Idx   = 0;
    static constexpr int comp1Idx   = 1;
    static constexpr int BrineIdx   = comp0Idx;
    static constexpr int CH4Idx     = comp1Idx;
    static constexpr int AcetateIdx   = 2;
    static constexpr int AmendmentIdx = 3;
    static constexpr int RMethylIdx   = 4;
    static constexpr int H2Idx        = 5;
    static constexpr int TCIdx        = 6;
    static constexpr int CO2Idx       = TCIdx;

private:

    // Adapter policy for the fluid state corresponding to the brine fluid system
    struct BrineAdapterPolicy
    {
        using FluidSystem = VariableSalinityBrine;

        static constexpr int phaseIdx(int brinePhaseIdx) { return phase0Idx; }
        static constexpr int compIdx(int brineCompIdx)
        {
            switch (brineCompIdx)
            {
                assert(brineCompIdx == VariableSalinityBrine::H2OIdx || brineCompIdx == VariableSalinityBrine::NaClIdx);
                case VariableSalinityBrine::H2OIdx: return BrineOrH2OIdx;
                case VariableSalinityBrine::NaClIdx: return NaClIdx;
                default: return 0; // this will never be reached, only needed to suppress compiler warning
            }
        }
    };

    template<class FluidState>
    using BrineAdapter = FluidStateAdapter<FluidState, BrineAdapterPolicy>;

public:

    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx)
    {
        switch (phaseIdx)
        {
            case BrineIdx: return std::string("w"); //TODO adapt to Dumux standard IOName::liquidPhase?
            case CH4Idx: return std::string("n");
        };
        DUNE_THROW(Dune::InvalidStateException, "Invalid phaseIdx index " << phaseIdx);
    }

    /*!
     * \brief Returns whether the fluids are miscible
     */
    static constexpr bool isMiscible()
    { return true; }

    /*!
     * \brief Return whether a phase is gaseous
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return phaseIdx == gasPhaseIdx;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isIdealGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // let the fluids decide
        if (phaseIdx == gasPhaseIdx)
            return useConstantSalinity ? (ConstantSalinityBrine::gasIsIdeal() && CO2::gasIsIdeal())
                                       : (H2O::gasIsIdeal() && CO2::gasIsIdeal());
        return false; // not a gas
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if Henry's law and Raoult's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if (phaseIdx == liquidPhaseIdx)
            return useConstantSalinity ? ConstantSalinityBrine::liquidIsCompressible()
                                       : VariableSalinityBrine::isCompressible(VariableSalinityBrine::liquidPhaseIdx);
        return true;
    }

    /*!
     * \brief Return the human readable name of a component
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {
        assert(0 <= compIdx && compIdx < numComponents);
        if (useConstantSalinity)
        {
            static std::string name[] = { ConstantSalinityBrine::name(),
                                          CH4::name(),
                                          Acetate::name(),
                                          Amendment::name(),
                                          RMethyl::name(),
                                          H2::name(),
                                          CO2::name() };
            return name[compIdx];
        }
        else
        {
            static std::string name[] = { VariableSalinityBrine::componentName(VariableSalinityBrine::H2OIdx),
                                          CH4::name(),
                                          Acetate::name(),
                                          Amendment::name(),
                                          RMethyl::name(),
                                          H2::name(),
                                          CO2::name(),
                                          VariableSalinityBrine::NaCl::name() };
            return name[compIdx];
        }
    }

    /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        assert(0 <= compIdx && compIdx < numComponents);
        if (useConstantSalinity)
        {
            static const Scalar M[] = { ConstantSalinityBrine::molarMass(),
                                        CH4::molarMass(),
                                        Acetate::molarMass(),
                                        Amendment::molarMass(),
                                        RMethyl::molarMass(),
                                        H2::molarMass(),
                                        CO2::molarMass() };
            return M[compIdx];
        }
        else
        {
            static const Scalar M[] = { VariableSalinityBrine::molarMass(VariableSalinityBrine::H2OIdx),
                                        CH4::molarMass(),
                                        Acetate::molarMass(),
                                        Amendment::molarMass(),
                                        RMethyl::molarMass(),
                                        H2::molarMass(),
                                        CO2::molarMass(),
                                        VariableSalinityBrine::molarMass(VariableSalinityBrine::NaClIdx) };
            return M[compIdx];
        }
    }

    /****************************************
     * thermodynamic relations
     ****************************************/

    // Initializing with default tables
    static void init()
    {
        init(/*startTemp=*/273.15, /*endTemp=*/623.15, /*tempSteps=*/100,
             /*startPressure=*/1e4, /*endPressure=*/40e6, /*pressureSteps=*/200);
    }

    // Initializing and custom tables
    static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
                     Scalar startPressure, Scalar endPressure, int pressureSteps)
    {
        std::cout << "The Brine-CH4-CO2 fluid system was configured with the following policy:\n";
        std::cout << " - use constant salinity: " << std::boolalpha << Policy::useConstantSalinity() << "\n";
        std::cout << " - use CO2 gas density as gas mixture density: " << std::boolalpha << Policy::useCO2GasDensityAsGasMixtureDensity() << std::endl;

        if(H2O::isTabulated)
            H2O::init(startTemp, endTemp, tempSteps,
                      startPressure, endPressure, pressureSteps);
    }

    using Base::density;
    /*!
     * \brief Given a phase's composition, temperature, pressure, and
     *        the partial pressures of all components, return its
     *        density \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param fluidState The fluid state
     * \param phaseIdx The index of the phase
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == phase0Idx)
        {
            // See: Eq. (7) in Class et al. (2002a)
            // This assumes each gas molecule displaces exactly one
            // molecule in the liquid.
            return H2O::liquidDensity(temperature, pressure);
//            return H2O::liquidDensity(temperature, pressure)/molarMass(BrineIdx)
//                   * (H2O::molarMass()*fluidState.moleFraction(phase0Idx, BrineIdx)
//                   + CH4::molarMass()*fluidState.moleFraction(phase0Idx, CH4Idx)
//                   + Acetate::molarMass()*fluidState.moleFraction(phase0Idx, AcetateIdx)
//                   + Amendment::molarMass()*fluidState.moleFraction(phase0Idx, AmendmentIdx)
//                   + RMethyl::molarMass()*fluidState.moleFraction(phase0Idx, RMethylIdx)
//                   + H2::molarMass()*fluidState.moleFraction(phase0Idx, H2Idx)
//                   + NaCl::molarMass()*fluidState.moleFraction(phase0Idx, NaClIdx)
//                   + CO2::molarMass()*fluidState.moleFraction(phase0Idx, TCIdx));
        }
        else
        {
            if (Policy::useCO2GasDensityAsGasMixtureDensity())
                // use the CO2 gas density only and neglect compositional effects
                return CO2::gasDensity(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));
            else
            {
                //assume ideal gas mixture
                const Scalar averageMolarMass = fluidState.averageMolarMass(phase1Idx);
                return IdealGas::density(averageMolarMass, temperature, pressure);
    //            return H2O::gasDensity(temperature, fluidState.partialPressure(phase1Idx, BrineIdx))
    //                   + CH4::gasDensity(temperature, fluidState.partialPressure(phase1Idx, CH4Idx))
    //                   + H2::gasDensity(temperature, fluidState.partialPressure(phase1Idx, H2Idx))
    //                   + CO2::gasDensity(temperature, fluidState.partialPressure(phase1Idx, TCIdx));
            }
        }
    }

    using Base::molarDensity;
   /*!
    * \brief The molar density \f$\rho_{mol,\alpha}\f$
    *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
    *
    * The molar density for the simple relation is defined by the
    * mass density \f$\rho_\alpha\f$ and the molar mass of the main component
    *
    * The molar density for the complrex relation is defined by the
    * mass density \f$\rho_\alpha\f$ and the mean molar mass \f$\overline M_\alpha\f$:
    *
    * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{\overline M_\alpha} \;.\f]
    */
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);

        if (phaseIdx == phase0Idx)
        {
            // assume pure water or that each gas molecule displaces exactly one
            // molecule in the liquid.
            return H2O::liquidMolarDensity(T, p);
        }
        else
        {
            if (Policy::useCO2GasDensityAsGasMixtureDensity())
                return CO2::gasMolarDensity(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));
            else
            {
                //assume ideal gas
                return IdealGas::molarDensity(T,p);
            }
        }
    }

    using Base::viscosity;
    /*!
     * \brief Calculate the dynamic viscosity of a fluid phase \f$\mathrm{[Pa*s]}\f$
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the viscosity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == phase0Idx)
            return useConstantSalinity ? ConstantSalinityBrine::liquidViscosity(temperature, pressure)
                                       : VariableSalinityBrine::viscosity( BrineAdapter<FluidState>(fluidState),
                                                                       VariableSalinityBrine::liquidPhaseIdx );
        else if (phaseIdx == phase1Idx)
            //assume pure CH4 viscosity TODO: add CO2 part?
            return CH4::gasViscosity(temperature, pressure);

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
    }

    using Base::fugacityCoefficient;
    /*!
     * \brief Returns the fugacity coefficient \f$\mathrm{[-]}\f$ of a component in a
     *        phase.
     *
     * The fugacity coefficient \f$\mathrm{\phi^\kappa_\alpha}\f$ of
     * component \f$\mathrm{\kappa}\f$ in phase \f$\mathrm{\alpha}\f$ is connected to
     * the fugacity \f$\mathrm{f^\kappa_\alpha}\f$ and the component's mole
     * fraction \f$\mathrm{x^\kappa_\alpha}\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     \f]
     * where \f$\mathrm{p_\alpha}\f$ is the pressure of the fluid phase.
     *
     * The fugacity itself is just an other way to express the
     * chemical potential \f$\mathrm{\zeta^\kappa_\alpha}\f$ of the component:
     *
     * \f[
     f^\kappa_\alpha := \exp\left\{\frac{\zeta^\kappa_\alpha}{k_B T_\alpha} \right\}
     \f]
     * where \f$\mathrm{k_B}\f$ is Boltzmann's constant.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component
     */
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIdx && compIdx < numComponents);

        if (phaseIdx == phase1Idx)
            // use the fugacity coefficients of an ideal gas. the
            // actual value of the fugacity is not relevant, as long
            // as the relative fluid compositions are observed,
            return 1.0;

        else if (phaseIdx == phase0Idx)
        {
            Scalar temperature = fluidState.temperature(phaseIdx);
            Scalar pressure = fluidState.pressure(phaseIdx);
            assert(temperature > 0);
            assert(pressure > 0);
            if (compIdx == BrineIdx)
                return H2O::vaporPressure(temperature)/pressure;
            else if (compIdx == CH4Idx)
                return H2O_CH4::henry(temperature)/pressure; //TODO Brine_CH4 henry: add salinity
            else if (compIdx == H2Idx)
                return H2O_H2::henry(temperature)/pressure; //TODO Check henry for salinity / create new brine_h2.hh?
            else if (compIdx == TCIdx) //TODO change once CO2Tables are added correctly
                return 1/pressure;
            // TODO more complex calculation available in brineco2.hh fluidsystem
            else
                return 0;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    using Base::diffusionCoefficient;
    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase \f$\mathrm{[mol^2 * s / (kg*m^3)]}\f$
     *
     * Molecular diffusion of a compoent \f$\mathrm{\kappa}\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \textbf{grad} mu_\kappa \f]
     *
     * where \f$\mathrm{\mu_\kappa}\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$\mathrm{J}\f$ is the
     * diffusive flux. \f$\mathrm{mu_\kappa}\f$ is connected to the component's
     * fugacity \f$\mathrm{f_\kappa}\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$\mathrm{p_\alpha}\f$ and \f$\mathrm{T_\alpha}\f$ are the fluid phase'
     * pressure and temperature.
     *
     * Maybe see http://www.ddbst.de/en/EED/PCP/DIF_C1050.php
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    }

    using Base::binaryDiffusionCoefficient;
    /*!
     * \brief Given the phase compositions, return the binary
     *        diffusion coefficient \f$\mathrm{[m^2/s]}\f$ of two components in a phase.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx Index of the component i
     * \param compJIdx Index of the component j
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIIdx && compIIdx < numComponents);
        assert(0 <= compJIdx && compJIdx < numComponents);
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
//
//        if (compIIdx > compJIdx)
//        {
//            using std::swap;
//            swap(compIIdx, compJIdx);
//        }
//
//        if (phaseIdx == phase0Idx
//            && compIIdx == BrineIdx
//            && compJIdx == CH4Idx)
//        {
//            assert(compIIdx == BrineIdx);
//            assert(compJIdx == CH4Idx);
//
//            Scalar result = H2O_CH4::liquidDiffCoeff(temperature, pressure);
//            Valgrind::CheckDefined(result);
//            return result;
//        }
//        else if (phaseIdx == phase1Idx
//                 && compJIdx == CH4Idx
//                 && compIIdx == BrineIdx)
//        {
//            assert(compIIdx == BrineIdx);
//            Scalar result = H2O_CH4::gasDiffCoeff(temperature, pressure);
//            Valgrind::CheckDefined(result);
//            return result;
//        }
//        else if ( phaseIdx == phase1Idx
//                  && compJIdx ==CO2Idx
//                  && compIIdx == BrineIdx)
//        {
//            assert(compIIdx == BrineIdx);
//            Scalar result = Brine_SimpleCO2::gasDiffCoeff(temperature, pressure);
//            Valgrind::CheckDefined(result);
//            return result;
//        }
//        else
//        {
//            //assume no binary diffusion for other components
//            return 0.0;
//        }

        if (phaseIdx == phase0Idx)
        {
         //TODO: Check values below!
         // All arbitrary atm, but range is ok and shouldn't make any difference.
         Scalar result = 0.0;
         if(compJIdx == CH4Idx)
             result = H2O_CH4::liquidDiffCoeff(temperature, pressure);
         else if (compJIdx == AcetateIdx)
             result = 0.67e-9; // glucose value from internet //TODO ACTUAL SOURCE GOES HERE!
         else if (compJIdx == AmendmentIdx)
             result = 0.67e-9; // arbitrary value
         else if (compJIdx == RMethylIdx)
             result = 0.67e-9; // arbitrary value
         else if (compJIdx == H2Idx)
             result = H2O_H2::liquidDiffCoeff(temperature, pressure);
         else if (compJIdx == NaClIdx)
             result = 1.587e-9;  //[m²/s]    //value for NaCl; J. Phys. D: Appl. Phys. 40 (2007) 2769-2776
         else if (compJIdx == TCIdx)
         {
             // result = 1.92e-9; // value for 25°C at atmospheric pressure (https://onlinelibrary.wiley.com/doi/pdf/10.1002/jrs.4742)
             if (compIIdx == BrineOrH2OIdx && compJIdx == CO2Idx)
                 return Brine_CO2::liquidDiffCoeff(temperature, pressure);
             if (!useConstantSalinity && compIIdx == BrineOrH2OIdx && compJIdx == NaClIdx)
                 return VariableSalinityBrine::binaryDiffusionCoefficient( BrineAdapter<FluidState>(fluidState),
                                                                           VariableSalinityBrine::liquidPhaseIdx,
                                                                           VariableSalinityBrine::H2OIdx,
                                                                           VariableSalinityBrine::NaClIdx );


         }
         else
             DUNE_THROW(Dune::NotImplemented, "Binary diffusion coefficient of components "
                                              << compIIdx << " and " << compJIdx
                                              << " in phase " << phaseIdx);
         Valgrind::CheckDefined(result);
         return result;
        }
        else
        {
            assert(phaseIdx == phase1Idx);

         if (compIIdx != CH4Idx)
         std::swap(compIIdx, compJIdx);
         Scalar result = 0.0;
         if(compJIdx == BrineIdx)
             result = H2O_CH4::gasDiffCoeff(temperature, pressure);
         else if (compJIdx == AcetateIdx)
             result = 0;
         else if (compJIdx == AmendmentIdx)
             result = 0;
         else if (compJIdx == RMethylIdx)
             result = 0;
         else if (compJIdx == H2Idx)
             result = H2O_H2::gasDiffCoeff(temperature, pressure);
         else if (compJIdx == NaClIdx)
             result = 0;
         else if (compJIdx == TCIdx)
            return Brine_CO2::gasDiffCoeff(temperature, pressure);
         else
             DUNE_THROW(Dune::NotImplemented, "Binary diffusion coefficient of components "
                                              << compIIdx << " and " << compJIdx
                                              << " in phase " << phaseIdx);
         Valgrind::CheckDefined(result);
         // !!! TODO all Diffusion OFF at the moment! CHANGE BACK!
         // Scalar result = 0.0;
         return result;
        }

    }



    using Base::enthalpy;
    /*!
     * \brief Given the phase composition, return the specific
     *        phase enthalpy \f$\mathrm{[J/kg]}\f$.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar enthalpy(const FluidState& fluidState, int phaseIdx)
    {
        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        if (phaseIdx == liquidPhaseIdx)
        {
            // Convert J/kg to kJ/kg
            const Scalar h_ls1 = useConstantSalinity ? ConstantSalinityBrine::liquidEnthalpy(T, p)/1e3
                                                     : VariableSalinityBrine::enthalpy( BrineAdapter<FluidState>(fluidState),
                                                                                        VariableSalinityBrine::liquidPhaseIdx )/1e3;

            // mass fraction of CO2 in Brine
            const Scalar X_CO2_w = fluidState.massFraction(liquidPhaseIdx, CO2Idx);

            // heat of dissolution for CO2 according to Fig. 6 in Duan and Sun 2003. (kJ/kg)
            // In the relevant temperature ranges CO2 dissolution is exothermal
            const Scalar delta_hCO2 = (-57.4375 + T * 0.1325) * 1000/44;

            // enthalpy contribution of water and CO2 (kJ/kg)
            const Scalar hw = H2O::liquidEnthalpy(T, p)/1e3;
            const Scalar hg = CO2::liquidEnthalpy(T, p)/1e3 + delta_hCO2;

            // Enthalpy of brine with dissolved CO2 (kJ/kg)
            return (h_ls1 - X_CO2_w*hw + hg*X_CO2_w)*1e3;
        }
        else if (phaseIdx == gasPhaseIdx)
        {
            Scalar result = 0;
            // we assume NaCl to not enter the gas phase, only consider H2O and CO2
            result += H2O::gasEnthalpy(T, p)*fluidState.massFraction(gasPhaseIdx, BrineOrH2OIdx);
            result += CO2::gasEnthalpy(T, p) *fluidState.massFraction(gasPhaseIdx, CO2Idx);
            result += CH4::gasEnthalpy(T, p)
                      * fluidState.massFraction(phase1Idx, CH4Idx);
            Valgrind::CheckDefined(result);
            return result;
        }

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
    }

    using Base::thermalConductivity;
    /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the thermal conductivity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState& fluidState, int phaseIdx)
    {
        if (phaseIdx == liquidPhaseIdx)
            return useConstantSalinity ? ConstantSalinityBrine::liquidThermalConductivity( fluidState.temperature(phaseIdx),
                                                                                           fluidState.pressure(phaseIdx) )
                                       : VariableSalinityBrine::thermalConductivity( BrineAdapter<FluidState>(fluidState),
                                                                                     VariableSalinityBrine::liquidPhaseIdx );
        else if (phaseIdx == gasPhaseIdx)
            return CO2::gasThermalConductivity(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
    }

    using Base::heatCapacity;
    /*!
     * \copybrief Base::heatCapacity
     *
     * \note We employ the heat capacity of the pure phases.
     *
     * \todo TODO Implement heat capacity for gaseous CO2
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        if(phaseIdx == liquidPhaseIdx)
            return useConstantSalinity ? ConstantSalinityBrine::liquidHeatCapacity( fluidState.temperature(phaseIdx),
                                                                                    fluidState.pressure(phaseIdx) )
                                       : VariableSalinityBrine::heatCapacity( BrineAdapter<FluidState>(fluidState),
                                                                              VariableSalinityBrine::liquidPhaseIdx );
        else if (phaseIdx == gasPhaseIdx)
            return CH4::liquidHeatCapacity(fluidState.temperature(phaseIdx),
                                           fluidState.pressure(phaseIdx));

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
    }

private:

};

} // end namespace FluidSystems
} // end namespace Dumux

#endif
