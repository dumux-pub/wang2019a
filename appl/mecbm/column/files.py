import numpy
import os
import shutil

def get_subdirectories(dir_path):
    return [name for name in os.listdir(dir_path)
            if os.path.isdir(os.path.join(dir_path, name))]

def delete_build_cmake(dir_path):
    dir1 = get_subdirectories(dir_path)
    for dir in dir1:
        path = os.path.join(dir_path,dir)
        subpath = (os.path.join(path,"build-cmake"))
        if os.path.exists(subpath):
            shutil.rmtree(subpath)


