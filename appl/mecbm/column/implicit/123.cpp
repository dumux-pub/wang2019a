//
// Created by 王悦 on 02.11.19.
//

// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
#ifndef DUMUX_MECBM_COLUMN_PROBLEM_HH
#define DUMUX_MECBM_COLUMN_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/brinech4co2.hh>

#include <dumux/material/solidsystems/mecbm.hh>

#include "mecbmcolumnspatialparams.hh"
#include <dumux/material/chemistry/biogeochemistry/mecbmreactions.hh>

#include <test/porousmediumflow/co2/implicit/co2tables.hh>

namespace Dumux {
/*!
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
    template <class TypeTag>
    class MECBMColumnProblem;

    namespace Properties {
// Create new type tags
        namespace TTag {
            struct MECBMColumnTypeTag { using InheritsFrom = std::tuple<TwoPNCMin>; };
            struct MECBMColumnBoxTypeTag { using InheritsFrom = std::tuple<MECBMColumnTypeTag, BoxModel>; };
            struct MECBMColumnCCTpfaTypeTag { using InheritsFrom = std::tuple<MECBMColumnTypeTag, CCTpfaModel>; };
        } // end namespace TTag

// Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::MECBMColumnTypeTag> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::MECBMColumnTypeTag> { using type = MECBMColumnProblem<TypeTag>; };

// Set fluid configuration
        template<class TypeTag>
        struct FluidSystem<TypeTag, TTag::MECBMColumnTypeTag> {
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = FluidSystems::BrineCH4CO2<Scalar,
            HeterogeneousCO2Tables::CO2Tables,
            Components::TabulatedComponent<Components::H2O<GetPropType<TypeTag, Properties::Scalar>>>,
            FluidSystems::BrineCH4CO2DefaultPolicy</*constantSalinity=*/true, true>>;
        };

        template<class TypeTag>
        struct SolidSystem<TypeTag, TTag::MECBMColumnTypeTag> {
            using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
            using type = SolidSystems::MECBMSolidPhase<Scalar>;
        };

// Set the spatial parameters
        template<class TypeTag>
        struct SpatialParams<TypeTag, TTag::MECBMColumnTypeTag>{
            using MT = GetPropType<TypeTag, ModelTraits>;
            static constexpr int numFluidComps = MT::numFluidComponents();
            static constexpr int numActiveSolidComps = MT::numSolidComps() - MT::numInertSolidComps();
            using type = MECBMColumnSpatialParams<GetPropType<TypeTag, FVGridGeometry>, GetPropType<TypeTag, Scalar>, numFluidComps, numActiveSolidComps>;
        };

//Set properties here to override the default property settings
        template<class TypeTag>
        struct ReplaceCompEqIdx<TypeTag, TTag::MECBMColumnTypeTag> { static constexpr int value = 9; }; //!< Replace gas balance by total mass balance

// Default formulation is pw-Sn, overwrite if necessary
        template<class TypeTag>
        struct Formulation<TypeTag, TTag::MECBMColumnTypeTag>
        { static constexpr auto value = TwoPFormulation::p0s1; };

// Enable caching or not (reference solutions created without caching)
// SET_BOOL_PROP(MECBMColumnTypeTag, EnableFVGridGeometryCache, true);
// SET_BOOL_PROP(MECBMColumnTypeTag, EnableGridVolumeVariablesCache, true);
// SET_BOOL_PROP(MECBMColumnTypeTag, EnableGridFluxVariablesCache, true);

    }

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water is injected to flush precipitated salt in a gas reservoir clogged due to precipitated salt.
 *
 * The domain is sized 10m times 20m and contains a vertical low-permeable layer of precipitated salt near an extraction well.
 *
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2pncmin</tt>
 */
    template <class TypeTag>
    class MECBMColumnProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
        using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
        using Chemistry = typename Dumux::MECBMReactionsChemistry<TypeTag>;
        using Indices = TwoPNCIndices;

        enum {
            pressureIdx = Indices::pressureIdx,
            switchIdx   = Indices::switchIdx, //Saturation

            //Indices of the phases
                    phase0Idx = FluidSystem::phase0Idx,
            phase1Idx = FluidSystem::phase1Idx,

            //Indices of the components
                    numComponents = FluidSystem::numComponents,
            comp0Idx    = FluidSystem::BrineIdx,
            comp1Idx    = FluidSystem::CH4Idx,
            AcetateIdx  = FluidSystem::AcetateIdx,
            AmendmentIdx= FluidSystem::AmendmentIdx,
            RMethylIdx  = FluidSystem::RMethylIdx,
            H2Idx       = FluidSystem::H2Idx,
            TCIdx       = FluidSystem::TCIdx,

            //Indices of the bio/coal volume fractions
                    CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
            AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
            AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
            HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
            MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
            CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,

            //Index of the primary component of G and L phase
                    conti0EqIdx = Indices::conti0EqIdx,

            // Phase State
                    wPhaseOnly = Indices::firstPhaseOnly,
            nPhaseOnly = Indices::secondPhaseOnly,
            bothPhases = Indices::bothPhases,

            // Grid and world dimension
                    dim      = GridView::dimension,
            dimWorld = GridView::dimensionworld,
        };

        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
        using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
        using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
        using Element = typename GridView::template Codim<0>::Entity;
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
        using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
        using SubControlVolume = typename FVElementGeometry::SubControlVolume;
        using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
        using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
        using CoordScalar = typename GridView::ctype;
        using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;


        //! property that defines whether mole or mass fractions are used
        static constexpr bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
        static constexpr auto numPhases = FluidSystem::numPhases;

    public:
        MECBMColumnProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
                : ParentType(fvGridGeometry)
        {
            name_                   = getParam<std::string>("Problem.Name");
            temperature_            = getParam<Scalar>("Problem.Temperature");
            reservoirPressure_      = getParam<Scalar>("Problem.ReservoirPressure");

            initxwCH4_              = getParam<Scalar>("Initial.initxwCH4"); // Initial wetting mole fraction of CH4
            initxwH2_               = getParam<Scalar>("Initial.initxwH2"); // Initial wetting mole fraction of Hydrogen
            initMassConcAmendment   = getParam<Scalar>("Initial.initMassConcAmendment"); // Initial mass concentration of Amendment
            initxwAcetate_          = getParam<Scalar>("Initial.initxwAcetate"); // Initial wetting mole fraction of Acetate
            initxwRMethyl_          = getParam<Scalar>("Initial.initxwRMethyl"); // Initial wetting mole fraction of Methyl
            initxwTC_               = getParam<Scalar>("Initial.initxwTC"); // Initial wetting mole fraction of total inorganic carbon
            initPhiCCoal_           = getParam<Scalar>("Initial.initPhiCCoal"); // Initial volume fraction of bioavailable coal
            initPhiCoalBac_         = getParam<Scalar>("Initial.initPhiCoalBac"); // Initial volume fraction of coal consuming bacteria
            initPhiAmCoalBac_       = getParam<Scalar>("Initial.initPhiAmCoalBac"); // Initial volume fraction of amendment and coal consuming bacteria
            initPhiAcetoArch_       = getParam<Scalar>("Initial.initPhiAcetoArch"); // Initial volume fraction of acetoclastics archaea
            initPhiHydroArch_       = getParam<Scalar>("Initial.initPhiMethyArch"); // Initial volume fraction of methylotrophic archaea
            initPhiMethyArch_       = getParam<Scalar>("Initial.initPhiHydroArch"); // Initial volume fraction of hydrogenotrophic archaea

            numInjections_          = getParam<int>("Injection.numInjections");
            injectionParameters_    = getParam<std::string>("Injection.InjectionParamFile");
            injQ_                   = getParam<Scalar>("Injection.injQ");

            nTemperature_           = getParam<int>("FluidSystem.NTemperature");
            nPressure_              = getParam<int>("FluidSystem.NPressure");
            pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
            pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
            temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
            temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");

            std::ifstream injectionData;
            std::string row;
            injectionData.open( injectionParameters_); // open the Injection data file
            if (not injectionData.is_open())
            {
                std::cerr << "\n\t -> Could not open file '"
                          << injectionParameters_
                          << "'. <- \n\n\n\n";
                exit(1) ;
            }
            int tempType = 0;

            // print file to make sure it is the right file
            std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
            while(!injectionData.eof())
            {
                getline(injectionData, row);
                std::cout << row << std::endl;
            }
            injectionData.close();

            // read data from file
            injectionData.open(injectionParameters_);

            while(!injectionData.eof())
            {
                getline(injectionData, row);

                if(row == "InjectionTypes")
                {
                    getline(injectionData, row);
                    while(row != "#")
                    {
                        if (row != "#")
                        {
                            std::istringstream ist(row);
                            ist >> tempType;
                            injType_.push_back(tempType);
                            std::cout << "size of injType: "<<injType_.size() << std::endl;
                        }
                        getline(injectionData, row);
                    }
                }
            }

            injectionData.close();

            if (injType_.size() != numInjections_)
            {
                std::cerr <<  "numInjections from the parameterfile and the number of injection types specified in the injection data file do not match!"
                          <<"\n numInjections from parameter file = "<<numInjections_
                          <<"\n numInjTypes from injection data file = "<<injType_.size()
                          <<"\n Abort!\n";
                exit(1) ;
            }

            unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
            Kxx_.resize(fvGridGeometry->gridView().size(codim));
            Kyy_.resize(fvGridGeometry->gridView().size(codim));
            Kzz_.resize(fvGridGeometry->gridView().size(codim));
            cellVolume_.resize(fvGridGeometry->gridView().size(codim));
            molOutFluxCH4w_.resize(fvGridGeometry->gridView().size(codim));
            molOutFluxCH4n_.resize(fvGridGeometry->gridView().size(codim));



            FluidSystem::init(/*Tmin=*/temperatureLow_,
                    /*Tmax=*/temperatureHigh_,
                    /*nT=*/nTemperature_,
                    /*pmin=*/pressureLow_,
                    /*pmax=*/pressureHigh_,
                    /*np=*/nPressure_);
        }

        void setTime( Scalar time )
        {
            time_ = time;
        }

        void setTimeStepSize( Scalar timeStepSize )
        {
            timeStepSize_ = timeStepSize;
        }

        void setEpisodeIdx( Scalar epiIdx )
        {
            episodeIdx_ = epiIdx;
        }

        int injectionType( Scalar epiIdx )
        {
            return injType_[epiIdx];
        }


        /*!
         * \name Problem parameters
         */


        /*!
         * \brief The problem name.
         *
         * This is used as a prefix for files generated by the simulation.
         */
        const std::string& name() const
        { return name_; }

        /*!
         * \brief Returns the temperature within the domain.
         *
         * This problem assumes a temperature of 10 degrees Celsius.
         */
        Scalar temperature() const
        { return temperature_; }

        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment.
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            // default to Neumann
            bcTypes.setAllNeumann();

//        const Scalar zMax = this->fvGridGeometry().bBoxMax()[dim-1];
//        if (globalPos[1] > zMax - eps_ )
//            bcTypes.setDirichlet(pressureIdx);

            return bcTypes;
        }

        /*!
         * \brief Evaluate the boundary conditions for a neumann
         *        boundary segment.
         *
         * This is the method for the case where the Neumann condition is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The neumann values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scvf The sub control volume face
         *
         * For this method, the \a values parameter stores the flux
         * in normal direction of each phase. Negative values mean influx.
         * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
         */
        NumEqVector neumann(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf) const
        {
            NumEqVector flux(0.0);

            const auto& ipGlobal = scvf.ipGlobal();
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];


            Scalar area = M_PI*0.0525*0.0525/4;

            Scalar waterFlux = injQ_/area;//*0.49/area;// [m/s]

            //! Inflow boundary at bottom

            int injProcess = injType_[episodeIdx_];

            if(ipGlobal[dimWorld-1] < eps_) // standard: no injection
            {
                if (injProcess == -99) // no injection
                {
                    flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx) / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux-99 \n"<<flux <<std::endl;

                }
                else if (injProcess == 1) //water injection
                {
                    flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux1 \n"<<flux <<std::endl;

//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
                }
                else if (injProcess == 2) //water injection with Amendment
                {
//                flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux2 \n"<<flux <<std::endl;

                    flux[AmendmentIdx] = - 1e-6* initMassConcAmendment // 15000 kg/m3liq
                                         / FluidSystem::molarMass(AmendmentIdx) // 0.113 kg/mol -> mol/m3liq
                                         / (61 *86400); // 61 days streched out injection to emulate cloquing sec injection duration --> mol/(m3tot * s)
                    flux[comp1Idx] = - 1e-6* initMassConcAmendment // 0.1 kg/m3liq
                                     / FluidSystem::molarMass(AmendmentIdx) // 0.113 kg/mol
                                     / (61 *86400); // 61 days streched out injection to emulate cloquing  sec injection duration
                }
                else if (injProcess == 0) // water injection
                {
                    flux = 0.0;
//                flux[comp0Idx] = 0;
//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
                }
            }
            else
                flux = 0.0; // kg/m/s

            // no-flow everywhere except at the top boundary
            if(ipGlobal[dimWorld-1] < this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps_)
                return flux;

            //! Outflow boundary at top

            // set a fixed pressure on the top of the domain
            const Scalar dirichletPressure = reservoirPressure_;

            // evaluate the gradient
            const auto gradient = [&](int phaseIdx)->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();
                const Scalar scvCenterPressureSol = volVars.pressure(phaseIdx);
//            auto grad = ipGlobal - scvCenter;
                auto grad = scvf.unitOuterNormal();
//            grad /= grad.two_norm2(); // ?
                grad *= -(dirichletPressure - scvCenterPressureSol)/ (ipGlobal -scvCenter).two_norm();
                return grad;
            };

            const Tensor K = volVars.permeability();
            for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx )
            {
                Scalar tpfaFlux = 0;
                for (int compIdx = 0; compIdx < numComponents; ++compIdx )
                {
                    const Scalar density = useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx);

                    // calculate the flux
                    tpfaFlux = vtmv(gradient(phaseIdx), K, scvf.unitOuterNormal());
                    auto gravity = this->gravity();
                    gravity *= volVars.density(phaseIdx);
                    tpfaFlux += vtmv(gravity, K, scvf.unitOuterNormal());
                    tpfaFlux *=  density * volVars.mobility(phaseIdx) * scvf.area();
                    //for safety: enforce outflow only in case some newton iteration goes wrong
                    if (tpfaFlux < 0)
                        std::cout<<"tpfaFlux"<<std::endl;
//                   tpfaFlux = 0;

                    // emulate an outflow condition for the component transport on the top side
                    tpfaFlux  *= (useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx));
                    flux[compIdx] += tpfaFlux;

                }
                molOutFlux_[phaseIdx] = tpfaFlux*scvf.area();
            }
//        std::cout<<"TopFlux \n"<<flux <<std::endl;

            return flux;
        }

        /*!
         * \brief Evaluate the boundary conditions for a dirichlet
         *        boundary segment.
         */
        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);

            return priVars;
        }

        /*!
         * \brief Evaluate the initial value for a control volume.
         *
         * \param globalPos The global position
         *
         * For this method, the \a values parameter stores primary
         * variables.
         */
        PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
        {
            PrimaryVariables priVars(0.0);

            priVars.setState(wPhaseOnly);
            priVars[pressureIdx]  = reservoirPressure_;
            priVars[switchIdx]    = initxwCH4_;                 // Sw primary variable

            // components
            priVars[H2Idx]        = initxwH2_;                  // Initial wetting mole fraction of Hydrogen
            priVars[AmendmentIdx] = 0;                          // set via injection file
            priVars[AcetateIdx]   = initxwAcetate_;             // Initial wetting mole fraction of Acetate
            priVars[RMethylIdx]   = initxwRMethyl_;             // Initial wetting mole fraction of Methyl
            priVars[TCIdx]        = initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon
            // biofilm/coal volume fractions
            priVars[CCoalIdx]     = initPhiCCoal_;              // Initial volume fraction of bioavailable coal
            priVars[CoalBacIdx]   = initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
            priVars[AmCoalBacIdx] = initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
            priVars[AcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
            priVars[MethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
            priVars[HydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction of methylotrophic archaea

            return priVars;
        }

        /*!
         * \name Volume terms
         */
        // \{

        /*!
         * \brief Evaluate the source term for all phases within a given
         *        sub-control-volume.
         *
         * This is the method for the case where the source term is
         * potentially solution dependent and requires some quantities that
         * are specific to the fully-implicit method.
         *
         * \param values The source and sink values for the conservation equations in units of
         *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
         * \param element The finite element
         * \param fvGeometry The finite-volume geometry
         * \param elemVolVars All volume variables for the element
         * \param scv The subcontrolvolume
         *
         * For this method, the \a values parameter stores the conserved quantity rate
         * generated or annihilate per volume unit. Positive values mean
         * that the conserved quantity is created, negative ones mean that it vanishes.
         * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
         */
        NumEqVector source(const Element &element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const SubControlVolume &scv) const
        {
            NumEqVector source(0.0);

            Chemistry chemistry;
            const auto& volVars = elemVolVars[scv];
            chemistry.reactionSource(source, volVars, timeStepSize_);

            return source;
        }

        /*!
         * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
         */

        const std::vector<Scalar>& getKxx()
        {
            return Kxx_;
        }

        const std::vector<Scalar>& getKyy()
        {
            return Kyy_;
        }

        const std::vector<Scalar>& getKzz()
        {
            return Kzz_;
        }

        const std::vector<Scalar>& cellVolume()
        {
            return cellVolume_;
        }

        const auto& molOutFluxCH4w()
        {
            return molOutFluxCH4w_;
        }

        const auto& molOutFluxCH4n()
        {
            return molOutFluxCH4n_;
        }
        /*!
         * \brief Return how much the domain is extruded at a given sub-control volume.
         *
         * This means the factor by which a lower-dimensional (1D or 2D)
         * entity needs to be expanded to get a full dimensional cell. The
         * default is 1.0 which means that 1D problems are actually
         * thought as pipes with a cross section of 1 m^2 and 2D problems
         * are assumed to extend 1 m to the back.
         */
        template<class ElementSolution>
        Scalar extrusionFactor(const Element &element,
                               const SubControlVolume &scv,
                               const ElementSolution& elemSol) const
        {
            const auto radius = 0.0525/2;
            return M_PI*radius*radius;
        }

        void updateVtkOutput(const SolutionVector& curSol)
        {
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {
                const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    const auto dofIdxGlobal = scv.dofIndex();
                    Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                    Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
                    Kzz_[dofIdxGlobal] = volVars.permeability()[2][2];
                    cellVolume_[dofIdxGlobal] = element.geometry().volume();
                    molOutFluxCH4w_[dofIdxGlobal] = molOutFlux_[phase0Idx][comp1Idx];
                    molOutFluxCH4n_[dofIdxGlobal] = molOutFlux_[phase1Idx][comp1Idx];
                }
            }

            writeFluxes();
        }

        void writeFluxes()
        {
            // filename of the output file
            std::string fileName = this->name();
            fileName += ".dat";
            std::ofstream dataFile;
            if (time_ == 0.0)
            {
                dataFile.open(fileName.c_str());
                dataFile << "#time timeStepSize molCH4w molCH4n \n";
                dataFile.close();
            }

            const Scalar time = time_;
            const Scalar timeStepSize = timeStepSize_;
            const Scalar molOutFluxCH4w = molOutFlux_[phase0Idx][comp1Idx];
            const Scalar molOutFluxCH4n = molOutFlux_[phase1Idx][comp1Idx];
            dataFile.open(fileName, std::ios::app);
            //time ; Sw ; Sn ; poro ; mwCh4 ; mnCh4
            dataFile<< time
                    <<" "
                    << timeStepSize
                    << " "
                    << molOutFluxCH4w
                    << " "
                    << molOutFluxCH4n
                    ;
            dataFile << "\n";
            dataFile.close();
        }

    private:

        /*!
         * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
         *
         * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
         */
        static Scalar massToMoleFrac_(Scalar XwNaCl)
        {
            const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(comp0Idx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
            const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

            const Scalar X_NaCl = XwNaCl;
            /* XwNaCl: conversion from mass fraction to mol fraction */
            auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
            return xwNaCl;
        }

        int nTemperature_;
        int nPressure_;
        std::string name_;

        Scalar pressureLow_, pressureHigh_;
        Scalar temperatureLow_, temperatureHigh_;
        Scalar reservoirPressure_;
        Scalar temperature_;

        Scalar initxwCH4_;                 // Initial wetting mole fraction of methane
        Scalar initxwH2_;                  // Initial wetting mole fraction of Hydrogen
        Scalar initMassConcAmendment;      // Initial wetting mole fraction of Amendment
        Scalar initxwAcetate_;             // Initial wetting mole fraction of Acetate
        Scalar initxwRMethyl_;             // Initial wetting mole fraction of Methyl
        Scalar initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon

        // biofilm/coal volume fractions
        Scalar initPhiCCoal_;              // Initial volume fraction of bioavailable coal
        Scalar initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
        Scalar initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
        Scalar initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
        Scalar initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
        Scalar initPhiHydroArch_;          // Initial volume fraction of methylotrophic archaea

        std::vector<int> injType_;
        int numInjections_;                // number of Injections
        std::string injectionParameters_;  // injectionParamteres from file
        Scalar injQ_;                      // injected volume flux in m3/s

        Scalar time_ = 0.0;
        Scalar timeStepSize_ = 0.0;
        Scalar episodeIdx_ = 0;

        static constexpr Scalar eps_ = 1e-6;
        std::vector<double> Kxx_;
        std::vector<double> Kyy_;
        std::vector<double> Kzz_;
        std::vector<double> cellVolume_;
        std::vector<double> molOutFluxCH4w_;
        std::vector<double> molOutFluxCH4n_;

        mutable std::array<NumEqVector,numPhases> molOutFlux_;
    };

} //end namespace Dumux

#endif
