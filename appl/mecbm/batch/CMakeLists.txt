dune_symlink_to_source_files(FILES test_mecbmbatch.input)
dune_symlink_to_source_files(FILES test_mecbmbatchAmendment.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal000.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal010.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal011.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal100.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal110.input)
dune_symlink_to_source_files(FILES test_mecbmbatchCoal111.input)
dune_symlink_to_source_files(FILES test_mecbmbatchGB100.input)
dune_symlink_to_source_files(FILES test_mecbmbatchGB110.input)
dune_symlink_to_source_files(FILES test_mecbmbatchGB111.input)
dune_symlink_to_source_files(FILES startBatch.sh)

# isothermal tests
dune_add_test(NAME test_mecbmbatch_tpfa
              SOURCES test_mecbmbatch_fv.cc
              COMPILE_DEFINITIONS TYPETAG=MECBMBatchCCTpfaTypeTag)

dune_symlink_to_source_files(FILES injections)
