// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief Fluid system for bench mark SHPCO2 of 9 components. 
 *  Details in Haeberlein, Florian. 
 * "Time space domain decomposition methods for reactive transport
 * -Application to CO 2 geological storage. No. FRNC-TH--8443."
 * Universite Paris XIII, 2011.
 * \note Immobility of gas phase!
 * \note The two aqueous equilibriums are realized through the function
 * of concentrations. Cl is as trace is not included.
 * 6 components as primary variables and 2 secondary species do not
 * appear in solution.
 */
#ifndef DUMUX_SHPCO2_FLUID_SYSTEM_HH
#define DUMUX_SHPCO2_FLUID_SYSTEM_HH

#include <type_traits>
#include <limits>

#include <dune/common/exceptions.hh>

#include <dumux/common/parameters.hh>
#include <dumux/material/idealgas.hh>
#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/fluidstates/adapter.hh>

#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/OHIon.hh>
#include <dumux/material/components/HIon.hh>
#include <dumux/material/components/NaIon.hh>
#include <dumux/material/components/HCO3Ion.hh>
#include <dumux/material/components/co2.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/CaIon.hh>
#include <dumux/material/components/SiO2.hh>

#include <dumux/io/name.hh>

namespace Dumux
{

// include the default tables for CO2
#ifndef DOXYGEN // hide tables from doxygen
#include <dumux/material/components/co2tables.inc>
#endif

    namespace FluidSystems
    {

        /*!
    * \ingroup Fluidsystems
    * \brief Default policy for the fluid system
    */
        template <bool fastButSimplifiedRelations = false, bool immobileGasPhase = true>
        struct CO2DefaultPolicy
        {
            static constexpr bool useCO2GasDensityAsGasMixtureDensity() { return fastButSimplifiedRelations; }
            static constexpr bool useCO2ViscosityAsGasMixtureViscosity() { return fastButSimplifiedRelations; }
            static constexpr bool useH2ODensityAsFluidMixtureDensity() { return fastButSimplifiedRelations; }
            static constexpr bool isGasPhaseImmobile() { return immobileGasPhase; }
        };
    }; // namespace FluidSystems

/*!
 * \ingroup Fluidsystems
 * \brief A compositional fluid system referring to the SPHCO2.
 *
 * \note 
 */
    template <class Scalar,
              class CO2Table,
              class H2OType = Components::TabulatedComponent<Components::H2O<Scalar>>,
              class Policy = CO2DefaultPolicy</*fastButSimplifiedRelations?*/ true,
                                              /*immobileGasPhase*/ true>>
    class SPHCO2FluidSystem
        : public Base<Scalar, SPHCO2FluidSystem<Scalar, CO2Table, H2OType, Policy>>
    {
        using ThisType = SPHCO2FluidSystem<Scalar, CO2Table, H2OType, Policy>;
        using Base = Dumux::FluidSystems::Base<Scalar, ThisType>;

    public:
        using ParameterCache = NullParameterCache;

        using H2O = H2OType;
        using HIon = Dumux::Components::HIon<Scalar>;
        using OHIon = Dumux::Components::OHIon<Scalar>;
        using NaIon = Dumux::Components::NaIon<Scalar>;
        using CO2 = Dumux::Components::CO2<Scalar, CO2Table>;
        using HCO3Ion = Dumux::Components::HCO3Ion<Scalar>;
        using CaIon = Dumux::Components::CaIon<Scalar>;
        using SiO2 = Dumux::Components::SiO2<Scalar>;

        static constexpr int numComponents = 6;
        static constexpr int numPhases = 2;

        // the number of chemical equilibrium in aqueous phase
        static constexpr int numEquilibrium = 2;
        
        // the total number of components 
        static constexpr int numSpecies = numComponents + numEquilibrium;

        static constexpr int liquidPhaseIdx = 0;         //!< index of the liquid phase
        static constexpr int gasPhaseIdx = 1;            //!< index of the gas phase
        static constexpr int phase0Idx = liquidPhaseIdx; //!< index of the first phase
        static constexpr int phase1Idx = gasPhaseIdx;    //!< index of the second phase

        static constexpr int comp0Idx = 0;
        static constexpr int comp1Idx = 1;
        static constexpr int H2OIdx = comp0Idx;
        static constexpr int CO2Idx = comp1Idx;
        static constexpr int HIonIdx = 2;
        static constexpr int NaIonIdx = 3;
        static constexpr int CaIonIdx = 4;
        static constexpr int SiO2Idx = 5;
        static constexpr int OHIonIdx = 6;
        static constexpr int HCO3IonIdx = 7;

    public:
        /*!
     * \brief Return the human readable name of a fluid phase
     * \param phaseIdx The index of the fluid phase to consider
     */
        static std::string phaseName(int phaseIdx)
        {
            switch (phaseIdx)
            {
            case liquidPhaseIdx:
                return IOName::liquidPhase();
            case gasPhaseIdx:
                return IOName::gaseousPhase();
            }
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
        }

        /*!
     * \brief Returns whether the fluids are miscible
     */
        static constexpr bool isMiscible()
        {
            return true;
        }

        /*!
     * \brief Return whether a phase is gaseous
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
        static constexpr bool isGas(int phaseIdx)
        {
            assert(0 <= phaseIdx && phaseIdx < numPhases);

            return phaseIdx == gasPhaseIdx;
        }

        /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     * \param phaseIdx The index of the fluid phase to consider
     */
        static constexpr bool isIdealGas(int phaseIdx)
        {
            assert(0 <= phaseIdx && phaseIdx < numPhases);
            // let the fluids decide
            if (phaseIdx == gasPhaseIdx)
                return H2O::gasIsIdeal() && CO2::gasIsIdeal();
            return false; // not a gas
        }

        /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if Henry's law and Raoult's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
        static bool isIdealMixture(int phaseIdx)
        {
            assert(0 <= phaseIdx && phaseIdx < numPhases);
            if (phaseIdx == liquidPhaseIdx)
                return false;
            return true;
        }

        /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
        static constexpr bool isCompressible(int phaseIdx)
        {
            assert(0 <= phaseIdx && phaseIdx < numPhases);
            if (phaseIdx == liquidPhaseIdx)
                return false;
            return true;
        }

        /*!
     * \brief Return the human readable name of a component
     * \param compIdx The index of the component to consider
     */
        static std::string componentName(int compIdx)
        {
            assert(0 <= compIdx && compIdx < numSpecies);

            static std::string name[] = {
                H2O::name(),
                CO2::name(),
                HIon::name(),
                NaIon::name(),
                CaIon::name(),
                SiO2::name(),
                OHIon::name(),
                HCO3Ion::name(),
            };
            return name[compIdx];
        }

        /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     * \param compIdx The index of the component to consider
     */
        static Scalar molarMass(int compIdx)
        {
            assert(0 <= compIdx && compIdx < numSpecies);
            static const Scalar M[] = {
                H2O::molarMass(),
                CO2::molarMass(),
                HIon::molarMass(),
                NaIon::molarMass(),
                CaIon::molarMass(),
                SiO2::molarMass(),
                OHIon::molarMass(),
                HCO3Ion::molarMass(),
            };

            return M[compIdx];
        }

        /****************************************
     * thermodynamic relations
     ****************************************/

        // Initializing with salinity and default tables
        static void init()
        {
            init(/*startTemp=*/273.15, /*endTemp=*/623.15, /*tempSteps=*/100,
                 /*startPressure=*/1e4, /*endPressure=*/40e6, /*pressureSteps=*/200);
        }

        // Initializing with custom tables
        static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
                         Scalar startPressure, Scalar endPressure, int pressureSteps)
        {
            std::cout << "The fluid system was configured with the following policy:\n";
            std::cout << " - the gas phase is immobile: " << std::boolalpha << Policy::isGasPhaseImmobile() << "\n";
            std::cout << " - use CO2 gas density as gas mixture density: " << std::boolalpha << Policy::useCO2GasDensityAsGasMixtureDensity() << std::endl;

            if (H2O::isTabulated)
                H2O::init(startTemp, endTemp, tempSteps, startPressure, endPressure, pressureSteps);
        }

        using Base::density;
        /*!
     * \brief Given a phase's composition, temperature, pressure, and
     *        the partial pressures of all components, return its
     *        density \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param fluidState The fluid state
     * \param phaseIdx The index of the phase
     */
        template <class FluidState>
        static Scalar density(const FluidState &fluidState, int phaseIdx)
        {
            Scalar T = fluidState.temperature(phaseIdx);
            if (phaseIdx == liquidPhaseIdx)

                return useH2ODensityAsFluidMixtureDensity ? H20::density : Dune

            else if (phaseIdx == gasPhaseIdx)
            {
                if (Policy::useCO2GasDensityAsGasMixtureDensity())
                    // use the CO2 gas density only and neglect compositional effects
                    return CO2::gasDensity(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));
                else
                {
                    // assume ideal mixture: steam and CO2 don't "see" each other
                    Scalar rho_gH2O = H2O::gasDensity(T, fluidState.partialPressure(gasPhaseIdx, H2OIdx));
                    Scalar rho_gCO2 = CO2::gasDensity(T, fluidState.partialPressure(gasPhaseIdx, CO2Idx));
                    return (rho_gH2O + rho_gCO2);
                }
            }

            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
        }

        using Base::molarDensity;
        /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density is defined by the
     * mass density \f$\rho_\alpha\f$ and the mean molar mass \f$\overline M_\alpha\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{\overline M_\alpha} \;.\f]
     */
        template <class FluidState>
        static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
        {
            Scalar T = fluidState.temperature(phaseIdx);
            if (phaseIdx == liquidPhaseIdx)
                return density(fluidState, phaseIdx) / fluidState.averageMolarMass(phaseIdx);
            else if (phaseIdx == gasPhaseIdx)
            {
                if (Policy::useCO2GasDensityAsGasMixtureDensity())
                    return CO2::gasMolarDensity(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));
                else
                {
                    // assume ideal mixture: steam and CO2 don't "see" each other
                    Scalar rhoMolar_gH2O = H2O::gasMolarDensity(T, fluidState.partialPressure(gasPhaseIdx, H2OIdx));
                    Scalar rhoMolar_gCO2 = CO2::gasMolarDensity(T, fluidState.partialPressure(gasPhaseIdx, CO2Idx));
                    return rhoMolar_gH2O + rhoMolar_gCO2;
                }
            }
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
        }

        using Base::viscosity;
        /*!
     * \brief Calculate the dynamic viscosity of a fluid phase \f$\mathrm{[Pa*s]}\f$
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the viscosity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
        template <class FluidState>
        static Scalar viscosity(const FluidState &fluidState, int phaseIdx)
        {
            Scalar T = fluidState.temperature(phaseIdx);
            Scalar p = fluidState.pressure(phaseIdx);

            if (phaseIdx == liquidPhaseIdx)
                return H2O::liquidViscosity(T, p);
            else if (phaseIdx == gasPhaseIdx)
            {
                if (isGasPhaseImmobile())
                {
                    return std::numeric_limits<Scalar>::infinity();
                }
                else if (useCO2ViscosityAsGasMixtureViscosity())
                {
                    return CO2::gasViscosity(T, p);
                }
                else
                {
                    DUNE_THROW(Dune::NotImplemented, "Viscosity of gas phase is not specified.");
                }
            }

            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
        }

        using Base::fugacityCoefficient;
        /*!
     * \brief Returns the fugacity coefficient \f$\mathrm{[-]}\f$ of a component in a
     *        phase.
     *
     * The fugacity coefficient \f$\mathrm{\phi^\kappa_\alpha}\f$ of
     * component \f$\mathrm{\kappa}\f$ in phase \f$\mathrm{\alpha}\f$ is connected to
     * the fugacity \f$\mathrm{f^\kappa_\alpha}\f$ and the component's mole
     * fraction \f$\mathrm{x^\kappa_\alpha}\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     \f]
     * where \f$\mathrm{p_\alpha}\f$ is the pressure of the fluid phase.
     *
     * The fugacity itself is just an other way to express the
     * chemical potential \f$\mathrm{\zeta^\kappa_\alpha}\f$ of the component:
     *
     * \f[
     f^\kappa_\alpha := \exp\left\{\frac{\zeta^\kappa_\alpha}{k_B T_\alpha} \right\}
     \f]
     * where \f$\mathrm{k_B}\f$ is Boltzmann's constant.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component
     */
        template <class FluidState>
        static Scalar fugacityCoefficient(const FluidState &fluidState,
                                          int phaseIdx,
                                          int compIdx)
        {
            assert(0 <= compIdx && compIdx < numComponents);

            if (phaseIdx == gasPhaseIdx)
                // use the fugacity coefficients of an ideal gas. the
                // actual value of the fugacity is not relevant, as long
                // as the relative fluid compositions are observed,
                return 1.0;

            else if (phaseIdx == liquidPhaseIdx)
            {
                Scalar T = fluidState.temperature(phaseIdx);
                Scalar pl = fluidState.pressure(liquidPhaseIdx);
                Scalar pg = fluidState.pressure(gasPhaseIdx);

                assert(T > 0);
                assert(pl > 0 && pg > 0);
                assert(compIdx <= numComponents);

                switch ((compIdx))
                {
                case H2OIdx:
                    return H2O::vaporPressure(T) / pl;
                case CO2Idx:
                    return CO2::vaporPressure(T) / pl;
                default:
                    return 0.0;
                }
            }
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index.");
        }

        using Base::diffusionCoefficient;
        /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase \f$\mathrm{[mol^2 * s / (kg*m^3)]}\f$
     *
     * Molecular diffusion of a compoent \f$\mathrm{\kappa}\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \textbf{grad} mu_\kappa \f]
     *
     * where \f$\mathrm{\mu_\kappa}\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$\mathrm{J}\f$ is the
     * diffusive flux. \f$\mathrm{mu_\kappa}\f$ is connected to the component's
     * fugacity \f$\mathrm{f_\kappa}\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$\mathrm{p_\alpha}\f$ and \f$\mathrm{T_\alpha}\f$ are the fluid phase'
     * pressure and temperature.
     *
     * Maybe see http://www.ddbst.de/en/EED/PCP/DIF_C1050.php
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
        template <class FluidState>
        static Scalar diffusionCoefficient(const FluidState &fluidState, int phaseIdx, int compIdx)
        {
            DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
        }

        using Base::binaryDiffusionCoefficient;
        /*!
     * \brief Given the phase compositions, return the binary
     *        diffusion coefficient \f$\mathrm{[m^2/s]}\f$ of two components in a phase.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx Index of the component i
     * \param compJIdx Index of the component j
     */
        template <class FluidState>
        static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                                 int phaseIdx,
                                                 int compIIdx,
                                                 int compJIdx)
        {
            assert(0 <= compIIdx && compIIdx < numSpecies);
            assert(0 <= compJIdx && compJIdx < numSpecies);
            //
            //        if (compIIdx > compJIdx)
            //        {
            //            using std::swap;
            //            swap(compIIdx, compJIdx);
            //        }
            //
            //        Scalar T = fluidState.temperature(phaseIdx);
            //        Scalar p = fluidState.pressure(phaseIdx);
            return getParam<Scalar>("Parameters.DiffusionCoefficient", 1e-9);
        }

        using Base::enthalpy;
        /*!
     * \brief Given the phase composition, return the specific
     *        phase enthalpy \f$\mathrm{[J/kg]}\f$.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
        template <class FluidState>
        static Scalar enthalpy(const FluidState &fluidState, int phaseIdx)
        {
            DUNE_THROW(Dune::NotImplemented, "enthalpy not implemented.");
        }

        /*!
     * \brief Returns the specific enthalpy \f$\mathrm{[J/kg]}\f$ of a component in a specific phase
     * \param fluidState The fluid state
     * \param phaseIdx The index of the phase
     * \param componentIdx The index of the component
     */
        template <class FluidState>
        static Scalar componentEnthalpy(const FluidState &fluidState, int phaseIdx, int componentIdx)
        {
            DUNE_THROW(Dune::NotImplemented, "componentEnthalpy not implemented." << phaseIdx);
        }

        using Base::thermalConductivity;
        /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the thermal conductivity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
        template <class FluidState>
        static Scalar thermalConductivity(const FluidState &fluidState, int phaseIdx)
        {
            DUNE_THROW(Dune::NotImplemented, " thermalConductivity not implemented");
        }

        using Base::heatCapacity;
        /*!
     * \copybrief Base::heatCapacity
     *
     * \note We employ the heat capacity of the pure phases.
     *
     * \todo TODO Implement heat capacity for gaseous CO2
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
        template <class FluidState>
        static Scalar heatCapacity(const FluidState &fluidState,
                                   int phaseIdx)
        {
            DUNE_THROW(Dune::NotImplemented, "heatCapacity not in implemented");
        }

    };

} // end namespace Dumux

#endif
