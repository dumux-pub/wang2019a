// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
#ifndef DUMUX_SHPCO2_DECOUPLED_REACTIONS_HH
#define DUMUX_SHPCO2_DECOUPLED_REACTIONS_HH

#include <cmath>
#include <dumux/material/fluidsystems/SHPCO2FluidSystemDecoupled.hh>

namespace Dumux
{
/*!
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
template <class TypeTag>
class SHPCO2DecoupledReaction
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ThisType = SHPCO2DecoupledReaction<TypeTag>;
    using Sources = GetPropType<TypeTag, Properties::NumEqVector>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

public:

    using ParameterCache = NullParameterCache;

    SHPCO2DecoupledReaction()
    {
        pKH2O = getParam<Scalar>("EquilibriumConstants.kH2O");
        pKCO2 = getParam<Scalar>("EquilibriumConstants.kCO2");
        pKHCO3= getParam<Scalar>("EquilibriumConstants.kHCO3");

        pKCalciteDis = getParam<Scalar>("EquilibriumConstants.kCalciteDis");
        pKQuartzDis  = getParam<Scalar>("EquilibriumConstants.kQuartzDis");

        kTCalcite25 = getParam<Scalar>("Precipitation.CalciteK25");
        kTQuartz25  = getParam<Scalar>("Precipitation.QuartzK25");

        EaCalcite = getParam<Scalar>("Precipitation.CalciteEa");
        EaQuartz  = getParam<Scalar>("Precipitation.QuartzEa");

        SrmQuartz = getParam<Scalar>("Precipitation.QuartzSrm");
        SrmCalcite= getParam<Scalar>("Precipitation.CalciteSrm");

        Scalar T = this->temprature() + 273.15;
        using std;
        Scalar R = 8.314;
        effKTQuartz  = kTQuartz25 * exp(EaQuartz/R/T0)*exp(-EaQuartz/R/T);
        effKTCalcite = kTCalcite25* exp(EaCalcite/R/T0)*exp(-EaCalcite/R/T);
    }

    //Indices of the phases
    static const int phase0Idx         = FluidSystem::phase0Idx;
    static const int phase1Idx         = FluidSystem::phase1Idx;
    static const int QuartzPhaseIdx    = SolidSystem::quartzPhaseIdx;
    static const int CalcitePhaseIdx   = SolidSystem::calcitePhaseIdx;


    //Indices of the components
    static constexpr int numComponents = FluidSystem::numComponents;

    static const int wCompIdx     = FluidSystem::comp0Idx;
    static const int nCompIdx     = FluidSystem::comp1Idx;
    static const int H2OIdx       = FluidSystem::H2OIdx;
    static const int CO2Idx       = FluidSystem::CO2Idx;
    static const int HIonIdx      = FluidSystem::HIonIdx;
    static const int NaIonIdx     = FluidSystem::NaIonIdx;
    static const int CaIonIdx     = FluidSystem::CaIonIdx;    
    static const int SiO2Idx      = FluidSystem::SiO2Idx;
    static const int OHIonIdx     = FluidSystem::OHIonIdx;
    static const int HCO3Idx      = FluidSystem::HCO3Idx;

    //Indices of the bio/coal volume fractions
    static const int QuartzIdx    = QuartzPhaseIdx + numComponents;
    static const int CalciteIdx   = CalcitePhaseIdx + numComponents


    /*!
     * \brief calculate the kinetic precipitation of quartz and calcite.
     * Assuming, the equlibrium is already achieved due to the fast rate.
     * Chemical equilibrium is calculated in calEquilbrium.
     * \param q Source Vector
     * \param volVars VolumeVariables
     */
    void reactionSource(Sources &q,
                        const VolumeVariables &volVars,
                        const Scalar dt)
    {   

        ////////////////////////////////
        /// Calcite
        ////////////////////////////////
        
        // calulate molality & solidmass
        Scalar mCa2 = FluidSystem::molality(volVars.fluidstate(),CaIonIdx,FluidSystem::liquidPhaseIdx);
        Scalar mH = FluidSystem::molality(volVars.fluidstate(),HIonIdx,FluidSystem::liquidPhaseIdx);
        Scalar mHCO3 = FluidSystem::molality(volVars.fluidstate(),HCO3Idx,FluidSystem::liquidPhaseIdx);
        Scalar CalciteMass = volVars.solidVolumeFraction(CalcitePhaseIdx)*SolidSystem::density(CalcitePhaseIdx); //[kg Solid/m3]

        // reaction rate for dissolution
        Scalar QCalciteDis = mCa2*mHCO3/mH;
        Scalar vDisCalcite = effKTCalcite * SrmCalcite * (1-QCalciteDis/std::pow(10,pKCalciteDis)); // [mol/kg Solid/s]
        vDisCalcite *= CalciteMass; // [mol/m3/s]
        
        Scalar QCalcitePre = 1/QCalciteDis;
        Scalar pKCalcitePre = -pKCalciteDis;
        Scalar vPreCalcite = effKTCalcite * SrmCalcite * (QCalcitePre/std::pow(10, pKCalcitePre)-1);
        vPreCalcite *= CalciteMass;
        
        q[CalciteIdx] += (vPreCalcite-vDisCalcite)* SolidSystem::molarMass(CalcitePhaseIdx);
        q[HIondx] += vPreCalcite - vDisCalcite;
        q[HCO3Idx] += vDisCalcite - vPreCalcite;
        q[CaIonIdx] += vDisCalcite - vPreCalcite;

        /////////////////////////////////////
        // Quartz
        ////////////////////////////////////

        // calulate molality & solidmass
        Scalar mSiO2 = FluidSystem::molality(volVars.fluidstate(),SiO2Idx,FluidSystem::liquidPhaseIdx);
        Scalar quartzMass = volVars.solidVolumeFraction(QuartzPhaseIdx)*SolidSystem::density(QuartzPhaseIdx); //[kg Solid/m3]

        // reaction rate for dissolution
        Scalar QQuartz = mSiO2;
        Scalar vDisQuartz = effKTQuartz * SrmQuartz * (1-QQuartz/std::pow(10,pKQuartzDis)); // [mol/kg Solid/s]
        vDisQuartz *= quartzMass; // [mol/m3/s]
        
        Scalar QQuartzPre = 1/QQuartz;
        Scalar pKQuartzPre = -pKQuartzDis;
        Scalar vPreQuartz = effKTQuartz * SrmQuartz * (QQuartzPre/std::pow(10, pKQuartzPre)-1);
        vPreQuartz *= CalciteMass;
        
        q[CalciteIdx] += (vPreCalcite-vDisCalcite)* SolidSystem::molarMass(CalcitePhaseIdx);
        q[HIondx] += vPreCalcite - vDisCalcite;
        q[HCO3Idx] += vDisCalcite - vPreCalcite;
        q[CaIonIdx] += vDisCalcite - vPreCalcite;
    }

    /*!
     * \brief use this function to evaluate the total concentration of species. 
     * The coefficient matrix must be adapted to meet the choice of primary variables.
     * The secondary species has the zero total concentration.
     * \tparam FluidState 
     * \param fs 
     * \return the vector of total concentration in unit [mol/m³]
     */
    template<class FluidState>
    NumEqVector calTotalConcentration(FluidState& fs)
    { 
        NumEqVector totolCon_(0.0); //[mol]

        Scalar numComp = FluidSystem::numComponents;

        // calculate the number of moles of each component
        for(int compIdx = 0; compIdx < numComp; ++compIdx)
        {   
            for(int phaseIdx = 0; phaseIdx < FluidSystem::numPHases; ++phaseIdx)
            {
                totalCon[compIdx] +=fs.saturation(phaseIdx,compIdx)
                                    *fs.molarDensity(phaseIdx)
                                    *fs.moleFraction(phaseIdx,compIdx);
            }
        }
    
        // calculate total concentration with the matrix
        Dune::FieldMatrix<Scalar,numComponents,numComponents> M(0.0);
        for(compIdx = 0; compIdx < numComponents - FluidSystem::numEquilibrium ; compIdx++)
        {
            M[compIdx][compIdx] = 1.0;
        }
        //   OH <-> H2O - H
        M[H2OIdx][OHIonIdx] = 1.0;
        M[HIonIdx][OHIonIdx] = -1.0;

        //   HCO3 <-> H2O + CO2 - H
        M[H2OIdx][HCO3Idx] = 1.0;
        M[CO2Idx][HCO3Idx] = 1.0;
        M[HIonIdx][HCO3Idx] = -1.0;

        return totalCon_;
    }



    /*!
     * \brief  use this function to calculate the equilibrium constant.
     * \tparam FluidState 
     * \tparam PrimaryVariables 
     * \param fluidState 
     * \param priVars 
     * \return NumEqVector 
     */
    template<class FluidState>
    NumEqVector calEquilibrium(FluidState& fs)
    {
        NumEqVector eq_(0.0);
        //calculate molarity
        //for water we use mole fraction
        Scalar mOH = FluidSystem::molality(fs,OHIonIdx,FluidSystem::liquidPhaseIdx);
        Scalar mH  = FluidSystem::molality(fs,HIonIdx,FluidSystem::liquidPhaseIdx);
        Scalar mH2O = fs.moleFraction(FluidSystem::liquidPhaseIdx,H2OIdx);
        Scalar mCO2 = FluidSystem::molality(fs,CO2Idx,FluidSystem::liquidPhaseIdx);
        Scalar mHCO3 = FluidSystem::molality(fs,HCO3Idx,FluidSystem::liquidPhaseIdx);

        eq_[OHIonIdx] = std::log(mOH) + std::log(mH) - std::log(mH2O) - pKH2O;
        eq_[HCO3Idx] = std::log(mHCO3) + std::log(mH) - std::log(mH2O) - std::log(mCO2) - pKHCO3;
        
        return eq_;
    }

    

    template <class VolumeVariables>
    void solveChemicalEquilibrium(VolumeVariables& volVars)
    {
        NumEqVector init_ = calTotalConcentration(volVars);

    }
private:
    Scalar K_;
    Scalar pKH2O;
    Scalar pKCO2;
    Scalar pKHCO3;
    Scalar pKCalciteDis;
    Scalar pKQuartzDis;
    Scalar effKTCalcite;
    Scalar effKTQuartz;
    Scalar EaCalcite;
    Scalar EaQuartz;
    Scalar SrmQuartz;
    Scalar SrmCalcite;
    Scalar kTCalcite25;
    Scalar kTQuartz25;
    Scalar T0 =25+273.15; // [k] 
};

} // end namespace

#endif
