// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
#ifndef DUMUX_BIO_CARBONIC_ACID_HH
#define DUMUX_BIO_CARBONIC_ACID_HH

#include <tutorial/ex4/fluidsystems/biomin.hh>

namespace Dumux
{
/*!
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
template <class TypeTag>
class BioCarbonicAcidChemistry
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using ThisType = BioCarbonicAcidChemistry<TypeTag>;
    using Sources = typename GET_PROP_TYPE(TypeTag, NumEqVector);

public:

    BioCarbonicAcidChemistry()
    {   //ureolysis kinetic parameters
        kub_ = getParam<Scalar>("UreolysisCoefficients.kub");
        kurease_ = getParam<Scalar>("UreolysisCoefficients.kurease");
        Ku_ = getParam<Scalar>("UreolysisCoefficients.Ku");
    }

    static const int wPhaseIdx = FluidSystem::wPhaseIdx;

    static const int wCompIdx = FluidSystem::wCompIdx;
    static const int nCompIdx = FluidSystem::nCompIdx;

    static const int H2OIdx = FluidSystem::H2OIdx;
    static const int CaIdx = FluidSystem::CaIdx;
    static const int CalciteIdx = FluidSystem::CalciteIdx;
    static const int BiofilmIdx = FluidSystem::BiofilmIdx;
    static const int UreaIdx = FluidSystem::UreaIdx;

    static const int bPhaseIdx = FluidSystem::bPhaseIdx;

    /*!
     * \brief Returns the molality of a component x (mol x / kg solvent) for a given
     * mole fraction (mol x / mol solution)
     * The salinity and the mole Fraction of CO2 are considered
     *
     */
    static Scalar moleFracToMolality(Scalar moleFracX, Scalar moleFracSalinity, Scalar moleFracCTot)
    {
        Scalar molalityX = moleFracX / (1 - moleFracSalinity - moleFracCTot) / FluidSystem::molarMass(H2OIdx);
        return molalityX;
    }

    /*!
     * \brief Returns the mole fraction of a component x (mol x / mol solution) for a given
     * molality  (mol x / kg solution)
     * The salinity and the mole Fraction of CO2 are considered
     *
     */
    static Scalar molalityToMoleFrac(Scalar molalityX, Scalar moleFracSalinity, Scalar moleFracCTot)
    {
        Scalar moleFracX = molalityX * (1 - moleFracSalinity - moleFracCTot) * FluidSystem::molarMass(H2OIdx);
        return moleFracX;
    }


    void reactionSource(Sources &q,const VolumeVariables &volVars)
    {
        //   define and compute some parameters for convenience: //TODO: update with new NEXT functions and locations!!
        Scalar xwCa = volVars.moleFraction(wPhaseIdx,CaIdx);
        Scalar densityBiofilm = volVars.density(bPhaseIdx); //FluidSystem.density(bPhaseIdx) ???

        Scalar volFracBiofilm = volVars.precipitateVolumeFraction(bPhaseIdx);
        if (volFracBiofilm < 0)
         volFracBiofilm = 0;
        Scalar massBiofilm = densityBiofilm * volFracBiofilm;

        Scalar molalityUrea = moleFracToMolality(volVars.moleFraction(wPhaseIdx,UreaIdx), xwCa, volVars.moleFraction(wPhaseIdx,nCompIdx));  //[mol_urea/kg_H2O]
        if (molalityUrea < 0)
         molalityUrea = 0;

        // compute rate of urealysis:
        Scalar Zub = kub_ *  massBiofilm; // [kgurease/m³]
        Scalar rurea = kurease_ * Zub * molalityUrea / (Ku_ + molalityUrea); //[mol/m³s]

        // compute dissolution and precipitation rate of calcite
        Scalar rdiss = 0;
        Scalar rprec = 0;

        if(xwCa>1e-8)
         rprec = rurea;
        // rdiss+rprec[mol/m³s]
        // rurea[mol/m³s]
        // q[mol/m³s]
        q[wCompIdx] += 0;
        q[nCompIdx] += rurea - rprec + rdiss;
        q[CaIdx] += - rprec + rdiss;
        q[UreaIdx] += - rurea;
        q[BiofilmIdx] += 0;
        q[CalciteIdx] += + rprec - rdiss;
    }

private:
    // urease parameters
        Scalar kub_;
        Scalar kurease_;
        Scalar Ku_;
};

} // end namespace

#endif
