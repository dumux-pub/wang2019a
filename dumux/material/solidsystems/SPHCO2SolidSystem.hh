// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SolidSystems
 * \brief @copybrief Dumux::SolidSystems::InertSolidPhase
 */
#ifndef DUMUX_CASO4_SOLID_PHASE_HH
#define DUMUX_CASO4_SOLID_PHASE_HH

#include <string>
#include <dune/common/exceptions.hh>

#include <dumux/material/components/quartz.hh>
#include <dumux/material/components/calcite.hh>
namespace Dumux {
namespace SolidSystems {

/*!
 * \ingroup SolidSystems
 * \brief Solid system speiced for SPHCO2 
 * \note a solid is considered inert if it can't dissolve in a liquid and
 *       and can't increase its mass by precipitation from a fluid phase.
 * \note inert components have to come after all non-inert components
 */
template <class Scalar>
class QuartzCalciteSolidSystem
{
public:
    using Quartz = Components::Quartz<Scalar>;
    using Calcite = Components::Calcite<Scalar>;

    /****************************************
     * Solid phase related static parameters
     ****************************************/
    static constexpr int numComponents = 2;
    static constexpr int numInertComponents = 0;
    // static constexpr int InertCoalPhaseIdx = 0;
    static constexpr int quartzPhaseIdx = 0;
    static constexpr int calcitePhaseIdx = 1;


    /*!
     * \brief Return the human readable name of a solid phase
     *
     * \param compIdx The index of the solid phase to consider
     */
    static std::string componentName(int compIdx)
    {
        switch (compIdx)
        {
            case quartzPhaseIdx:
                return std::string("Quartz");
            case calcitePhaseIdx:
                return std::string("Calcite");
            default:
                DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }



    }

    /*!
     * \brief A human readable name for the solid system.
     */
    static std::string name()
    { return "QuartzCalciteSolidSystem"; }

    /*!
     * \brief Returns whether the phase is incompressible
     */
    static constexpr bool isCompressible(int compIdx)
    { return false; }

    /*!
     * \brief Returns whether all components are inert (don't react)
     */
    static constexpr bool isInert()
    { return (numComponents == numInertComponents); }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the component.
     */
    static Scalar molarMass(int compIdx)
    {
        switch (compIdx)
        {
            case calcitePhaseIdx: return Calcite::molarMass();
            case quartzPhaseIdx: return Quartz::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState)
    {
        Scalar rhoQuartz = Quartz::solidDensity(solidState.temperature());
        Scalar rhoCalcite = Calcite::solidDensity(solidState.temperature());

        Scalar volFracQuartz = solidState.volumeFraction(quartzPhaseIdx);
        Scalar volFracCalcite = solidState.volumeFraction(calcitePhaseIdx);


        return (
                rhoQuartz*volFracQuartz
                + rhoCalcite*volFracCalcite)
               /(volFracQuartz
                 + volFracCalcite);
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case calcitePhaseIdx: return Calcite::solidDensity(solidState.temperature());
            case quartzPhaseIdx: return Quartz::solidDensity(solidState.temperature());

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The molar density of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar molarDensity(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case calcitePhaseIdx: return Calcite::solidDensity(solidState.temperature())/Calcite::molarMass();
            case quartzPhaseIdx: return Quartz::solidDensity(solidState.temperature())/Quartz::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }
};

} // end namespace SolidSystems
} // end namespace Dumux

#endif
