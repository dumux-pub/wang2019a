// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SolidSystems
 * \brief @copybrief Dumux::SolidSystems::InertSolidPhase
 */
#ifndef DUMUX_SOLIDSYSTEMS_COMPOSITIONAL_SOLID_PHASE_HH
#define DUMUX_SOLIDSYSTEMS_COMPOSITIONAL_SOLID_PHASE_HH

#include <string>
#include <dune/common/exceptions.hh>

#include "../components/biofilm.hh"
#include <dumux/material/components/ccoal.hh>

namespace Dumux {
namespace SolidSystems {

/*!
 * \ingroup SolidSystems
 * \brief A solid phase consisting of 6 reactive solid components
 * \note a solid is considered inert if it can't dissolve in a liquid and
 *       and can't increase its mass by precipitation from a fluid phase.
 * \note inert components have to come after all non-inert components
 */
template <class Scalar>
class MECBMSolidPhase
{
public:
    using Biofilm = Components::Biofilm<Scalar>;
    using CCoal = Components::CCoal<Scalar>;

    /****************************************
     * Solid phase related static parameters
     ****************************************/
    static constexpr int numComponents = 7;
    static constexpr int numInertComponents = 1;
    // static constexpr int InertCoalPhaseIdx = 0;
    static constexpr int CoalBacPhaseIdx = 0;
    static constexpr int AmCoalBacPhaseIdx = 1;
    static constexpr int AcetoArchPhaseIdx = 2;
    static constexpr int HydroArchPhaseIdx = 3;
    static constexpr int MethyArchPhaseIdx = 4;
    static constexpr int CCoalPhaseIdx = 5;
    static constexpr int CoalMatrixPhaseIdx = 6;

    /*!
     * \brief Return the human readable name of a solid phase
     *
     * \param compIdx The index of the solid phase to consider
     */
    static std::string componentName(int compIdx)
    {
        switch (compIdx)
        {
            case CoalBacPhaseIdx: return std::string("CoalBacPhase");
            case AmCoalBacPhaseIdx: return  std::string("AmCoalBacPhase");
            case AcetoArchPhaseIdx: return std::string("AcetoArchPhase");
            case HydroArchPhaseIdx: return std::string("HydroArchPhase");
            case MethyArchPhaseIdx: return std::string("MethyArchPhase");
            case CCoalPhaseIdx: return std::string("CCoalPhase");
            case CoalMatrixPhaseIdx: return std::string("CoalMatrix");

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief A human readable name for the solid system.
     */
    static std::string name()
    { return "MECBMSolidSystem"; }

    /*!
     * \brief Returns whether the phase is incompressible
     */
    static constexpr bool isCompressible(int compIdx)
    { return false; }

    /*!
     * \brief Returns whether all components are inert (don't react)
     */
    static constexpr bool isInert()
    { return (numComponents == numInertComponents); }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the component.
     */
    static Scalar molarMass(int compIdx)
    {
        switch (compIdx)
        {
            case CoalBacPhaseIdx: return Biofilm::molarMass();
            case AmCoalBacPhaseIdx: return Biofilm::molarMass();
            case AcetoArchPhaseIdx: return Biofilm::molarMass();
            case HydroArchPhaseIdx: return Biofilm::molarMass();
            case MethyArchPhaseIdx: return Biofilm::molarMass();
            case CCoalPhaseIdx: return CCoal::molarMass();
            case CoalMatrixPhaseIdx: return CCoal::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState)
    {
        Scalar rhoCoalBac = Biofilm::solidDensity(solidState.temperature());
        Scalar rhoAmCoalBac = Biofilm::solidDensity(solidState.temperature());
        Scalar rhoAcetoArch = Biofilm::solidDensity(solidState.temperature());
        Scalar rhoHydroArch = Biofilm::solidDensity(solidState.temperature());
        Scalar rhoMethyArch = Biofilm::solidDensity(solidState.temperature());
        Scalar rhoCCoal = CCoal::solidDensity(solidState.temperature());
        Scalar rhoCoalMatrix = CCoal::solidDensity(solidState.temperature());

        Scalar volFracCoalBac = solidState.volumeFraction(CoalBacPhaseIdx);
        Scalar volFracAmCoalBac = solidState.volumeFraction(AmCoalBacPhaseIdx);
        Scalar volFracAcetoArch = solidState.volumeFraction(AcetoArchPhaseIdx);
        Scalar volFracHydroArch = solidState.volumeFraction(HydroArchPhaseIdx);
        Scalar volFracMethyArch = solidState.volumeFraction(MethyArchPhaseIdx);
        Scalar volFracCCoal = solidState.volumeFraction(CCoalPhaseIdx);
        Scalar volFracCoalMatrix = solidState.volumeFraction(CoalMatrixPhaseIdx);


        return (
                rhoCoalBac*volFracCoalBac
                + rhoAmCoalBac*volFracAmCoalBac
                + rhoAcetoArch*volFracAcetoArch
                + rhoHydroArch*volFracHydroArch
                + rhoMethyArch*volFracMethyArch
                + rhoCCoal*volFracCCoal
                + rhoCoalMatrix*volFracCoalMatrix)
               /(
                 volFracCoalBac
                 + volFracAmCoalBac
                 + volFracAcetoArch
                 + volFracHydroArch
                 + volFracMethyArch
                 + volFracCCoal
                 + volFracCoalMatrix);
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case CoalBacPhaseIdx: return Biofilm::solidDensity(solidState.temperature());
            case AmCoalBacPhaseIdx: return Biofilm::solidDensity(solidState.temperature());
            case AcetoArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature());
            case HydroArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature());
            case MethyArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature());
            case CCoalPhaseIdx: return CCoal::solidDensity(solidState.temperature());
            case CoalMatrixPhaseIdx: return CCoal::solidDensity(solidState.temperature());

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The molar density of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar molarDensity(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case CoalBacPhaseIdx: return Biofilm::solidDensity(solidState.temperature())/Biofilm::molarMass();
            case AmCoalBacPhaseIdx: return Biofilm::solidDensity(solidState.temperature())/Biofilm::molarMass();
            case AcetoArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature())/Biofilm::molarMass();
            case HydroArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature()/Biofilm::molarMass());
            case MethyArchPhaseIdx: return Biofilm::solidDensity(solidState.temperature())/Biofilm::molarMass();
            case CCoalPhaseIdx: return CCoal::solidDensity(solidState.temperature())/CCoal::molarMass();
            case CoalMatrixPhaseIdx: return CCoal::solidDensity(solidState.temperature())/CCoal::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }
};

} // end namespace SolidSystems
} // end namespace Dumux

#endif
