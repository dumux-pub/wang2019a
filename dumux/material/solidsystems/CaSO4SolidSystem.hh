// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SolidSystems
 * \brief @copybrief Dumux::SolidSystems::InertSolidPhase
 */
#ifndef DUMUX_CASO4_SOLID_PHASE_HH
#define DUMUX_CASO4_SOLID_PHASE_HH

#include <string>
#include <dune/common/exceptions.hh>

#include "../components/caso4.hh"
#include <dumux/material/components/ccoal.hh>
namespace Dumux {
namespace SolidSystems {

/*!
 * \ingroup SolidSystems
 * \brief A solid phase consisting of CaSO4
 * \note a solid is considered inert if it can't dissolve in a liquid and
 *       and can't increase its mass by precipitation from a fluid phase.
 * \note inert components have to come after all non-inert components
 */
template <class Scalar>
class CaSO4Solid
{
public:
    using CaSO4 = Components::CaSO4<Scalar>;
    using CCoal = Components::CCoal<Scalar>;
    
    /****************************************
     * Solid phase related static parameters
     ****************************************/
    static constexpr int numComponents = 2;
    static constexpr int numInertComponents = 1;
    // static constexpr int InertCoalPhaseIdx = 0;
    static constexpr int CaSO4PhaseIdx = 0;
    static constexpr int CoalMatrixPhaseIdx = 1;

    /*!
     * \brief Return the human readable name of a solid phase
     *
     * \param compIdx The index of the solid phase to consider
     */
    static std::string componentName(int compIdx)
    {
        switch (compIdx)
        {
            case CaSO4PhaseIdx: return std::string("CaSO4Phase");
            case CoalMatrixPhaseIdx: return std::string("CoalMatrix");
            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief A human readable name for the solid system.
     */
    static std::string name()
    { return "CaSO4SolidSystem"; }

    /*!
     * \brief Returns whether the phase is incompressible
     */
    static constexpr bool isCompressible(int compIdx)
    { return false; }

    /*!
     * \brief Returns whether all components are inert (don't react)
     */
    static constexpr bool isInert()
    { return (numComponents == numInertComponents); }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the component.
     */
    static Scalar molarMass(int compIdx)
    {
        switch (compIdx)
        {
            case CaSO4PhaseIdx: return CaSO4::molarMass();
            case CoalMatrixPhaseIdx: return CCoal::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState)
    {
        Scalar rhoCaSO4 = CaSO4::solidDensity(solidState.temperature());
        Scalar rhoCoalMatrix = CCoal::solidDensity(solidState.temperature());

        Scalar volFracCaSO4 = solidState.volumeFraction(CaSO4PhaseIdx);
        Scalar volFracCoalMatrix = solidState.volumeFraction(CoalMatrixPhaseIdx);

        DUNE_THROW(Dune::InvalidStateException, "Phase Density not implemented ");

        return (
                rhoCaSO4*volFracCaSO4
                + rhoCoalMatrix*volFracCoalMatrix)
               /(volFracCaSO4
                 + volFracCoalMatrix);
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case CaSO4PhaseIdx: return CaSO4::solidDensity(solidState.temperature());
            case CoalMatrixPhaseIdx: return CCoal::solidDensity(solidState.temperature());

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief The molar density of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar molarDensity(const SolidState& solidState, const int compIdx)
    {
        switch (compIdx)
        {
            case CaSO4PhaseIdx: return CaSO4::solidDensity(solidState.temperature())/CaSO4::molarMass();
            case CoalMatrixPhaseIdx: return CCoal::solidDensity(solidState.temperature())/CCoal::molarMass();

            default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }
};

} // end namespace SolidSystems
} // end namespace Dumux

#endif
