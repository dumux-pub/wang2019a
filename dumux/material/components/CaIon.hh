/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$Ca^{2+}\f$ fluid properties
 */
#ifndef DUMUX_CaIon_HH
#define DUMUX_CaIon_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the CaIon fluid properties
 */
template <class Scalar>
class CaIon
: public Components::Base<Scalar, CaIon<Scalar> >
, public Components::Liquid<Scalar, CaIon<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$Ca^{2+}\f$.
    */
    static const char *name()
    { return "Ca(2+)"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of \f$Ca^{2+}\f$.
    */
    static Scalar molarMass()
    { return 40.078e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of CaIon in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
    //{ return 2e-9; } // Check the right value TODO
};

} // end namespace Components
} // end namespace Dumux

#endif
