/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$SiO_2\f$ in fluid phase
 */
#ifndef DUMUX_SiO2_HH
#define DUMUX_SiO2_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$SiO_2\f$ fluid properties
 */
template <class Scalar>
class SiO2
: public Components::Base<Scalar, SiO2<Scalar> >
, public Components::Liquid<Scalar, SiO2to<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$SiO_2\f$.
    */
    static const char *name()
    { return "SiO2"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 60.0840e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
