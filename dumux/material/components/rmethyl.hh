/*
 * RMethyl.hh
 *
 *  Created on: 24.10.2016
 *      Author: scholz
 */

/*!
 * \file
 *
 * \brief A class for the RMethyl fluid properties
 */
#ifndef DUMUX_RMETHYL_HH
#define DUMUX_RMETHYL_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \brief A class for the RMethyl fluid properties
 */
template <class Scalar>
class RMethyl
: public Components::Base<Scalar, RMethyl<Scalar> >
, public Components::Liquid<Scalar, RMethyl<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the RMethyl.
    */
    static const char *name()
    { return "RMethyl"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of molecular RMethyl.
    */
    static Scalar molarMass()
    { return 15.0345e-3; } // kg/mol


   /*!
    * \brief The diffusion Coefficient of RMethyl in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.5e-9; }
    //{ return 2e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
