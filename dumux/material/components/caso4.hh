// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A class for the CaSO4 solid phase properties.
 */
#ifndef DUMUX_CaSO4_HH
#define DUMUX_CaSO4_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/solid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the CaSO4 solid phase properties.
 */
template <class Scalar>
class CaSO4
: public Components::Base<Scalar, CaSO4<Scalar> >
, public Components::Solid<Scalar, CaSO4<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the CaSO4.
    */
    static std::string name()
    { return "CaSO4"; }

   /*!
    * \brief Returns true if the solid phase is assumed to be compressible
    */
    static constexpr bool solidIsCompressible()
    {
        return false; // iso c++ requires a return statement for constexpr functions
    }
   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of molecular CaSO4.
    */
    static Scalar molarMass()
    { return 136.1406e-3; } //!< in kg/mol, based on a cell mass of 2.5e-16 kg, the molar mass of cells would be 1.5e8 kg/mol.

   /*!
    * \brief in \f$\mathrm{[kg/m^3]}\f$ of CaSO4.The (dry) density
    */
    static Scalar solidDensity(Scalar temperature)
    {
       return 2.96e3;
    }
};

} // end namespace Components
} // end namespace Dumux

#endif
