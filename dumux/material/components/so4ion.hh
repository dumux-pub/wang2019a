/*
 * so4ion.hh
 *
 *  Created on: 21.03.2020
 *      Author: yue
 */

/*!
 * \file
 * \ingroup Components
 * \brief A class for the $SO_4^{2+}$
 */
#ifndef DUMUX_SO4ION_HH
#define DUMUX_SO4ION_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the SO4Ion fluid properties
 */
template <class Scalar>
class SO4Ion
: public Components::Base<Scalar, SO4Ion<Scalar> >
, public Components::Liquid<Scalar, SO4Ion<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the SO4 ion.
    */
    static const char *name()
    { return "SO4"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of SO4Ion.
    */
    static Scalar molarMass()
    { return 96.06e-3; } // kg/mol CH3COOH

   /*!
    * \brief The diffusion Coefficient of SO4Ion in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.070e-9; }
    //{ return 2e-9; } // Check the right value TODO
};

} // end namespace Components
} // end namespace Dumux

#endif
