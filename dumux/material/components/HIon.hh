/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$H^+\f$ in fluid phase
 */
#ifndef DUMUX_H_ION_HH
#define DUMUX_H_ION_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$H^+\f$ fluid properties
 */
template <class Scalar>
class HIon
: public Components::Base<Scalar, HIon<Scalar> >
, public Components::Liquid<Scalar, HIon<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$H^+\f$.
    */
    static const char *name()
    { return "H(+)"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 1.008e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
