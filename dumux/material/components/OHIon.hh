/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$OH^-\f$ in fluid phase
 */
#ifndef DUMUX_OH_ION_HH
#define DUMUX_OH_ION_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$OH^-\f$ fluid properties
 */
template <class Scalar>
class OHION
: public Components::Base<Scalar, OHION<Scalar> >
, public Components::Liquid<Scalar, OHION<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$OH^-\f$.
    */
    static const char *name()
    { return "OH(-)"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 17.0075e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
