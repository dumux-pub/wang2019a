/*
 * CarAm.hh
 *
 *  Created on: 24.10.2016
 *      Author: scholz
 */

/*!
 * \file
 * \ingroup Components
 * \brief A class for the CarAm (Carbon Amendment=Substrate) fluid properties
 */
#ifndef DUMUX_CARAM_HH
#define DUMUX_CARAM_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the CarAm (Carbon Amendment=Substrate) fluid properties
 */
template <class Scalar>
class CarAm
: public Components::Base<Scalar, CarAm<Scalar> >
, public Components::Liquid<Scalar, CarAm<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the Carbon Amendment / Substrate.
    */
    static const char *name()
    { return "Amendment"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of molecular CarAm.
    */
    static Scalar molarMass()
    { return 0.1131146; } // kg/mol - Here: C5H702N (Robin) -> Glucose/yeast: 0.18016

   /*!
    * \brief The diffusion Coefficient of CarAm in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.5e-9; }
   // { return 2e-9; } // TODO Check the right value
};

} // end namespace Components
} // end namespace Dumux

#endif
