/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$H2O\f$ in fluid phase
 */
#ifndef DUMUX_H2O_SIMPLE_HH
#define DUMUX_H2O_SIMPLE_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$H2OSimple\f$ fluid properties
 */
template <class Scalar>
class H2OSimple
: public Components::Base<Scalar, H2OSimple<Scalar> >
, public Components::Liquid<Scalar, H2OSimple<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$H2OSimple\f$.
    */
    static const char *name()
    { return "H2O"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 1.80155e-2; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
