/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$HCO_3^{-}\f$ in fluid phase
 */
#ifndef DUMUX_HCO3_ION_HH
#define DUMUX_HCO3_ION_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$HCO_3^{-}\f$ fluid properties
 */
template <class Scalar>
class HCO3Ion
: public Components::Base<Scalar, HCO3Ion<Scalar> >
, public Components::Liquid<Scalar, HCO3Ion<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$HCO_3^{-}\f$.
    */
    static const char *name()
    { return "HCO3(-)"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 61.0170e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
};

} // end namespace Components
} // end namespace Dumux

#endif
