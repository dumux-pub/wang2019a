/*!
 * \file
 * \ingroup Components
 * \brief A class for the \f$Na^+\f$ fluid properties
 */
#ifndef DUMUX_Na_Ion_HH
#define DUMUX_Na_Ion_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the \f$Na^+\f$ fluid properties
 */
template <class Scalar>
class NaIon
: public Components::Base<Scalar, NaIon<Scalar> >
, public Components::Liquid<Scalar, NaIon<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the \f$Na^+\f$.
    */
    static const char *name()
    { return "Na(+)"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of \f$Na^+\f$.
    */
    static Scalar molarMass()
    { return 22.990e-3; } // kg/mol

   /*!
    * \brief The diffusion Coefficient of CaIon in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1e-9; }
    //{ return 2e-9; } // Check the right value TODO
};

} // end namespace Components
} // end namespace Dumux

#endif
