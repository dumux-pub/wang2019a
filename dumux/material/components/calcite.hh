// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A class for the calcite solid phase properties.
 */
#ifndef DUMUX_CALCITE_HH
#define DUMUX_CALCITE_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/solid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the quartz solid phase properties.
 */
template <class Scalar>
class Calcite
: public Components::Base<Scalar, Calcite<Scalar> >
, public Components::Solid<Scalar, Calcite<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the calcite.
    */
    static std::string name()
    { return "Calcite"; }

   /*!
    * \brief Returns true if the solid phase is assumed to be compressible
    */
    static constexpr bool solidIsCompressible()
    {
        return false; // iso c++ requires a return statement for constexpr functions
    }
   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of molecular calcite.
    */
    static Scalar molarMass()
    { return 100.087e-3; }

    /*!
     * \brief The density in \f$\mathrm{[kg/m^3]}\f$ of the calcite.
     */
    static Scalar solidDensity(Scalar &temp)
    {
        return 2.71e3;
    }
};

} // end namespace Components
} // end namespace Dumux

#endif
